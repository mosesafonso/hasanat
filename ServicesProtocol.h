//
//  ServicesProtocol.h
//  CCIReferenceArchitecture
//
//  Created by Rahul Shettigar on 19/11/14.
//  Copyright (c) 2014 CCI. All rights reserved.
//

#ifndef CCIReferenceArchitecture_ServicesProtocol_h
#define CCIReferenceArchitecture_ServicesProtocol_h

@protocol ServicesProtocol <NSObject>

@required

- (void)executePostWithUrl:(NSString *)url request:(id)requestObject responseClass:(Class)responseClass callBack:(void(^)(id result,NSError *error))callBack;

- (void)setBaseURL:(NSString *)url;

- (void)executeGetWithUrl:(NSString *)url queryParams:(NSDictionary *)queryParams requestHeaders:(NSDictionary *)requestHeaders responseClass:(Class)responseClass callBack:(void(^)(NSDictionary*, NSError *error))callBack;

- (void) executePostWithUrl:(NSString *)url request:(id)requestObject data:(NSData*) data responseClass:(Class)responseClass callBack:(void (^)(id, NSError *))callBack;

- (void) setCustomHeader:(NSString*) value;

@end

#endif
