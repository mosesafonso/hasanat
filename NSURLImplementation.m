//
//  NSURLImplementation.m
//  Demo-Restkit
//
//  Created by Soniya Gadekar on 25/11/15.
//  Copyright © 2015 creativecapsule. All rights reserved.
//

#import "NSURLImplementation.h"
#import "JSONModel.h"
#import "AccessToken.h"



typedef void (^RequestCallbacks)(id, NSError *);
/**
 *  This class will be used for mapping request operations to the respective callbacks.
 **/
#define baseURL @"http://54.251.157.35/event/api/"

@interface NSURLImplementation ()

@property (nonatomic,strong) NSURLSession *session;
@property (nonatomic,strong) NSMutableURLRequest *urlRequest;
@property (nonatomic, strong)  NSURLSessionConfiguration *sessionConfiguration;

@end

@implementation NSURLImplementation

- (id) init {
    self = [super init];
//    self.sessionConfiguration.HTTPAdditionalHeaders = @{
//                                                        @"Content-Type": @"application/json"
//                                                        };
    //self.sessionConfiguration = [[NSURLSessionConfiguration alloc] init];
    self.urlRequest = [[NSMutableURLRequest alloc] init];
    self.session = [NSURLSession sessionWithConfiguration:self.sessionConfiguration];//[NSURLSession sharedSession];
    
    //[self.urlRequest setValue:@"733290bf-6819-4163-bb8d-68d8376d89e2" forHTTPHeaderField:@"Authorization"];
    return self;
}

- (NSString*) requestURLString: (NSString *)pathURL{
    return [baseURL stringByAppendingString:pathURL];
}

- (void) setCustomHeader:(NSString*) value{
    [self.urlRequest setValue:value forHTTPHeaderField:@"Authorization"];
}


/**
 * executes POST server request using NSURLSession
 * @url - url to fetch data, @requestObject - request object is mapped to json object and converted to NSData and sent as http body , @requestHeaders - if any, the request headers to set in the URL request, @responseClass - the response object to be mapped to the model object class
 *@callback - error:if any, json - response is parsed to the reponseclass object
 **/
-(void)executePostWithUrl:(NSString *)url request:(id)requestObject responseClass:(Class)responseClass callBack:(void (^)(id, NSError *))callBack{
    
    [self setCustomHeader:[[AccessToken classMethodeOfAccessToken] getAccessToken]];
    [self.urlRequest setURL:[NSURL URLWithString:[self requestURLString:url]]];
    [self.urlRequest setHTTPMethod:@"POST"];
    [self.urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
   // NSError *writeError = nil;
    NSDictionary *dictionary = requestObject;//[requestObject dictionaryMapping];
    //NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&writeError];
    //NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //NSLog(@"JSON Output: %@", jsonString);
    
    NSMutableString *paramsString = [[NSMutableString alloc] init];
    
    for (id key in dictionary) {
        NSString *keyString = [key description];
        NSString *valueString = [[dictionary objectForKey:key] description];
        
        [paramsString appendFormat:@"%@=%@&",keyString,valueString];
    }
    
    NSLog(@"Param String %@",paramsString);
    
    [paramsString deleteCharactersInRange:NSMakeRange([paramsString length]-1, 1)];
    
    NSData* jsonData=[paramsString dataUsingEncoding:NSUTF8StringEncoding];


    [self.urlRequest setHTTPBody:jsonData];
    
    NSURLSessionDataTask *uploadTask = [self.session dataTaskWithRequest:self.urlRequest completionHandler:^(NSData * data, NSURLResponse *response, NSError * error) {

        id responseObject = [[responseClass alloc] init];
        NSDictionary *json;
        NSInteger statusCode = ((NSHTTPURLResponse*) response).statusCode;
        
        if (!error && (statusCode == 200 || statusCode == 201)) {
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [responseObject setValuesForKeysWithDictionary:json];
            callBack(responseObject, nil);
        }
        else if(statusCode == 200 || statusCode == 201){
            if (error == nil) {
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [responseObject setValuesForKeysWithDictionary:json];
                callBack(responseObject, error);
            }else{
                callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
            }
            
        }else{
            callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
        }
    }];
    
    [uploadTask resume];
}


/**
 * executes GET server request using NSURLSession
 * @url - url to fetch data, @queryParams - if any, then the values are used to construct the url string, @requestHeaders - if any, the request headers to set in the URL request, @responseClass - the response object to be mapped to the model object class
 *@callback - error:if any, json - response is parsed to the reponseclass object
 **/
-(void)executeGetWithUrl:(NSString *)url queryParams:(NSDictionary *)queryParams requestHeaders:(NSDictionary *)requestHeaders responseClass:(Class)responseClass callBack:(void (^)(NSDictionary*, NSError *))callBack{
    
    // set query params to the url - forming the url string with the query params
    
       self.urlRequest = [[NSMutableURLRequest alloc] init];
    
    NSString *requestURLString = [self requestURLString:url];
    NSString *paramValue = @"";
    
    if (queryParams != nil && queryParams.count > 0) {
        requestURLString = [requestURLString stringByAppendingString:@"?"];
        
        for (NSString *key in queryParams) {
            paramValue = [queryParams objectForKey:key];
            paramValue = [key stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            if ([requestURLString characterAtIndex:[requestURLString length] -1 ] != '?') {
                paramValue= [NSString stringWithFormat:@"&%@=%@",paramValue, [queryParams objectForKey:key]];
            }else{
                paramValue = [NSString stringWithFormat:@"%@=%@", paramValue, [queryParams objectForKey:key]];
            }
            
            requestURLString = [requestURLString stringByAppendingString:paramValue];
            
        }
    }
    
    NSLog(@"Url %@",requestURLString);
    
    
    
    
   [self setCustomHeader:[[AccessToken classMethodeOfAccessToken] getAccessToken]];
    [self.urlRequest setURL:[NSURL URLWithString:requestURLString]];
    [self.urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self.urlRequest setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithRequest:self.urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
        id responseObject = [[responseClass alloc] init];
        NSDictionary *json;
        NSInteger statusCode = httpResp.statusCode;
        if (!error && (statusCode == 200 || statusCode == 201)) {
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [responseObject setValuesForKeysWithDictionary:json];
            callBack(json, nil);
            
        }else if(statusCode == 200 || statusCode == 201){
            if (error == nil) {
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [responseObject setValuesForKeysWithDictionary:json];
                callBack(json, error);
            }else{
                callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
            }
        }else{
            //errorObj.code = 404;
            callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
        }
    }];
    
    [dataTask resume];
}


- (void) executePostWithUrl:(NSString *)url request:(id)requestObject data:(NSData*) data responseClass:(Class)responseClass callBack:(void (^)(id, NSError *))callBack{
    
    [self setCustomHeader:[[AccessToken classMethodeOfAccessToken] getAccessToken]];
    [self.urlRequest setURL:[NSURL URLWithString:[self requestURLString:url]]];
    [self.urlRequest setHTTPMethod:@"POST"];
    

    
    
//    NSString *boundary = @"----------V2ymHFg03ehbqgZCaKO6jy";//--ARCFormBoundaryhcrmc6mljysnhfr
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [self.urlRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
//    
//    NSMutableData *body = [NSMutableData data];
//    
//    if(data != NULL)
//    {
//        //only send these methods when transferring data as well as id and caption
//        [body appendData:[@"Content-Disposition: form-data; name=\"files\"; filename=\"files.jpg\"r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[NSData dataWithData:data]];
//    }
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"alb_id\"\r\n\r\n%i", [[requestObject objectForKey:@"alb_id"] intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"caption\"\r\n\r\n%@", [requestObject objectForKey:@"caption"]] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [self.urlRequest setHTTPBody:body];
    
    
  //  NSLog(@"%.2f",(float)webData.length/1024.0f/1024.0f);
    
   
    
    NSString *boundary = @"_187934598797439873422234";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [self.urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:data]];
    
    
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"album_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *alb_id = [NSString stringWithFormat:@"%@",[requestObject objectForKey:@"album_id"]];
    [body appendData:[alb_id dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@""dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"caption_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[requestObject objectForKey:@"caption_name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self.urlRequest setHTTPBody:body];
    [self.urlRequest addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLSessionUploadTask *uploadImageTask = [self.session uploadTaskWithRequest:self.urlRequest fromData:body completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        id responseObject = [[responseClass alloc] init];
        NSDictionary *json;
        NSInteger statusCode = ((NSHTTPURLResponse*) response).statusCode;
        
        if (!error && (statusCode == 200 || statusCode == 201)) {
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            
            NSLog(@"Json %@",json);
            
            [responseObject setValuesForKeysWithDictionary:json];
            callBack(responseObject, nil);
            
        }else if(statusCode == 200 || statusCode == 201){
            if (error == nil) {
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [responseObject setValuesForKeysWithDictionary:json];
                callBack(responseObject, error);
            }else{
                callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
            }
            
        }else{
            callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
        }
    }];
    
    [uploadImageTask resume];
    
}


/**
 * executes PUT server request using NSURLSession
 * @url - url to fetch data, @requestObject - request object is mapped to json object and converted to NSData and sent as http body , @requestHeaders - if any, the request headers to set in the URL request, @responseClass - the response object to be mapped to the model object class
 *@callback - error:if any, json - response is parsed to the reponseclass object
 **/
-(void) executePutWithUrl:(NSString *)url request:(id)requestObject responseClass:(Class)responseClass callBack:(void (^)(id, NSError *, NSDictionary *))callBack{

    [self setBaseURL:url];
    
    [self.urlRequest setHTTPMethod:@"PUT"];
    
    NSError *writeError = nil;
    //NSError *error = nil;
    NSDictionary *dictionary = [requestObject dictionaryMapping];
    // id object = [[ObjectMapper mapper] mapObject:requestObject toClass:responseClass withError:&error];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"JSON Output: %@", jsonString);
    
    [self.urlRequest setHTTPBody:jsonData];
    
    
    NSURLSessionUploadTask *uploadTask = [self.session uploadTaskWithRequest:self.urlRequest fromData:nil completionHandler:^(NSData * data, NSURLResponse *response, NSError * error) {
        
        id responseObject = [[responseClass alloc] init];
        NSDictionary *json;
        NSInteger statusCode = ((NSHTTPURLResponse*) response).statusCode;
        
        //NSDictionary *dict =@{@"transactionSetupField": @"12hjdhkhvkl"};
        //[responseObject setValuesForKeysWithDictionary:dict];
        
        if (!error && (statusCode == 200 || statusCode == 201)) {
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            [responseObject setValuesForKeysWithDictionary:json];
            callBack(responseObject, nil, nil);
            
        }else if(statusCode == 200 || statusCode == 201){
            if (error == nil) {
                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [responseObject setValuesForKeysWithDictionary:json];
                callBack(responseObject, error, nil);
            }else{
                callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil],nil);
            }
            
        }else{
            callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil], nil);
        }
    }];
    
    [uploadTask resume];
}

-(void)setBaseURL:(NSString *)url{
     self.urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
}

//- (void) URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
//    NSLog(@"resp");
//}






@end
