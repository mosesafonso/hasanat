//
//  BaseService.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSURLImplementation.h"


@interface BaseService : NSObject

@property (nonatomic, strong) NSURLImplementation* serviceDelegate;

//- (void) setHeaders:(NSString*)value;
@end
