//
//  SettingsResponse.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NotifcationData.h"

@interface SettingsResponse : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NotifcationData *data;

- (NSDictionary*) dictionaryMapping;

@end
