//
//  DocumentsViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "DocumentsViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "NewsletterTableViewCell.h"
#import "DocumentsListService.h"
#import "ReaderViewController.h"
#import "AFURLSessionManager.h"
#import "MBProgressHUD.h"
#import "PDFViewerViewController.h"

@interface DocumentsViewController ()<ReaderViewControllerDelegate>
{
    NSMutableArray *data;
    NSInteger pageCount;
    UIActivityIndicatorView *spinnerFooter;
    UIRefreshControl *refreshControl;
}
@end

@implementation DocumentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    pageCount = 0;
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Documents";
    
    [self customSetup];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([NewsletterTableViewCell class])
                                    bundle:nil];
    [self.newsLetterTableView registerNib:cellNib
                   forCellReuseIdentifier:@"NewsletterTableViewCell"];
    self.newsLetterTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.newsLetterTableView.estimatedRowHeight = 72;
    
     self.newsLetterTableView.tableFooterView = [UIView new];
    
    
    
    data = [[NSMutableArray alloc] init];
    
    [self callWebservice];
    
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startRefresh:)forControlEvents:UIControlEventValueChanged];
    
    [self.newsLetterTableView addSubview:refreshControl];
}

-(IBAction)startRefresh:(id)sender
{
    // [refreshControl endRefreshing];
    
    pageCount = 0;
    [self callWebservice];
}

-(void)callWebservice
{
    pageCount = pageCount +1;
    
    if (pageCount==1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    [[DocumentsListService sharedService] fetchDocumentsListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
               
                
               
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    if (refreshControl.isRefreshing) {
                        [data removeAllObjects];
                        [self.newsLetterTableView reloadData];
                        [refreshControl endRefreshing];
                    }
                 
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"documents"]];
                    
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // code here
                                [spinnerFooter removeFromSuperview];
                                [self.newsLetterTableView reloadData];
                                
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                            });
                 
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.newsLetterTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }


        
        
    }];
//    }: {
//
//        NSLog(@"Response %@",response);
//        
//        
//        
//        [data addObjectsFromArray:[[response objectForKey:@"data"] objectForKey:@"documents"]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [spinnerFooter removeFromSuperview];
//            [self.newsLetterTableView reloadData];
//            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//        
//        
//    }];
    
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsletterTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"NewsletterTableViewCell"];
    
    cell.imageIcon.image = [UIImage imageNamed:@"documents"];
    
    cell.name.text = data[indexPath.row][@"document_name"];
    
    NSString * str = data[indexPath.row][@"date"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
    cell.timeLabel.text = arr[1];
    
    cell.createdOn.text = arr[0];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:25];
        
    }
    else
    {
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:20];
        
    }


    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 0.0) {
        // [spinnerFooter startAnimating];
        spinnerFooter = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinnerFooter startAnimating];
        spinnerFooter.frame = CGRectMake(0, 0,self.newsLetterTableView.frame.size.width, 44);
        self.newsLetterTableView.tableFooterView = spinnerFooter;
        [self callWebservice];
        
        //[self methodThatAddsDataAndReloadsTableView];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.urlString = data[indexPath.row][@"file_path"];
    
    [self performSegueWithIdentifier:@"pdfDocumentsViewer" sender:self];
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    NSString *docURL = [NSString stringWithFormat:@"%@",data[indexPath.row][@"file_path"]];
//    
//    NSString *escapedText = [docURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
//    
//    NSArray *parts = [escapedText componentsSeparatedByString:@"/"];
//    NSString *filename = [parts lastObject];
//    
//    NSURL *fileURL = [[NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:filename]] absoluteURL];
//    NSString *filePath = [NSString stringWithFormat:@"%@",[fileURL path]];
//    
//    
//    NSURL *localFilePath = [NSURL URLWithString:filePath];
//    
//    bool b=[[NSFileManager defaultManager] fileExistsAtPath:[localFilePath path]];
//    
//    
//    if (b) {
//        
//         [MBProgressHUD hideHUDForView:self.view animated:YES];
//        
//        NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
//        
//        
//        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
//        
//        if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
//        {
//            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
//            
//            readerViewController.delegate = self; // Set the ReaderViewController delegate to self
//            
//            //  readerViewController.READER_FLAT_U
//            
//#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
//            
//            [self.navigationController pushViewController:readerViewController animated:YES];
//            
//#else // present in a modal view controller
//            
//            readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
//            
//            [self presentViewController:readerViewController animated:YES completion:NULL];
//            
//#endif // DEMO_VIEW_CONTROLLER_PUSH
//        }
//        else // Log an error so that we know that something went wrong
//        {
//            NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, filePath, phrase);
//        }
//    }
//    else
//    {
//        [self downloadFileCircle:escapedText];
//    }
//
    
}


#pragma mark - ReaderViewControllerDelegate methods

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
    
    [self.navigationController popViewControllerAnimated:YES];
    
#else // dismiss the modal view controller
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
#endif // DEMO_VIEW_CONTROLLER_PUSH
}

-(void)downloadFileCircle:(NSString *)UrlAddress
{
    
    NSArray *parts = [UrlAddress componentsSeparatedByString:@"/"];
    NSString *filename = [parts lastObject];
    
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    // return [];
    NSString *filePath = [NSString stringWithFormat:@"%@",[documentsDirectoryURL URLByAppendingPathComponent:filename]];
    
    
    // NSLog(@"File Path %@",filePath);
    
    NSURL *localFilePath = [NSURL URLWithString:filePath];
    
    bool b=[[NSFileManager defaultManager] fileExistsAtPath:[localFilePath path]];
    
    if (b) {
        NSLog(@"File Path %@",filePath);
    }
    else
    {
        
        
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        NSURL *URL = [NSURL URLWithString:UrlAddress];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            NSURL *fileURL = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[response suggestedFilename]]];
            //  NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            //  return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
            return fileURL;
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            NSLog(@"File downloaded to: %@", filePath);
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
            
            
            
            NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
            
            
            ReaderDocument *document = [ReaderDocument withDocumentFilePath:[filePath path] password:phrase];
            
            if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
            {
                ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
                
                readerViewController.delegate = self; // Set the ReaderViewController delegate to self
                
                //  readerViewController.READER_FLAT_U
                
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
                
                [self.navigationController pushViewController:readerViewController animated:YES];
                
#else // present in a modal view controller
                
                readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [self presentViewController:readerViewController animated:YES completion:NULL];
                
#endif // DEMO_VIEW_CONTROLLER_PUSH
            }
            else // Log an error so that we know that something went wrong
            {
                NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, filePath, phrase);
            }
            
        }];
        [downloadTask resume];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pdfDocumentsViewer"])
    {
        
        // Get destination view
        PDFViewerViewController *vc = [segue destinationViewController];
        vc.urlString = self.urlString;
        
    }
    
    
    //    }else if ([[segue identifier] isEqualToString:@"addAlbumIdentifier"]) {
    //
    //        // Get destination view
    //        AddAlbumViewController *vc = [segue destinationViewController];
    //        vc.delegate = self;
    //
    //
    //    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
