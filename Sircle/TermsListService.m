//
//  TermsListService.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "TermsListService.h"
#import "AccessToken.h"
#import <UIKit/UIKit.h>
#import "URLS.h"

#define photoListURL @"user/get_all_terms"

@implementation TermsListService



static TermsListService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [TermsListService new];
    }
    return groupService;
}

-(void) fetchTermsWithCompletion:(void (^)(NSDictionary *, NSError *))callback
{
    
 //   NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    
//    
//    NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
//    
//    NSDictionary *dict = @{@"regId" : uniqueIdentifier,@"page":pageNumber,@"groupId":joinedComponents,@"month":month,@"year":year};
    
    
    [self.serviceDelegate executeGetWithUrl:photoListURL queryParams:NULL requestHeaders:nil responseClass:NULL callBack:^(NSDictionary *json, NSError *error) {
        if (error) {
            
        }else{
            callback(json, nil);
        }
    }];
}


-(void) fetchTermsWithCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:GetTerms];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@""];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
}




@end
