//
//  GroupManager.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupResponse.h"

@interface GroupManager : NSObject

+ (id) sharedManager;
-(void) fetchGroupsWithCompletion:(void(^)(NSDictionary *result,NSError *error))callback;

@end
