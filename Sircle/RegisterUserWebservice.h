//
//  RegisterUserWebservice.h
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegisterUserWebservice : NSObject
+ (instancetype)sharedService;


-(void) registerUserName:(NSString*)userName Password:(NSString*)password ConfirmPassword:(NSString*)confirmPassword Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;

@end
