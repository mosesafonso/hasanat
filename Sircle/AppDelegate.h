//
//  AppDelegate.h
//  Sircle
//
//  Created by MOSES AFONSO on 04/09/15.
//  Copyright (c) 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) NSString *albumID;
@property (weak, nonatomic) NSString *eventID;

@end

