//
//  AddHolidayEventViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbstractActionSheetPicker;
@interface AddHolidayEventViewController : UIViewController
- (IBAction)startDatePressed:(id)sender;
- (IBAction)endDatePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UIButton *endDate;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@end
