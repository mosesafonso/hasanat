//
//  ForgotPasswordWebservice.h
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForgotPasswordWebservice : NSObject
+ (instancetype)sharedService;


-(void) forgotPasswordWithUserName:(NSString*)userName Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;

@end
