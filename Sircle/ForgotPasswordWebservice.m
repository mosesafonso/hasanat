//
//  ForgotPasswordWebservice.m
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "ForgotPasswordWebservice.h"
#import "URLS.h"

@implementation ForgotPasswordWebservice
static ForgotPasswordWebservice *loginWebservice = nil;
+ (instancetype)sharedService
{
    if (nil ==  loginWebservice) {
        loginWebservice = [ForgotPasswordWebservice new];
    }
    return loginWebservice;
}


-(void) forgotPasswordWithUserName:(NSString*)userName Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;
{
    
    
    
    NSString* urlText = [NSString stringWithFormat:forgotPassword];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    
    
    NSString* userUpdate=[NSString stringWithFormat:@"email=%@",userName];
    NSLog(@"URL::%@",urlTextEscaped);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:loginCallBack] resume];
    
}
@end
