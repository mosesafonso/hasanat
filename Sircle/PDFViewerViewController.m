//
//  PDFViewerViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 04/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "PDFViewerViewController.h"

@interface PDFViewerViewController ()

@end

@implementation PDFViewerViewController
@synthesize urlString;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
