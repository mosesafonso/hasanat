//
//  NotificationListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"
#import "AddCallResponse.h"


@interface NotificationListService : BaseService
+ (instancetype)sharedService;
-(void) fetchNotificationListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;
//- (void) addNotification:(NSString*)title message:(NSString*)message Completion:(void (^)(AddCallResponse *, NSError *))callback;

-(void) fetchNotificationsListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) addNotificationWithTitle:(NSString*)title Message:(NSString*)message Groups:(NSString*)groups Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;




@end
