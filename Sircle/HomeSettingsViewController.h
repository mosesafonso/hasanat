//
//  HomeSettingsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeSettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
- (IBAction)saveGroups:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@end
