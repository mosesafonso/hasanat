//
//  NotificationsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
- (IBAction)addNotificationsCalled:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addMessages;
@property (weak, nonatomic) IBOutlet UITableView *notificationsTableView;
@end
