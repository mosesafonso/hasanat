//
//  LoginRequest.m
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "LoginRequest.h"

@implementation LoginRequest
@synthesize username, password, regId,notiId;

- (NSDictionary*) dictionaryMapping{
    return @{@"regId": self.regId, @"loginId": self.username, @"pwd": self.password,@"noti_id":self.notiId};
}

@end
