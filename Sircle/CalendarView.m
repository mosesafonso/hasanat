//
//  CalendarView.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "CalendarView.h"
#import "CalendarViewService.h"
#import "EventListMonthWise.h"
#import "EventListDateWiseViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+HexaDecimalColors.h"

@interface CalendarView ()
{
    NSMutableDictionary *_eventsByDate;
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    NSDate *_dateSelected;
    NSMutableArray *data;
    NSString *currenttTermNumber;
    NSString *currentTermType;
    NSDate *currentTermDate;
}

@end

@implementation CalendarView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    data = [[NSMutableArray alloc] init];
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    
   // _calendarManager.settings.pageViewHaveWeekDaysView = NO; // You don't want WeekDaysView in the contentView
    _calendarManager.settings.pageViewNumberOfWeeks = 0; // Automatic number of weeks
    
   // _weekDayView.manager = _calendarManager;
    
    // Generate random events sort by date using a dateformatter for the demonstration
   [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
   [self createMinAndMaxDate];
    
    
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:_todayDate];
    
  //  NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:_todayDate]; // Get necessary date components
    
    ; //gives you month
  //  [components day]; //gives you day
    [components year];
    
   
    
    [self callWebserviceWithMonth: [NSString stringWithFormat:@"%ld",(long)[components month]] andyear: [NSString stringWithFormat:@"%ld",(long)[components year]]];
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        self.todayButton.hidden= YES;
    }
    
   }

-(void)callWebserviceWithMonth:(NSString*)month andyear:(NSString*)year
{
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[CalendarViewService sharedService] fetchEventDatesForMonth:month ForYear:year WithPage:@"1" Completion:^(NSDictionary *response, NSError *error) {
//        NSLog(@"response %@",response);
//        
//        _eventsByDate = [NSMutableDictionary new];
//        
//        data = [[response objectForKey:@"data"] objectForKey:@"events"];
//        
//        [[EventListMonthWise classMethodeOfEventListMonthWise] setEventListMonthWise:data];
//        
//        for(int i = 0; i < data.count; ++i){
//            // Generate 30 random dates between now and 60 days later
//            // NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
//            
//            // Use the date as key for eventsByDate
//            //  NSString *key = [[self dateFormatter] stringFromDate:randomDate];
//            
//            NSString *key = data[i][@"start_date"];
//            
//            NSDate * randomDate = [[self dateFormatter] dateFromString:key];
//            
//            if(!_eventsByDate[key]){
//                _eventsByDate[key] = [NSMutableArray new];
//            }
//            
//            [_eventsByDate[key] addObject:randomDate];
//        }
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [_calendarManager reload];
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            
//            
//        });
//        
//      
//    }];
    
    [[CalendarViewService sharedService] fetchEventDatesForMonth:month ForYear:year Page:@"1" Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                _eventsByDate = [NSMutableDictionary new];
                
                data = [jsonLoginDictionary objectForKey:@"data"];
                
                [[EventListMonthWise classMethodeOfEventListMonthWise] setEventListMonthWise:data];
                
                for(int i = 0; i < data.count; ++i){
                    // Generate 30 random dates between now and 60 days later
                    // NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
                    
                    // Use the date as key for eventsByDate
                    //  NSString *key = [[self dateFormatter] stringFromDate:randomDate];
                    
                    
                  //  NSString * str = @"Hi Hello How Are You ?";
                    NSArray * arrayStartDate = [data[i][@"event_start_date"] componentsSeparatedByString:@" "];
                     NSArray * arrayEndDate = [data[i][@"event_end_date"] componentsSeparatedByString:@" "];
                   // NSLog(@"Array values are : %@",arr);
                    
                    
                    if ([arrayStartDate[0] isEqualToString:arrayEndDate[0]]) {
                        
                        
                        NSString *key = arrayStartDate[0];
                        
                        NSDate * randomDate = [[self dateFormatter] dateFromString:key];
                        
                        
                        
                        if(!_eventsByDate[key]){
                            _eventsByDate[key] = [NSMutableArray new];
                        }
                        
                        [_eventsByDate[key] addObject:randomDate];
                    }
                    
                    else
                    {
                        
                        
                        
                        NSString *key = arrayStartDate[0];
                        
                        NSDate * randomDate = [[self dateFormatter] dateFromString:key];
                        
                        
                        
                        if(!_eventsByDate[key]){
                            _eventsByDate[key] = [NSMutableArray new];
                        }
                        
                        [_eventsByDate[key] addObject:randomDate];

                        
                        NSDate *minDate = [[self dateFormatter] dateFromString:arrayStartDate[0]];
                        
                        NSDate *maxDate = [[self dateFormatter] dateFromString:arrayEndDate[0]];
                        
                        
                         NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
                         NSDateComponents *days = [[NSDateComponents alloc] init];
                         NSInteger dayCount = 0;
                         while ( TRUE ) {
                         [days setDay: ++dayCount];
                         NSDate *date = [gregorianCalendar dateByAddingComponents: days toDate: minDate options: 0];
                         if ( [date compare: maxDate] == NSOrderedDescending )
                         {
                         break;
                         }
                             else
                             {
                               
                              
                                 NSString *key = [[self dateFormatter] stringFromDate:date];
                                 
    
                                 if(!_eventsByDate[key]){
                                     _eventsByDate[key] = [NSMutableArray new];
                                 }
                                 
                                 [_eventsByDate[key] addObject:date];
                             }
                         // Do something with date like add it to an array, etc.
                         }
                    }
                   
                    
                    
                 
                    
                
                }
                

                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                   
                    
                    [_calendarManager reload];
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                   // [self.navigationController popViewControllerAnimated:YES];
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }
        

        
    }];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"eventDateWiseIdentifier"]) {
        
        // Get destination view
        EventListDateWiseViewController *vc = [segue destinationViewController];
        vc.selectedDate = _dateSelected;
    }
    
}


- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-2];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:2];
}


- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
        
            dayView.dotView.backgroundColor = [UIColor clearColor];
          // dayView.dotView.layer.cornerRadius = dayView.dotView.frame.size.width/2;
          // dayView.dotView.clipsToBounds = YES;
//            dayView.dotView.layer.borderColor = [UIColor colorWithHexString:@"CBCBCB"].CGColor;
        dayView.dotView.layer.borderColor = [UIColor colorWithHexString:@"339966"].CGColor;

            dayView.dotView.layer.borderWidth = 1.0f;
        
        //dayView.circleView.hidden = NO;
       // dayView.circleView.backgroundColor = [UIColor colorWithHexString:@"F87177"];
   
       //dayView.textLabel.textColor = [UIColor whiteColor];
    }
    else{
        dayView.dotView.hidden = YES;
    }
    
    if([_calendarManager.dateHelper date:dayView.date isTheSameDayThan:currentTermDate]){
//        dayView.circleView.hidden = NO;
//        dayView.circleView.backgroundColor = [UIColor blueColor];
//        dayView.dotView.backgroundColor = [UIColor whiteColor];
//        dayView.textLabel.textColor = [UIColor whiteColor];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30,20)];
        
        if ([currentTermType isEqualToString:@"End"]) {
            label.textColor = [UIColor redColor];
        }
        else
        {
            label.textColor = [UIColor greenColor];
        }
        
        label.text = currenttTermNumber;
        
        
        
        [dayView addSubview:label];

    }
    
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor colorWithHexString:@"339966"];
        dayView.dotView.backgroundColor = [UIColor clearColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
//        dayView.circleView.hidden = NO;
//        dayView.circleView.backgroundColor = [UIColor redColor];
//        dayView.dotView.backgroundColor = [UIColor whiteColor];
//        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        //dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        //dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
    
    NSLog(@"Date Selected %@",_dateSelected);
    
    [self performSegueWithIdentifier:@"eventDateWiseIdentifier" sender:self];
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
//- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
//{
//    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
//}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Next page loaded");
    
    NSCalendar* calendar1 = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar1 components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate: calendar.date]; // Get necessary date components
    
    
    
    [self callWebserviceWithMonth: [NSString stringWithFormat:@"%ld",(long)[components month]] andyear: [NSString stringWithFormat:@"%ld",(long)[components year]]];
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Previous page loaded");
    
    NSCalendar* calendar1 = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar1 components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate: calendar.date]; // Get necessary date components
    
   
    
    [self callWebserviceWithMonth: [NSString stringWithFormat:@"%ld",(long)[components month]] andyear: [NSString stringWithFormat:@"%ld",(long)[components year]]];

}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
       // dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm aa";
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
   
}

- (IBAction)TodayButtonClicked:(id)sender {
    

    
     [_calendarManager setDate:[NSDate date]];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:[NSDate date]]; // Get necessary date components
    
    ; //gives you month
    //  [components day]; //gives you day
    [components year];
    
    
    
    [self callWebserviceWithMonth: [NSString stringWithFormat:@"%ld",(long)[components month]] andyear: [NSString stringWithFormat:@"%ld",(long)[components year]]];
}

-(void)TermDateSelected:(NSString*)termDate TermNumber:(NSString*)termNumber TermPeriod:(NSString*)termPeriod;
{
   // NSString *dateStr = @"Tue, 25 May 2010 12:53:58 +0000";
    
    // Convert string to date object
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [dateFormat dateFromString:termDate];
    
    currentTermDate = date;
    currenttTermNumber = termNumber;
    currentTermType = termPeriod;
    
    [_calendarManager setDate:date];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth fromDate:date]; // Get necessary date components
    
    ; //gives you month
    //  [components day]; //gives you day
    [components year];
    
    
    
    [self callWebserviceWithMonth: [NSString stringWithFormat:@"%ld",(long)[components month]] andyear: [NSString stringWithFormat:@"%ld",(long)[components year]]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
//{
//    JTCalendarDayView *view = [JTCalendarDayView new];
//  //  view.textLabel.font = [UIFont fontWithName:@"Avenir-Light" size:20];
//  //  view.textLabel.textColor = [UIColor redColor];
//    
//    if ([view. compare:currentTermDate] == NSOrderedSame) {
//        
//       //
//        NSLog(@"Date %@",view.date);
//
//    }
//    
//    return view;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
