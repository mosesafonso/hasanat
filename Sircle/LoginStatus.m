//
//  LoginStatus.m
//  STYFI
//
//  Created by MOSES AFONSO on 24/09/15.
//  Copyright © 2015 Stylabs. All rights reserved.
//

#import "LoginStatus.h"

@implementation LoginStatus

static LoginStatus* _loginStatus;


+(LoginStatus*)classMethodeOfLoginStatus
{
    if (_loginStatus==nil)
    {
        _loginStatus=[[LoginStatus alloc]init];
        
    }
    return _loginStatus;

}

-(void)setLoginStatusValue:(BOOL)status
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setBool:status  forKey:@"LoginStatus"];
    
    [defaults synchronize];

}

-(BOOL)getLoginStatus
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults boolForKey:@"LoginStatus"];
   
}

-(void)setUserTypeValue:(NSString*)status
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:status forKey:@"UserType"];
    
    [defaults synchronize];
    
}

-(NSString*)getUserType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults stringForKey:@"UserType"];
}

@end
