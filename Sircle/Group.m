//
//  Group.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "Group.h"

@implementation Group

@synthesize groupId, groupName, active;

- (NSDictionary*) dictionaryMapping{
    return @{@"group_id": self.groupId, @"group_name": self.groupName, @"active": self.active};
    
}

@end
