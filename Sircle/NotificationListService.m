//
//  NotificationListService.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "NotificationListService.h"
#import <UIKit/UIKit.h>
#import "AddCallResponse.h"
#import "URLS.h"
#import "AccessToken.h"

#define notificationListURL @"user/notifications_group"
#define addNotificationListURL @"user/add_notification"


@implementation NotificationListService
static NotificationListService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [NotificationListService new];
    }
    return groupService;
}

-(void) fetchNotificationListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback{
    
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
    
    NSDictionary *dict = @{@"regId" : uniqueIdentifier,@"page":pageNumber,@"groupId":joinedComponents};
    
    [self.serviceDelegate executeGetWithUrl:notificationListURL queryParams:dict requestHeaders:nil responseClass:NULL callBack:^(NSDictionary *json, NSError *error) {
        if (error) {
            
        }else{
            callback(json, nil);
        }
    }];
}

-(void) fetchNotificationsListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:NotificationsListUrl];
    
    
    NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"page=%@",pageNumber];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
    
}


//- (void) addNotification:(NSString*)title message:(NSString*)message Completion:(void (^)(NSDictionary *, NSError *))callback{
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
//
//    NSDictionary *dict = @{ @"grp":joinedComponents, @"msg" : message, @"subject" : title};
//    [self.serviceDelegate executePostWithUrl:addNotificationListURL request:dict responseClass:[AddCallResponse class] callBack:^(id result, NSError *error) {
//        if (error) {
//            
//        }else{
//            callback(result, nil);
//        }
//    }];
//}

-(void) addNotificationWithTitle:(NSString*)title Message:(NSString*)message Groups:(NSString*)groups Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:AddNotifications];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"notification_title=%@&notification_message=%@&group_id=%@",title,message,groups];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
}


@end
