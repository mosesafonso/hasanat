//
//  TermsListViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TermsListViewControllerDelegate <NSObject>

-(void)TermDateSelected:(NSString*)date TermNumber:(NSString*)termNumber TermPeriod:(NSString*)termPeriod;

@end

@interface TermsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *termsTableView;
@property(nonatomic,weak)id<TermsListViewControllerDelegate> delegate;
@end
