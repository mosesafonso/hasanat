//
//  AddSchoolEventViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 18/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddSchoolEventViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "SettingsHeaderTableViewCell.h"
#import "GroupManager.h"
#import "Group.h"
#import "GroupSaveService.h"
#import "ActionSheetPicker.h"
#import "NSDate+TCUtils.h"
#import "CalendarViewService.h"
#import "MBProgressHUD.h"
#import "GroupService.h"

@interface AddSchoolEventViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *groupNames;
@property (nonatomic,strong) NSMutableArray *activeGroupIds;
@end

@implementation AddSchoolEventViewController
@synthesize actionSheetPicker = _actionSheetPicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
    
    navigationControllerObject.navigationView.hidden=YES;
    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([SettingsHeaderTableViewCell class])
                                    bundle:nil];
    [self.settingsTableView registerNib:cellNib
                 forCellReuseIdentifier:@"SettingsHeaderTableViewCell"];
    self.settingsTableView.rowHeight = 60;
    
    self.settingsTableView.estimatedRowHeight = 60;
    
    self.groupNames = [[NSMutableArray alloc] init];
    self.activeGroupIds = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    
    self.settingsTableView.tableFooterView = [UIView new];
    
    self.settingsTableView.layer.borderWidth = 1.0;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    // self.navigationController.navigationBar.topItem.title = @"Holiday";
    
    self.navigationItem.title = @"School Holiday";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[GroupManager sharedManager] fetchGroupsWithCompletion:^(NSDictionary *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            // id obj = result.data;
//            //            for (Group* group in obj) {
//            //                [self.groupNames addObject:group.groupName];
//            //            }
//            
//            NSLog(@"Data %@",[result objectForKey:@"data"]);
//            
//            [self.groupNames addObjectsFromArray:[result objectForKey:@"data"]];
//            
//            
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                CGFloat height = self.settingsTableView.rowHeight;
//                height *= self.groupNames.count;
//                
//                self.tableViewHeight.constant = height+self.settingsTableView.rowHeight;
//              
//                [self.settingsTableView reloadData];
//                
//                 [MBProgressHUD hideHUDForView:self.view animated:YES];
//            });
//            
//        }
//    }];
    
    [[GroupService sharedService] fetchGroupsListWithCompletion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    [self.groupNames addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"groups"]];
                    
                    CGFloat height = self.settingsTableView.rowHeight;
                                    height *= self.groupNames.count;
                    
                                    self.tableViewHeight.constant = height+self.settingsTableView.rowHeight;
                    
                    for (int i =0; i<self.groupNames.count; i++) {
                        if ([[NSString stringWithFormat:@"%@",self.groupNames[i][@"select"]] isEqualToString:@"1"]) {
                            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //   [spinnerFooter removeFromSuperview];
                        //   [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //  [spinnerFooter removeFromSuperview];
                        //  [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
    }];

    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsHeaderTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    cell.groupNameLabel.text = self.groupNames[indexPath.row][@"name"];
    
    
    
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]]) {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    else
    {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    
    [cell.radioButton setTag:indexPath.row];
    [cell.radioButton addTarget:self action:@selector(checkOrUncheckGroup:) forControlEvents: UIControlEventTouchUpInside];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    else
    {
        
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    
    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
    
    // [headerCell setTag:section];
    return headerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(IBAction)selectAllGroups:(id)sender
{
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [self.activeGroupIds removeAllObjects];
        for (int i=0; i<self.groupNames.count; i++) {
            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
            [self.settingsTableView reloadData];
        }
    }
    else
    {
        [self.activeGroupIds removeAllObjects];
        [self.settingsTableView reloadData];
        
    }
}


-(IBAction)checkOrUncheckGroup:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    SettingsHeaderTableViewCell *cell = (SettingsHeaderTableViewCell*)[self.settingsTableView cellForRowAtIndexPath:indexPath];
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]])
    {
        [self.activeGroupIds removeObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
        
    }
    else
    {
        [self.activeGroupIds addObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    // 1. Dequeue the custom header cell
//    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
//    }
//    else
//    {
//
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
//    }
//
//    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
//
//    // [headerCell setTag:section];
//    return headerCell;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 60;
//}


//-(IBAction)selectAllGroups:(id)sender
//{
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [self.activeGroupIds removeAllObjects];
//        for (int i=0; i<self.groupNames.count; i++) {
//            [self.activeGroupIds addObject:self.groupNames[i][@"group_id"]];
//            [self.settingsTableView reloadData];
//        }
//    }
//    else
//    {
//        [self.activeGroupIds removeAllObjects];
//        [self.settingsTableView reloadData];
//
//    }
//}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)startDatePressed:(id)sender {
    
    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(StartDateWasSelected:) origin:sender];
    
    
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)endDatePressed:(id)sender {
    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(EndDateWasSelected:) origin:sender];
    
    
    [self.actionSheetPicker showActionSheetPicker];
}

- (void)StartDateWasSelected:(NSDate *)selectedDate{
    // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    // self.dateTextField.text = [self.selectedDate description];
    
    
    
    [self.startDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (void)EndDateWasSelected:(NSDate *)selectedDate{
    // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    // self.dateTextField.text = [self.selectedDate description];
    
    
    
    [self.endDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm:ss aa";
    }
    
    return dateFormatter;
}


- (IBAction)addEvent:(id)sender {
    // {event_type:event_type_id,title:title,loc:loc,event_cat:event_cat,grp:grp,strdate:strdate,enddate:enddate,strtime:strtime,endtime:endtime,detail:det,rem_days:rem_days,rem_hours:rem_hours,rem_mins:rem_mins,repeats:repeats,repeat_type_id:repeat_type_id,repeat_type:repeat_type_name, repeat_every : number, repeat_end_type_id: end_type_id, repeat_end_type : end_type_text, rep_after_occurence, rep_ondate, repeat_week_days: (Array), grp, repeat_monthly_on
    if (self.titleTextField.text != nil && (self.activeGroupIds != nil || self.activeGroupIds.count > 0)) {
        
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
//        NSDictionary *dict = @{@"event_type" : @3, @"title" : @"event", @"grp" : [self.activeGroupIds componentsJoinedByString:@","], @"strdate" : self.startDate.titleLabel.text, @"enddate" : self.endDate.titleLabel.text };
//        
//        //{event_type:event_type_id,title:title,strdate:strdate,enddate:enddate,grp:grp}
//        
//        [[CalendarViewService sharedService] addEventWithRequest:dict Completion:^(AddCallResponse *result, NSError *error) {
//            if (error) {
//                
//            }else{
//                int status = [result.status intValue];
//                if (status == 200){
//                    // success
//                    
//                    NSLog(@"success add event");
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // code here
//                        //   [_calendarManager reload];
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        [self.navigationController popViewControllerAnimated:YES];
//                        
//                        
//                    });
//                }
//            }
//        }];
        
        
          NSString *joinedComponents = [self.activeGroupIds componentsJoinedByString:@","];
        
        [[CalendarViewService sharedService] AddSchoolHolidayEventWithTitle:self.titleTextField.text  StartDate:self.startDate.titleLabel.text EndDate:self.endDate.titleLabel.text Groups:joinedComponents Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
            
            if (error) {
            }else{
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        [self.navigationController popViewControllerAnimated:YES];
                        
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
            }
            
            
        }];
        
        
    }else{
        // alert action
    }

    
    
}


#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.titleTextField resignFirstResponder];
    //  [self.passwordTextFeild resignFirstResponder];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
    
    if (textField==self.titleTextField) {
        
        
        
        if([[textField text] length] > 72)
        {
            return NO;
        }
        
    }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
    
    
    return YES;
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
