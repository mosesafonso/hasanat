//
//  AddCallResponse.m
//  Sircle
//
//  Created by Soniya Gadekar on 16/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddCallResponse.h"

@implementation AddCallResponse

@synthesize status, message, data;

- (NSDictionary*) dictionaryMapping{
    return @{@"status": self.status, @"message": message, @"data" : data};
    
}
@end
