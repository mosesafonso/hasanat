//
//  RegisterUserWebservice.m
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "RegisterUserWebservice.h"
#import "URLS.h"

@implementation RegisterUserWebservice

static RegisterUserWebservice *loginWebservice = nil;
+ (instancetype)sharedService
{
    if (nil ==  loginWebservice) {
        loginWebservice = [RegisterUserWebservice new];
    }
    return loginWebservice;
}


-(void) registerUserName:(NSString*)userName Password:(NSString*)password ConfirmPassword:(NSString*)confirmPassword Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;
{
    
    
    
    NSString* urlText = [NSString stringWithFormat:registerUser];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    
    
    NSString* userUpdate=[NSString stringWithFormat:@"email=%@&password=%@&confirm_password=%@",userName,password,confirmPassword];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:loginCallBack] resume];
    
}

@end
