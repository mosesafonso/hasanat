//
//  LoginWebservice.m
//  Sircle
//
//  Created by MOSES AFONSO on 26/05/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "LoginWebservice.h"
#import "URLS.h"
#import "AccessToken.h"

#ifdef DEBUG
#define IS_DEV @"0"
#else
#define IS_DEV @"1"
#endif


@implementation LoginWebservice
static LoginWebservice *loginWebservice = nil;
+ (instancetype)sharedService
{
    if (nil ==  loginWebservice) {
        loginWebservice = [LoginWebservice new];
    }
    return loginWebservice;
}

-(void) loginWithUserName:(NSString*)userName Password:(NSString*)password DeviceToken:(NSString*)deviceToken DeviceType:(NSString*)deviceType Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack
{
    
 
    
    NSString* urlText = [NSString stringWithFormat:LoginUrl];
    
    NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
   // NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    
    // is_dev
    
    NSString* userUpdate=[NSString stringWithFormat:@"email=%@&password=%@&device_token=%@&device_type=%@&is_dev=%@",userName,password,deviceToken,deviceType,IS_DEV];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:loginCallBack] resume];
    
}

-(void) logoutWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:LogoutUser];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    [request setHTTPMethod:@"GET"];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:loginCallBack] resume];
}

@end
