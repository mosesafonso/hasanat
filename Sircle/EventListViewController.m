//
//  EventListViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "EventListViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "EventListDateWiseTableViewCell.h"
#import "EventListMonthWise.h"
#import "EventDetailsViewController.h"
//#import "MBProgressHUD.h"

@interface EventListViewController ()
{
    NSMutableArray *data;
}
@end

@implementation EventListViewController
@synthesize messageLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     messageLbl = [[UILabel alloc]init];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([EventListDateWiseTableViewCell class])
                                    bundle:nil];
    [self.eventsTableView registerNib:cellNib
               forCellReuseIdentifier:@"EventListDateWiseTableViewCell"];
    
    self.eventsTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.eventsTableView.estimatedRowHeight = 94;
    
    self.eventsTableView.tableFooterView = [UIView new];

}

-(void)viewDidAppear:(BOOL)animated
{
    data = [[EventListMonthWise classMethodeOfEventListMonthWise] returnData];
    [self.eventsTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (data.count == 0) {
        //create a lable size to fit the Table View
        messageLbl.frame =CGRectMake(0, 0,self.eventsTableView.bounds.size.width,self.eventsTableView.bounds.size.height);
        //set the message
        messageLbl.text = @"No Events Today";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        //auto size the text
        [messageLbl sizeToFit];
        
        //set back to label view
        self.eventsTableView.backgroundView = messageLbl;
        //no separator
        self.eventsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        // return 0;
    }
    else
    {
        self.eventsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        messageLbl.text=@"";
    }
    
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventListDateWiseTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"EventListDateWiseTableViewCell"];
    
    cell.title.text = data[indexPath.row][@"event_title"];
    
    NSString * str = data[indexPath.row][@"event_start_date"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
    cell.startDate.text = arr[0];
    
    
    cell.startTime.text = arr[1];
    
    str = data[indexPath.row][@"event_end_date"];
    arr = [str componentsSeparatedByString:@" "];
    
     cell.endTime.text = arr[1];
    
   //
    
   
    if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"event_type"] ] isEqualToString:@"N"])
    {
        cell.startTime.hidden = NO;
        cell.endTime.hidden = NO;
        
    }
    
    
  //  cell.startDate.text = data[indexPath.row][@""];
    
    
    
    if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Arts"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"arts"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Sports"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"sports"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"3"]) {
        //[cell.eventIcon setImage:[UIImage imageNamed:@"arts"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Academics"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"academics"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Performance"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"performance"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Others"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"others"]];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        cell.title.font = [UIFont fontWithName:@"Roboto-Light" size:25];
    }
    else
    {
        cell.title.font = [UIFont fontWithName:@"Roboto-Light" size:20];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if ([data[indexPath.row][@"video_from"] isEqualToString:@"youtube"]) {
//        self.videoID = data[indexPath.row][@"video_provider_id"];
//        [self performSegueWithIdentifier:@"YouTubeVideoPlayerIdentifier" sender:self];
//    }
//    
//    else if ([data[indexPath.row][@"video_from"] isEqualToString:@"vimeo"]) {
//        self.videoID = data[indexPath.row][@"video_provider_id"];
//        // [self performSegueWithIdentifier:@"YouTubeVideoPlayerIdentifier" sender:self];
//        [YTVimeoExtractor fetchVideoURLFromURL:[NSString stringWithFormat:@"http://vimeo.com/%@",self.videoID] quality:self.quality completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
//            if (error) {
//                NSLog(@"Error : %@", [error localizedDescription]);
//            } else if (videoURL) {
//                NSLog(@"Extracted url : %@", [videoURL absoluteString]);
//                NSLog(@"Extracted with quality : %@", @[@"Low", @"Medium", @"HD 720", @"HD 1080"][quality]);
//                
//                dispatch_async (dispatch_get_main_queue(), ^{
//                    self.playerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
//                    [self.playerView.moviePlayer prepareToPlay];
//                    [self presentViewController:self.playerView animated:YES completion:^(void) {
//                        self.playerView = nil;
//                    }];
//                });
//            }
//        }];
//    }
    
    self.eventID = data[indexPath.row][@"event_id"];
     [self performSegueWithIdentifier:@"eventDetailsIdentifier" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"eventDetailsIdentifier"]) {
        
        // Get destination view
        EventDetailsViewController *vc = [segue destinationViewController];
        vc.eventID = self.eventID;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
