//
//  PhotoSingleViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "PhotoSingleViewController.h"
#import "SinglePhotoCollectionViewCell.h"
#import "UIImageView+WebCache.h"


@interface PhotoSingleViewController ()

@end

@implementation PhotoSingleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupCollectionView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupCollectionView {
  //  [self.photosCollectionView registerClass:[SinglePhotoCollectionViewCell class] forCellWithReuseIdentifier:@"SinglePhotoCollectionViewCell"];
    
       [self.photosCollectionView registerNib:[UINib nibWithNibName:@"SinglePhotoCollectionViewCell" bundle:nil]   forCellWithReuseIdentifier: @"SinglePhoto"];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.photosCollectionView setPagingEnabled:YES];
    [self.photosCollectionView setCollectionViewLayout:flowLayout];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SinglePhotoCollectionViewCell *cell = (SinglePhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"SinglePhoto" forIndexPath:indexPath];
    
    
    NSString * urlTextEscaped = [[NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"file_path"]]  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [cell.photo sd_setImageWithURL:[NSURL URLWithString:urlTextEscaped] placeholderImage: [UIImage imageNamed:@"imagePlaceholder"]];
    
      cell.caption.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"file_name"]];
    

    return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.photosCollectionView.frame.size;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
