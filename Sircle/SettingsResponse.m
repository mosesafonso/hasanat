//
//  SettingsResponse.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "SettingsResponse.h"

@implementation SettingsResponse

@synthesize status, message, data;

- (NSDictionary*) dictionaryMapping{
    return @{@"status": self.status, @"message": message, @"data": data};
    
}

//user/notifications_group
@end
