//
//  GroupSaveRequest.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseModel.h"

@interface GroupSaveRequest : BaseModel

@property (nonatomic, strong) NSString *regId;
@property (nonatomic, strong) NSString *groupValString;


- (NSDictionary*) dictionaryMapping;

@end
