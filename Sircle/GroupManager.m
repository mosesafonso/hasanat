//
//  GroupManager.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "GroupManager.h"
#import "GroupService.h"

@implementation GroupManager

static GroupManager *groupManager = nil;

+ (instancetype)sharedManager {
    
    if (nil ==  groupManager) {
        groupManager = [GroupManager new];
    }
    return groupManager;
}

-(void) fetchGroupsWithCompletion:(void (^)(NSDictionary *, NSError *))callback{
    [[GroupService sharedService] fetchGroupsWithCompletion:^(NSDictionary *response, NSError *error) {
        if (error) {
            callback(nil, error);
        }else{
            callback(response, nil);
        }
    }];
}

@end
