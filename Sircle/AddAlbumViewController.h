//
//  AddAlbumViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 16/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddAlbumViewControllerDelegate <NSObject>

-(void)completionOfAddalbum;

@end

@interface AddAlbumViewController : UIViewController
@property(nonatomic,weak)id<AddAlbumViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *addNameTExtFeild;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@property (weak, nonatomic) NSString *albumID;
@property(nonatomic,strong) UIImage *sImage;
@end
