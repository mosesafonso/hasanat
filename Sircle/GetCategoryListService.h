//
//  GetCategoryListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 18/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface GetCategoryListService : BaseService
+ (instancetype)sharedService;
-(void) fetchCategoriesWithCompletion:(void (^)(NSDictionary *, NSError *))callback;

-(void) fetchCategoriesWithCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
