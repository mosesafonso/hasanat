//
//  HomeViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 10/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "HomeViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "EventDetailsViewController.h"
#import "AlbumGridViewController.h"
#import "LoginStatus.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // self.navigationController.navigationBar.hidden = NO;
     [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
   //  [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];

    
  
        self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
        self.navigationController.navigationBar.translucent = NO;
     [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    self.navigationController.navigationItem.backBarButtonItem.enabled = NO;
    
    
        
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"HOME";
    
    [self customSetup];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
     [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    if (self.eventID.length!=0) {
        [self goToEvent];
    }
//    
//    UIAlertController * alert =   [UIAlertController
//                                   alertControllerWithTitle:@"User Info"
//                                   message:self.albumID
//                                   preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* ok = [UIAlertAction
//                         actionWithTitle:@"OK"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                             //Do some thing here
//                             
//                             
//                             
//                             //  [self.navigationController popViewControllerAnimated:YES];
//                             
//                             //  [self.delegate completionOfAddalbum];
//                             
//                             
//                         }];
//    [alert addAction:ok];
//    
//    [self presentViewController:alert animated:YES completion:nil];
    
    if (self.albumNotiID.length!=0)
    {
        [self goToAlbum];
    }
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"HomeEventDetailIdentifier"]) {
        
        // Get destination view
        EventDetailsViewController *vc = [segue destinationViewController];
        vc.eventID = self.eventID;
        self.eventID = @"";
    }
    
    if ([[segue identifier] isEqualToString:@"HomeAlbumIdentifier"]) {
        
        // Get destination view
        AlbumGridViewController *vc = [segue destinationViewController];
        vc.albumID = self.albumNotiID;
        self.albumNotiID = @"";
        
        
    }
    
    
}



-(void)goToEvent
{
    [self performSegueWithIdentifier:@"HomeEventDetailIdentifier" sender:self];
}

-(void)goToAlbum
{
    [self performSegueWithIdentifier:@"HomeAlbumIdentifier" sender:self];
}

- (void)customSetup
{
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailLabelTapped:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [_emailLabel addGestureRecognizer:tapGestureRecognizer];
    _emailLabel.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneLabelTapped:)];
    tapGestureRecognizer1.numberOfTapsRequired = 1;
    [_numberLabel addGestureRecognizer:tapGestureRecognizer1];
    _numberLabel.userInteractionEnabled = YES;
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
       // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
                [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}


- (void)emailLabelTapped:(UITapGestureRecognizer *)tapGesture {
     [self callMailClient];
}


- (void)phoneLabelTapped:(UITapGestureRecognizer *)tapGesture {
   // [self callMailClient];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+919819737252"]];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openMailClient:(id)sender {
  
    [self callMailClient];
}

-(void)callMailClient
{
    if ([MFMailComposeViewController canSendMail]) {
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@snaptech.in"];
        [mailComposer setToRecipients:toRecipents];
        //  [mailComposer setSubject:@""];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    else
    {
        NSString *recipients = @"mailto:support@snaptech.in";
        
        //?subject=Feedback";
        
        
        NSString *email = [NSString stringWithFormat:@"%@", recipients];
    
        
        email = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
        
    }
}

#pragma mark - mail compose delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (IBAction)settingsButtonClicked:(id)sender {
    
   //
    if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
        [self performSegueWithIdentifier:@"settingsFromHomeIdentifier" sender:self];
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert"
                                      message:@"This Feature is not available to the Admin"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Dismiss"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        

    

    }
}
@end
