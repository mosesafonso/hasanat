//
//  URLS.h
//  Sircle
//
//  Created by MOSES AFONSO on 26/05/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#ifndef URLS_h
#define URLS_h



#define BaseURL @"https://hasanat.emissioapp.com/api/"

//#define BaseURL @"https://demo.emissioapp.com/api/"

//Login Url
#define LoginUrl BaseURL @"login"


//DocumentsList Url
#define DocumentsListUrl BaseURL @"file/getDocuments"

//NewslettersList Url
#define NewsLettersListUrl BaseURL @"file/getNewsPapers"

//VideosList Url
#define VideosListUrl BaseURL @"file/getVideos"

//PhotosList Url
#define PhotosListUrl BaseURL @"file/album"

//NotificationsList Url
#define NotificationsListUrl BaseURL @"notification"

//LinksList Url
#define LinksListUrl BaseURL @"link"

#define GroupsListURL BaseURL @"login/setting"

#define AddNotifications BaseURL @"notification/add"

#define AddLink BaseURL @"link/add"

#define AddAlbum BaseURL @"file/addAlbum"

#define LogoutUser BaseURL @"login/logout"

#define DeleteEvent BaseURL @"event/delete"

#define EventDetails BaseURL @"event/eventDetails"

#define AddEvent BaseURL @"event/add"

#define EventList BaseURL @"event"

#define GetCategories BaseURL @"category"

#define GetTerms BaseURL @"term"

#define GetAlbumImages BaseURL @"file/getAlbumImages"

#define addPhotoURL BaseURL @"file/addImage"

#define forgotPassword BaseURL @"login/forgot_password"

#define registerUser BaseURL @"login/register"

#define Time_Interval 60

#define ForceUpdateUrl BaseURL @"notification/version"

#define CheckUserStatusUrl BaseURL @"login/force_logout"

#define ForcefullyLogoutUserUrl BaseURL @"login/user_loggedout"

#endif /* URLS_h */
