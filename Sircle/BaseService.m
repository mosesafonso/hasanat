//
//  BaseService.m
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@implementation BaseService

- (id) init{
    self = [super init];
    self.serviceDelegate = [[NSURLImplementation alloc] init];
    return self;
}

- (void) setHeaders:(NSString*)value{
    [self.serviceDelegate setCustomHeader:value];
}

@end
