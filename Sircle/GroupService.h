//
//  GroupService.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"
#import "GroupResponse.h"

@interface GroupService : BaseService

+ (instancetype)sharedService;
-(void) fetchGroupsWithCompletion:(void (^)(NSDictionary *, NSError *))callback;


-(void) fetchGroupsListWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
