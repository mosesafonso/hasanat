//
//  EventListDateWise.m
//  Sircle
//
//  Created by Soniya Gadekar on 20/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "EventListDateWise.h"

@implementation EventListDateWise

static EventListDateWise* _eventListDateWise;


+(EventListDateWise*)classMethodeOfEventListDateWise
{
    if (_eventListDateWise==nil)
    {
        _eventListDateWise=[[EventListDateWise alloc]init];
        
    }
    return _eventListDateWise;
    
}

-(void)setEventListDateWise:(NSMutableArray*)dataArray day:(int)day
{
    data = [[NSMutableArray alloc] init];
    
    for (NSDate *date in dataArray) {
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date]; // Get necessary date components
        if ([components day] == day) {
            [data addObject:date];
        }
    }
}


-(NSMutableArray*)returnData;
{
    return data;
}


@end
