//
//  GroupSaveService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"


@interface GroupSaveService : BaseService
+ (instancetype)sharedService;


- (void) saveGroupsWithCompletion:(NSDictionary*)values  completion:(void(^)(NSDictionary *,NSError *error))callback;

-(void) saveGroupsWithIds:(NSString*)groupIds Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
