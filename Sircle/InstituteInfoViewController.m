//
//  InstituteInfoViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 23/04/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "InstituteInfoViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"


@interface InstituteInfoViewController ()

@end

@implementation InstituteInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Institute Info";
    
    [self customSetup];
    
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.emailLabel.attributedText = [[NSAttributedString alloc] initWithString:@"admin@hasanathighschool.com"
                                                             attributes:underlineAttribute];
    
    self.numberLabel.attributedText = [[NSAttributedString alloc] initWithString:@"+912228323238"
                                                                     attributes:underlineAttribute];
    
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(emailLabelTapped:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [_emailLabel addGestureRecognizer:tapGestureRecognizer];
    _emailLabel.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneLabelTapped:)];
    tapGestureRecognizer1.numberOfTapsRequired = 1;
    [_numberLabel addGestureRecognizer:tapGestureRecognizer1];
    _numberLabel.userInteractionEnabled = YES;
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)emailLabelTapped:(UITapGestureRecognizer *)tapGesture {
    [self callMailClient];
}


- (void)phoneLabelTapped:(UITapGestureRecognizer *)tapGesture {
    // [self callMailClient];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+912228323238"]];
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mailComposeController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void)callMailClient
{
    if ([MFMailComposeViewController canSendMail]) {
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"admin@hasanathighschool.com"];
        [mailComposer setToRecipients:toRecipents];
        //  [mailComposer setSubject:@""];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    else
    {
        NSString *recipients = @"mailto:admin@hasanathighschool.com";
        
        //?subject=Feedback";
        
        
        NSString *email = [NSString stringWithFormat:@"%@", recipients];
        
         email = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
      //  email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
