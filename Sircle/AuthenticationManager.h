//
//  AuthenticationManager.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthenticationManager : NSObject

+ (id) sharedLoginManager;
- (void) signInWithUsername:(NSString*) username andPassword:(NSString*) password completion:(void(^)(id result,NSError *error))callback;

@end
