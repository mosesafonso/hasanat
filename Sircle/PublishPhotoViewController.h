//
//  PublishPhotoViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol PublishPhotoViewControllerDelegate <NSObject>

-(void)completionOfUploadImage;

@end

@interface PublishPhotoViewController : UIViewController
@property(nonatomic,weak)id<PublishPhotoViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;
@property(nonatomic,strong) UIImage *sImage;
@property (weak, nonatomic) NSString *albumID;
@property (weak, nonatomic) IBOutlet UITextField *addDescriptionTextFeild;

@end
