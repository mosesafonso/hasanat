//
//  DocumentsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) IBOutlet UITableView *newsLetterTableView;
@property (weak, nonatomic) NSString *urlString;
@end
