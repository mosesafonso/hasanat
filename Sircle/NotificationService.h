//
//  NotificationService.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseService.h"
#import "SettingsResponse.h"

@interface NotificationService : BaseService

+ (instancetype)sharedService;
-(void) fetchNotificationGroupsWithCompletion:(void(^)(SettingsResponse *result,NSError *error))callback;

@end
