//
//  NotifcationData.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "NotifcationData.h"

@implementation NotifcationData
@synthesize totalRecords, page, pageRecords, notifications;

// data -> total_records, page, page_records, notifications

-(NSDictionary*) dictionaryMapping{
    return @{@"total_records": self.totalRecords, @"page": self.page, @"page_records": self.pageRecords, @"notifications" : notifications};
}

@end
