//
//  LoginData.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OAuthData.h"

@interface LoginData : NSObject
//user_id, user_type_id, user_type, username, email, oauth

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *userTypeId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSNumber *email;
@property (nonatomic, strong) OAuthData *oauth;

//- (NSMutableDictionary*) dictionaryMapping;

@end
