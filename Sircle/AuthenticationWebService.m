//
//  AuthenticationWebService.m
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AuthenticationWebService.h"
#import "LoginRequest.h"
#import "AccessToken.h"

#define signInURL @"user_ios/user_get"
@implementation AuthenticationWebService


static AuthenticationWebService *authenticationService = nil;
+ (instancetype)sharedLoginService {
    
    if (nil ==  authenticationService) {
        authenticationService = [AuthenticationWebService new];
    }
    return authenticationService;
}


- (void) signInWithUsername:(NSString*) username andPassword:(NSString*) password completion:(void(^)(LoginResponse *result,NSError *error))callback{
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"UDID:: %@", uniqueIdentifier);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   // [defaults setObject:newdeviceToken forKey:@"DeviceToken"];
    
    LoginRequest *request = [[LoginRequest alloc] init];
    request.username = username;
    request.password = password;
    request.regId = uniqueIdentifier;
    request.notiId = [defaults stringForKey:@"DeviceToken"];
    
    NSDictionary *dict = @{ @"loginId":username, @"pwd" : password, @"regId" : uniqueIdentifier, @"noti_id" : [defaults stringForKey:@"DeviceToken"]};

    
    [self.serviceDelegate executePostWithUrl:signInURL request:dict responseClass:[LoginResponse class] callBack:^(id result, NSError *error) {
        
        if (error == nil) {
            
          //NSString * accessToken = [((LoginResponse*) result).data objectForKey:@"access_token"];
            id data = ((LoginResponse*) result).data;
            id oAuth = [data objectForKey:@"oauth"];//[((LoginData*)data) dictionaryMapping];
            NSString *accessToken = [oAuth objectForKey:@"access_token"];//((LoginResponse*) result).data.oauth.accessToken;
            
            [[AccessToken classMethodeOfAccessToken] setAccessTokenValue:accessToken];
            
          //  NSLog(@"Error %@",;
            if (  [[NSString stringWithFormat:@"%@",((LoginResponse*) result).status] isEqualToString:@"200"]) {
                
                 callback(result, error);
            }
        }
    }];
}


@end
