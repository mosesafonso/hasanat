//
//  LoginResponse.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginData.h"

@interface LoginResponse : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) LoginData *data;
//
//- (NSMutableDictionary*) dictionaryMapping;

@end
