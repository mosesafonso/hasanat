//
//  EventListViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;
@property (weak, nonatomic) NSString *eventID;
@property (strong, nonatomic) IBOutlet UILabel *messageLbl;
@end
