//
//  CalendarViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "CalendarViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "LoginStatus.h"

@interface CalendarViewController ()

@end

@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Calendar";
    
    
   if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
   {
       [self.addEventButton setEnabled:NO];
       [self.addEventButton setTintColor: [UIColor clearColor]];
   }
    
    [self customSetup];
    

}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController isKindOfClass:[RKSwipeBetweenViewControllers class]])
    {
    
    RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
    
    navigationControllerObject.navigationView.hidden=NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addEvents:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *holidayAction  = [UIAlertAction actionWithTitle:@"Holiday" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        [self performSegueWithIdentifier:@"holidayEventIdentifier" sender:self];
        
    }];
    
    
    
    UIAlertAction *eventAction  = [UIAlertAction actionWithTitle:@"Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
        
        [self performSegueWithIdentifier:@"addEventIdentifier" sender:self];
    }];
    
    UIAlertAction *schoolHolidayAction  = [UIAlertAction actionWithTitle:@"School Holiday" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        
         [self performSegueWithIdentifier:@"schoolHolidayEventIdentifier" sender:self];
        
    }];
    
    
    UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"Reset action");
    }];
    
    
    
    [alertController addAction:holidayAction];
    [alertController addAction:eventAction];
    [alertController addAction:schoolHolidayAction];
    [alertController addAction:cancelAction];
    
//    [alertController setModalPresentationStyle:UIModalPresentationPopover];
//    
//    UIPopoverPresentationController *popPresenter = [alertController
//                                                     popoverPresentationController];
//    popPresenter.sourceView = _addEventButton.customView;
//    popPresenter.sourceRect = _addEventButton.customView.bounds;
//    [self presentViewController:alertController animated:YES completion:nil];
    
    UIBarButtonItem *item = self.addEventButton ;
    
    UIView *view = [item valueForKey:@"view"];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];
    popPresenter.sourceView = view;
    popPresenter.sourceRect = view.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
    
  
    
//    UIPopoverPresentationController *popPresenter = [alertController
//                                                     popoverPresentationController];
//    UIButton *button = (UIButton*)sender;
//    
//    popPresenter.sourceView = (UIButton*)sender;
//    popPresenter.sourceRect = button.bounds;
  //  [self presentViewController:alertController animated:YES completion:nil];
    
   
}
@end
