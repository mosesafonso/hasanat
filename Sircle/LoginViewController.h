//
//  ViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 04/09/15.
//  Copyright (c) 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
- (IBAction)submitButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextFeild;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextFeild;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *welcomeTextTopspace;

@end

