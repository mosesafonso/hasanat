//
//  LoginStatus.h
//  STYFI
//
//  Created by MOSES AFONSO on 24/09/15.
//  Copyright © 2015 Stylabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginStatus : NSObject


+(LoginStatus*)classMethodeOfLoginStatus;

-(void)setLoginStatusValue:(BOOL)status;

-(BOOL)getLoginStatus;

-(void)setUserTypeValue:(NSString*)status;

-(NSString*)getUserType;


@end
