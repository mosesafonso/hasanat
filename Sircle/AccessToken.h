//
//  AccessToken.h
//  STYFI
//
//  Created by MOSES AFONSO on 24/09/15.
//  Copyright © 2015 Stylabs. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface AccessToken : NSObject

+(AccessToken*)classMethodeOfAccessToken;

-(void)setAccessTokenValue:(NSString*)accessToken;

-(NSString*)getAccessToken;


-(void)setRefreshTokenValue:(NSString*)accessToken;

-(NSString*)getRefreshToken;

@end
