//
//  PhotosTableViewCell.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "PhotosTableViewCell.h"

@implementation PhotosTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    self.coverImage.layer.borderColor = [UIColor blackColor].CGColor;
    self.coverImage.layer.borderWidth = 1.0f;
    
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
