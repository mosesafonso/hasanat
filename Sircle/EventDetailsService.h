//
//  EventDetailsService.h
//  Sircle
//
//  Created by MOSES AFONSO on 19/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface EventDetailsService : BaseService
+ (instancetype)sharedService;
-(void) fetchEventDetailsWithEventId:(NSString*)Id Completion:(void (^)(NSDictionary *, NSError *))callback;

-(void)fetchEventDetailsWithEventid:(NSString*)eventID Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
