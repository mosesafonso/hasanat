//
//  NotificationManager.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SettingsResponse.h"

@interface NotificationManager : NSObject

+ (id) sharedManager;
-(void) fetchNotificationGroupsWithCompletion:(void(^)(SettingsResponse *result,NSError *error))callback;
@end
