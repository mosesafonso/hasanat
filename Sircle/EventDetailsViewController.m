//
//  EventDetailsViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 19/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "EventDetailsService.h"
#import "RKSwipeBetweenViewControllers.h"
#import "CalendarViewService.h"
#import <EventKit/EventKit.h>
#import "MBProgressHUD.h"
#import "UIColor+HexaDecimalColors.h"
#import "LoginStatus.h"

@interface EventDetailsViewController ()
{
    NSString *startDate,*endDate;
}

@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   // [self initializeUI];
    
    if ([self.navigationController isKindOfClass:[RKSwipeBetweenViewControllers class]]) {
        RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
        
        navigationControllerObject.navigationView.hidden=YES;
    }
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
  
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[EventDetailsService sharedService] fetchEventDetailsWithEventid:self.eventID Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            

            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                   
                    
                                startDate = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_start_date"]];
                    
                    
                                endDate = [NSString stringWithFormat:@"%@", [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_end_date"]];
                    
                                 self.eventDateLabel.text = [NSString stringWithFormat:@"%@ ",startDate];
                    
                      self.eventEndDateLabel.text = [NSString stringWithFormat:@"%@ ",endDate];
                    
                  
                    
                               // self.eventTitleLabel.text = ;
                    
                    self.navigationItem.title = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_title"]];
                    
                    
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"category_name"]]] isEqualToString:@""]) {
                          self.eventCategoryLabel.text = [NSString stringWithFormat:@"None"];
                    }
                    else
                    {
                          self.eventCategoryLabel.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"category_name"]]];
                    }
                    
                    if ([[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_location"]] isEqualToString:@""]) {
                        self.eventLocationLabel.text = [NSString stringWithFormat:@"None"];
                    }
                    else
                    {
                         self.eventLocationLabel.text = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_location"]];
                    }
                    
                    
                    
                                   self.eventInfoLabel.text = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"event_description"]];
                    
//                                   self.eventInfoLabel.text = [NSString stringWithFormat:@"%@", [[[result objectForKey:@"data"] objectForKey:@"event_info"] objectForKey:@"detail"]];
                    
                    NSArray *array = [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"groups"];
                    
                    NSMutableArray *myArray = [[NSMutableArray alloc] init];
                    
                    
                    for (int i =0; i<array.count; i++) {
                        
                        [myArray addObject:array[i][@"group_name"]];
                    }
                    
                                 self.eventGroupsLabel.text = [NSString stringWithFormat:@"%@",[myArray componentsJoinedByString:@","]];
                    
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                   
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }

        
    }];
    
//    [[EventDetailsService sharedService] fetchEventDetailsWithEventId:self.eventID Completion:^(NSDictionary *result, NSError *error) {
//        NSLog(@"Data %@",result);
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            

//            
//             [MBProgressHUD hideHUDForView:self.view animated:YES];
//        
//            
//        });
//        
//    }];
    
    
    if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
        [self.deleteButton setEnabled:NO];
        [self.deleteButton setHidden:YES];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveEvent:(id)sender {
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            return;
        }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = self.navigationItem.title;
        
        
         NSDate *checkDate = [[self dateFormatter] dateFromString:startDate];
        
        NSDate *startDate1;
        NSDate *endDate1;
        
        if (checkDate==nil) {
            
            startDate1  = [[self dateFormatter1] dateFromString:startDate];
            
            
            endDate1 = [[self dateFormatter1] dateFromString:endDate];
            
        }
        
        
        else
        {
            startDate1  = [[self dateFormatter] dateFromString:startDate];
            
            
            endDate1 = [[self dateFormatter] dateFromString:endDate];
        }
        
     
        
        
         event.startDate = startDate1;
        
        
        if ([startDate1 isEqualToDate:endDate1]) {
            event.endDate = [event.startDate dateByAddingTimeInterval:60*60];
        }
        else
        {
             event.endDate = endDate1;
        }
        
       //today
        
       // event.startDate = [[NSDate alloc]init];
        
      //  NSLog(@"start Date %@ %@",[[self dateFormatter] dateFromString:startDate],startDate);
        
       
        //set 1 hour meeting
       //  event.endDate = [event.startDate dateByAddingTimeInterval:60*60];
         event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
      if (err) {
            NSLog(@"Error %@",err.description);
        }
        else
        {

            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //                // code here
                //                 [hud hide:YES afterDelay:3.f];
                //
                //                
                //
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Alert"
                                          message:@"Event Saved"
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Dismiss"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
       
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
                
                 });
            
            
            
        }
       // self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
    }];
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm aa";
    }
    
    return dateFormatter;
}


- (NSDateFormatter *)dateFormatter1
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (IBAction)deleteEvent:(id)sender {
//    [[CalendarViewService sharedService] deleteEventWithID:self.eventID completion:^(AddCallResponse *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            int status = [result.status intValue];
//            NSString *message = result.message;
//            if (status == 200){
//                // success
//                NSLog(@"success delete event %@", message);
//            }
//        }
//    }];
    
    [[CalendarViewService sharedService] deleteEventWithID:self.eventID Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }

        
    }];
    
}

-(void)initializeUI
{
    

    
    self.eventDateLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventDateLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventDateLabel.clipsToBounds = YES;
    
    self.eventEndDateLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventEndDateLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventEndDateLabel.clipsToBounds = YES;
    
    self.eventTitleLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventTitleLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventTitleLabel.clipsToBounds = YES;
    
    self.eventCategoryLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventCategoryLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventCategoryLabel.clipsToBounds = YES;
    
    self.eventLocationLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventLocationLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventLocationLabel.clipsToBounds = YES;
    
    self.eventInfoLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventInfoLabel.layer.borderWidth =1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventInfoLabel.clipsToBounds = YES;
    
    self.eventGroupsLabel.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.eventGroupsLabel.layer.borderWidth = 1.0f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.eventGroupsLabel.clipsToBounds = YES;
    
   
}




@end
