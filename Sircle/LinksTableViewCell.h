//
//  LinksTableViewCell.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KILabel.h"

@interface LinksTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet KILabel *linkLabel;
@property (weak, nonatomic) IBOutlet UIImageView *favicon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *createdOn;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
