//
//  AlbumGridViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AlbumGridViewController.h"
#import "AlbumGridCollectionViewCell.h"
#import "AlbumDetailsService.h"
#import "UIImageView+WebCache.h"
#import "PhotoSingleViewController.h"
#import "SCNavigationController.h"
#import "VPImageCropperViewController.h"
#import "PublishPhotoViewController.h"
#import "GalleryViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+HexaDecimalColors.h"
#import "LoginStatus.h"
#import "MHGalleryItem.h"
#import "CHTCollectionViewWaterfallLayout.h"



@interface AlbumGridViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,SCNavigationControllerDelegate,VPImageCropperDelegate,PublishPhotoViewControllerDelegate,CHTCollectionViewDelegateWaterfallLayout>
{
    NSMutableArray *dataArray;
    UIBarButtonItem *item2;
    NSMutableArray *galleryData;

CHTCollectionViewWaterfallLayout *layout;
}
@end

@implementation AlbumGridViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    galleryData = [[NSMutableArray alloc] init];

    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.hidden=NO;
    self.navigationController.navigationBar.translucent = NO;
    
    [self.cView registerNib:[UINib nibWithNibName:@"AlbumGridCollectionViewCell" bundle:nil]   forCellWithReuseIdentifier: @"AlbumGridCollectionViewCell"];
    
    
    dataArray = [[NSMutableArray alloc] init];
    
    [self setCollectionLayout];
    
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID andPage:@"1" Completion:^(NSDictionary *response, NSError *error) {
//        NSLog(@"response %@",response);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            dataArray = [[response objectForKey:@"data"] objectForKey:@"photos"];
//            [self.cView reloadData];
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//    }];
    
    
    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        dataArray = [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"album_images"];
                        
                        NSLog(@"Data Array %@",dataArray);
                        
                        for (int i=0; i<dataArray.count; i++) {
                            
                            NSString * urlTextEscaped = [[NSString stringWithFormat:@"%@",dataArray[i][@"file_path"]]  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                            
                            
                            NSString * fileName = [NSString stringWithFormat:@"%@",dataArray[i][@"file_name"]];
                            
                            //
                            
                            
                            
                            //  MHGalleryItem *image1 = [MHGalleryItem itemWithURL:urlTextEscaped galleryType:MHGalleryTypeImage];
                            
                            MHGalleryItem *image1 = [MHGalleryItem itemWithURL:urlTextEscaped galleryType:MHGalleryTypeImage imageCaption:fileName];
                            
                            //  MHGalleryItem *youtube = [MHGalleryItem itemWithYoutubeVideoID:@"myYoutubeID"];
                            
                            [galleryData addObject:image1];
                        }
                        
                        
                        [self.cView reloadData];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [self.cView reloadData];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
    }];
    
    
    
    if([[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
         item2= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"addImage"] style:UIBarButtonItemStylePlain target:self action:@selector(takePhoto:)];
        item2.tintColor = [UIColor whiteColor];
        NSArray * buttonArray =[NSArray arrayWithObjects:item2 ,nil];
        self.navigationItem.rightBarButtonItems =buttonArray;
    }
    
    
    
    
    
  
    //   self.navigationItem.leftBarButtonItem = rightButton;
    
//    UIBarButtonItem *barBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(pop)];
//    //self.navigationItem.backBarButtonItem = barBtnItem;
//    barBtnItem.tintColor=[UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = barBtnItem;
//    
//    self.navigationItem.hidesBackButton = YES;

  //  [self callAfterSixtySecond];

}


-(void)setCollectionLayout
{
    //layout set of collection view cell
    layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirectionLeftToRight;
    layout.minimumColumnSpacing = 10.0;
    if ([self isIpad]) {
         layout.columnCount = 3;
    }
    else
    {
       layout.columnCount = 2;
    }
   
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 0, 10);
    [self.cView setCollectionViewLayout:layout];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"AlbumGridCollectionViewCell";
    
    AlbumGridCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSString * urlTextEscaped = [[NSString stringWithFormat:@"%@",dataArray[indexPath.row][@"file_path"]]  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    
    [cell.thumbImage sd_setImageWithURL:[NSURL URLWithString:urlTextEscaped] placeholderImage:[UIImage imageNamed:@"imagePlaceholder"]];
    
    
   cell.caption.text = [NSString stringWithFormat:@"%@",dataArray[indexPath.row][@"file_name"]];
    
    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    AlbumGridCollectionViewCell * cell = (AlbumGridCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
//    
//    if (cell == nil) {
//        
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AlbumGridCollectionViewCell" owner:self options:nil];
//        cell = [topLevelObjects objectAtIndex:0];
//        
//        [cell layoutIfNeeded];
//        
//    }
//    
//    CGSize CellSize = [cell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    
//    
//    return CellSize;
//    
//    
//}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize result;
    
    result=CGSizeMake((([UIScreen mainScreen].bounds.size.width)/2),(([UIScreen mainScreen].bounds.size.width)/2));
    return result;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//     [self performSegueWithIdentifier:@"PhotoSingleViewIdentifier" sender:self];
    //  [self performSegueWithIdentifier:@"PhotoSingleViewIdentifier" sender:self];
    AlbumGridCollectionViewCell * cell = (AlbumGridCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    
    MHGalleryController *gallery = [MHGalleryController galleryWithPresentationStyle:MHGalleryViewModeImageViewerNavigationBarHidden];
    // gallery.UICustomization.hideShare = YES;
    
    gallery.UICustomization.barTintColor = [UIColor colorWithHexString:@"339966"];
    gallery.UICustomization.barStyle = UIBarStyleBlackOpaque;
    
    // gallery.UICustomization.showOverView = NO;
    gallery.UICustomization.barButtonsTintColor = [UIColor whiteColor];
    gallery.galleryItems = galleryData;
    gallery.presentingFromImageView = cell.thumbImage;
    gallery.presentationIndex = indexPath.row;
    
    
    __weak MHGalleryController *blockGallery = gallery;
    
    
    
    
    gallery.finishedCallback = ^(NSInteger currentIndex,UIImage *image,MHTransitionDismissMHGallery *interactiveTransition,MHGalleryViewMode viewMode)
    {
        
        NSIndexPath *newIndex = [NSIndexPath indexPathForRow:currentIndex inSection:0];
        
        
        [self.cView scrollToItemAtIndexPath:newIndex atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:NO];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            AlbumGridCollectionViewCell * cell = (AlbumGridCollectionViewCell *) [collectionView cellForItemAtIndexPath:newIndex];
            
            [blockGallery dismissViewControllerAnimated:YES dismissImageView:cell.thumbImage completion:nil];
        });
        
    };
    [self presentMHGalleryController:gallery animated:YES completion:nil];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"PhotoSingleViewIdentifier"]) {
        
        // Get destination view
       GalleryViewController  *vc = [segue destinationViewController];
        vc.dataArray = dataArray;
    }
    else if ([segue.identifier isEqualToString:@"publishPhotoIdentifier"])
    {
        PublishPhotoViewController *controller = ([segue.destinationViewController isKindOfClass:[PublishPhotoViewController class]]) ? segue.destinationViewController : nil;
        controller.sImage=self.sImage;
        controller.albumID =self.albumID;
        controller.delegate = self;
        
    }
}

-(void)completionOfUploadImage
{
    
//    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID andPage:@"1" Completion:^(NSDictionary *response, NSError *error) {
//        NSLog(@"response %@",response);
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            dataArray = [[response objectForKey:@"data"] objectForKey:@"photos"];
//            [self.cView reloadData];
//        });
//    }];
    
   
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Success"
                                  message:@"Photo Saved Successfully"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                             
                         }];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    [dataArray removeAllObjects];
    [self.cView reloadData];
    
    
    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        dataArray = [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"album_images"];
                        [self.cView reloadData];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [self.cView reloadData];
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
    }];

    
}

-(IBAction)takePhoto:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoAction  = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSLog(@"Reset action");
        SCNavigationController *nav = [[SCNavigationController alloc] init];
        nav.scNaigationDelegate = self;
        [nav showCameraWithParentController:self];
        
    }];
    
    
    
    UIAlertAction *choosePhotoAction  = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //  NSLog(@"Reset action");
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        // imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        NSLog(@"Reset action");
    }];
    
    
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:choosePhotoAction];
    [alertController addAction:cancelAction];
    
    UIBarButtonItem *item = item2 ;
    
    UIView *view = [item valueForKey:@"view"];
    
    UIPopoverPresentationController *popPresenter = [alertController
                                                     popoverPresentationController];
    popPresenter.sourceView = view;
    popPresenter.sourceRect = view.bounds;
    
    
//    UIPopoverPresentationController *popPresenter = [alertController
//                                                     popoverPresentationController];
//    UIButton *button = (UIButton*)sender;
//    
//    popPresenter.sourceView = (UIButton*)sender;
//    popPresenter.sourceRect = button.bounds;
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        //    portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
//        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
//        imgCropperVC.delegate = self;
//        [self presentViewController:imgCropperVC animated:YES completion:^{
//            // TO DO
//        }];
        
        self.sImage = portraitImg;
        
        [self performSegueWithIdentifier:@"publishPhotoIdentifier" sender:self];
        
    }];
    
    
}

#pragma mark - SCNavigationController delegate
- (void)didTakePicture:(SCNavigationController *)navigationController image:(UIImage *)image {
    // PostViewController *con = [[PostViewController alloc] init];
    // con.postImage = image;
    // [navigationController pushViewController:con animated:YES];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    CapturedImageViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"capturedImageIdentifier"];
//    vc.postImage = image;
//    [navigationController pushViewController:vc animated:YES];
    self.sImage = image;
   // self.albumID = self.albumID;
    [self performSegueWithIdentifier:@"publishPhotoIdentifier" sender:self];
    
}


#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    // self.myImage.image = editedImage;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
        
        self.sImage = editedImage;
        
          [self performSegueWithIdentifier:@"publishPhotoIdentifier" sender:self];
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)isIpad
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return YES; /* Device is iPad */
    }
    
    return NO;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
