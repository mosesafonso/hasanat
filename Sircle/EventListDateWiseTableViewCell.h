//
//  EventListDateWiseTableViewCell.h
//  Sircle
//
//  Created by MOSES AFONSO on 19/04/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListDateWiseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *startDate;

@property (weak, nonatomic) IBOutlet UIImageView *eventIcon;

@property (weak, nonatomic) IBOutlet UILabel *endTime;

@end
