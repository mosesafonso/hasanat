//
//  EventListDateWiseViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 20/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "EventListDateWiseViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "EventListTableViewCell.h"
#import "EventListDateWiseTableViewCell.h"
#import "EventListMonthWise.h"
#import "EventDetailsViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "MBProgressHUD.h"
#import "CalendarViewService.h"

@interface EventListDateWiseViewController ()
{
    NSMutableArray *data;
   // UILabel *messageLbl;
}
@end

@implementation EventListDateWiseViewController
@synthesize messageLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    messageLbl = [[UILabel alloc]init];
    
    RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
    
    navigationControllerObject.navigationView.hidden=YES;
    
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([EventListDateWiseTableViewCell class])
                                    bundle:nil];
    [self.eventsTableView registerNib:cellNib
               forCellReuseIdentifier:@"EventListDateWiseTableViewCell"];
    self.eventsTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.eventsTableView.estimatedRowHeight = 94;
    
    self.eventsTableView.tableFooterView = [UIView new];
    
    data = [[NSMutableArray alloc] init];
    
//    NSArray *array = [[EventListMonthWise classMethodeOfEventListMonthWise] returnData];
//    // [self.eventsTableView reloadData];
//    
//    NSCalendar* calendar = [NSCalendar currentCalendar];
//    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self.selectedDate];
//    
//    for (int i = 0;i< [[EventListMonthWise classMethodeOfEventListMonthWise] returnData].count;i++) {
//        // start_date
//        // Get necessary date components
//        if ([components day] == [self getDayFromDate:array[i][@"event_start_date"]]) {
//            [data addObject:array[i]];
//        }
//    }
//    
//    NSLog(@"Data %@",data);
//    
//    [self.eventsTableView reloadData];
    
    
    
    [self callWebserviceWithDate:[[self dateFormatter] stringFromDate:self.selectedDate]];
    
    
  //  event 1 = icon_id = 1 Arts
    
  //  event 2 = icon_id = 2 Sports
    
  //  event 3 = icon_id = 3 Excursion
    
  //  event 4 = icon_id = 4 Academics
    
  //  event 5 = icon_id = 5 Performance
    
  //  event 5 = icon_id = 6 Other
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
   
}


-(int)getDayFromDate:(NSString*)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate: [[self dateFormatter] dateFromString:date]];
    
    return (int)[components day];
   
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
//        dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm aa";
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (data.count == 0) {
        //create a lable size to fit the Table View
       messageLbl.frame =CGRectMake(0, 0,self.eventsTableView.bounds.size.width,self.eventsTableView.bounds.size.height);
        //set the message
        messageLbl.text = @"No Events Today";
        //center the text
        messageLbl.textAlignment = NSTextAlignmentCenter;
        //auto size the text
        [messageLbl sizeToFit];
        
        //set back to label view
        self.eventsTableView.backgroundView = messageLbl;
        //no separator
        self.eventsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
       // return 0;
    }
    else
    {
        self.eventsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        messageLbl.text=@"";
    }
    
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventListDateWiseTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"EventListDateWiseTableViewCell"];
    
    cell.title.text = data[indexPath.row][@"event_title"];
   // cell.startTime.text = data[indexPath.row][@"event_start_date"];
    // cell.endTime.text = data[indexPath.row][@"event_end_date"];
    
    NSString * str = data[indexPath.row][@"event_start_date"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
    cell.startDate.text = arr[0];
    
    
    cell.startTime.text = arr[1];
    
    str = data[indexPath.row][@"event_end_date"];
    arr = [str componentsSeparatedByString:@" "];
    
    cell.endTime.text = arr[1];
    
    if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"event_type"] ] isEqualToString:@"N"])
    {
        cell.startTime.hidden = NO;
        cell.endTime.hidden = NO;
        
    }
    
    if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Arts"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"arts"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Sports"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"sports"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"3"]) {
        //[cell.eventIcon setImage:[UIImage imageNamed:@"arts"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Academics"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"academics"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Performance"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"performance"]];
    }
    else  if ([[NSString stringWithFormat:@"%@",data[indexPath.row][@"category_name"]] isEqualToString:@"Others"]) {
        [cell.eventIcon setImage:[UIImage imageNamed:@"others"]];
    }
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        cell.title.font = [UIFont fontWithName:@"Roboto-Light" size:25];
        
    }
    else
    {
        cell.title.font = [UIFont fontWithName:@"Roboto-Light" size:20];
        
    }

    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    self.eventID = data[indexPath.row][@"event_id"];
    [self performSegueWithIdentifier:@"eventDetailsDateWiseIdentifier" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"eventDetailsDateWiseIdentifier"]) {
        
        // Get destination view
        EventDetailsViewController *vc = [segue destinationViewController];
        vc.eventID = self.eventID;
    }
    
}


-(void)callWebserviceWithDate:(NSString*)mySelectedDate
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    

    [[CalendarViewService sharedService] fetchEventDatesForDate:mySelectedDate Page:@"1" Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
              
                
                data = [jsonLoginDictionary objectForKey:@"data"];
                
               
             
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    
                    
                    [self.eventsTableView reloadData];
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                    // [self.navigationController popViewControllerAnimated:YES];
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }
        
        
        
    }];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
