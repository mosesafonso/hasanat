//
//  EventDetailsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 19/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *eventDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventEndDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventGroupsLabel;
- (IBAction)saveEvent:(id)sender;
- (IBAction)deleteEvent:(id)sender;

@property (weak, nonatomic) NSString *eventID;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
