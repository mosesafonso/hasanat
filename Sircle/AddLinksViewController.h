//
//  AddLinksViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 15/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddLinksViewControllerDelegate <NSObject>

-(void)completionOfAddLinks;

@end



@interface AddLinksViewController : UIViewController
@property(nonatomic,weak)id<AddLinksViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *linkTitleTextFeild;
@property (weak, nonatomic) IBOutlet UITextField *linkUrlTextFeild;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@end
