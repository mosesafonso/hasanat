//
//  PublishPhotoViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "PublishPhotoViewController.h"
#import "PhotosListService.h"
#import "MBProgressHUD.h"

@interface PublishPhotoViewController ()<UITextFieldDelegate>

@end

@implementation PublishPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.selectedImage.image = [self compressImage:self.sImage withScaleFactor:350];//self.sImage;
    
    
}

- (UIImage *)compressImage:(UIImage *)image withScaleFactor:(NSInteger)scaleFactor{
    
    // Check if either the width or the height is greater than
    // Image_Upload_Scalling_Factor else return original image
    // Check which value is greater width or height
    // Scale the greater value to 1500
    // Get the new image maintaining the aspect ratio
    
    CGSize newSize;
    
    if (image.size.width > scaleFactor || image.size.height > scaleFactor) {
        if (image.size.width > image.size.height) {
            newSize.width = scaleFactor;
            newSize.height =
            (scaleFactor * image.size.height) /
            image.size.width;
        } else {
            newSize.height = scaleFactor;
            newSize.width = (scaleFactor * image.size.width) /
            image.size.height;
        }
        
        UIGraphicsBeginImageContext(newSize);
        [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return newImage;
    } else {
        return image;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addPhotoButton:(id)sender {
    
//    
//    {
//        code = 404;
//        data =     {
//            "method_type" = POST;
//        };
//        error =     {
//            "caption_name" = "Caption Name must be between 3 and 64 characters!";
//        };
//        message = "Invalid Request!";
//    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData *imageData = UIImageJPEGRepresentation(self.selectedImage.image,0.3);
    
   NSLog(@"File size is : %.2f MB",(float)imageData.length/1024.0f/1024.0f);
    
    
    [[PhotosListService sharedService] addPhotoWithCaption:self.addDescriptionTextFeild.text AlbumId:self.albumID File:imageData Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [MBProgressHUD hideHUDForView:self.view animated:YES];
                    //[self.navigationController popViewControllerAnimated:YES];
                    //[self.delegate completionOfAddalbum];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.delegate completionOfUploadImage];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    
                


                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Alert"
                                                  message:[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"error"]]
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Do some thing here
                                             
                                             
                                         }];
                    [alert addAction:ok];
                    
                    
                    
                });
                
            }
            
            
        }

        
    }];
    
//   /
    
//     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    [[PhotosListService sharedService] addPhotoWithDesc:self.addDescriptionTextFeild.text albumId:self.albumID andData:imageData Completion:^(UploadAlbumResponse *response, NSError *error) {
//        if (error){
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
//                
//                //  [self.navigationController popViewControllerAnimated:YES];
//                // [self.delegate completionOfUploadImage];
//                
//            });
//            
//        }else{
//           // image_name , thumbnailUrl, url
//            if ([response.code integerValue] == 200){
//                NSLog(@"success %@",response.message);
//                
//               // if ([response.message isEqualToString:@"Image added successfully!"]) {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        
//                        
//                          [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        [self.navigationController popViewControllerAnimated:YES];
//                        [self.delegate completionOfUploadImage];
//                        
//                    });
//               }
//                else
//                {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        
//                          [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        
//                      //  [self.navigationController popViewControllerAnimated:YES];
//                       // [self.delegate completionOfUploadImage];
//                        
//                    });
//                }
//                
//                //Image added successfully!
//                
//                // use this data to display the photo in grid
////                NSString *image_name = [response.data objectForKey:@"image_name"];
////                NSString *thumbnailURL = [response.data objectForKey:@"thumbnailUrl"];
////                NSString *url = [response.data objectForKey:@"url"];
//
//        
//        }
//    }];
    
    
    
    
}


#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.addDescriptionTextFeild resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
//    if([string isEqualToString:@"\n"]) {
//        [textField resignFirstResponder];
//        return NO;
//    }
//    
//    if([string length] == 0)
//    {
//        if([textField.text length] != 0)
//        {
//            return YES;
//        }
//    }
//
    
    
    if([[textField text] length] > 55)
                {
                    return NO;
                }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
//    if (textView == self.notificationTitleTextFeild) {
//        if([[textView text] length] > 139)
//        {
//            return NO;
//        }
//    }
//    else
//    {
//        if([[textView text] length] > 254)
//        {
//            return NO;
//        }
//    }
//    
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
