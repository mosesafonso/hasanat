//
//  LoginRequest.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface LoginRequest : BaseModel

//regId, loginId, pwd

@property (nonatomic, strong) NSString *regId;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *notiId;

- (NSDictionary*) dictionaryMapping;

@end
