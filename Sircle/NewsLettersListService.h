//
//  NewsLettersListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface NewsLettersListService : BaseService
+ (instancetype)sharedService;
-(void) fetchNewsLettersListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;

-(void) fetchNewsLettersListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
