//
//  GroupService.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "GroupService.h"
#import <UIKit/UIKit.h>
#import "AccessToken.h"
#import "URLS.h"

#define groupURL @"login/setting"


@implementation GroupService

static GroupService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [GroupService new];
    }
    return groupService;
}

-(void) fetchGroupsWithCompletion:(void (^)(NSDictionary *response, NSError *error))callback{
    
  //  NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
  //  NSDictionary *dict = @{@"regId" : uniqueIdentifier};
    [self.serviceDelegate executeGetWithUrl:groupURL queryParams:NULL requestHeaders:nil responseClass:[GroupResponse class] callBack:^(NSDictionary *json, NSError *error) {
        if (error) {
            
        }else{
            callback(json, nil);
        }
    }];
}

-(void) fetchGroupsListWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:GroupsListURL];
    
   
    
    NSString* urlTextEscaped =  [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    [request setHTTPMethod:@"GET"];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
    
}


@end
