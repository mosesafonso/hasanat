//
//  ViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 04/09/15.
//  Copyright (c) 2015 MOSES AFONSO. All rights reserved.
//

#import "LoginViewController.h"
#import "UIColor+HexaDecimalColors.h"
//#import "AuthenticationManager.h"
//#import "LoginResponse.h"
#import "LoginWebservice.h"
#import "AccessToken.h"
#import "MBProgressHUD.h"
#import "LoginStatus.h"
#import "TSMessage.h"
#import "Reachability.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = YES;
    self.emailIdTextFeild.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.emailIdTextFeild.layer.borderWidth = 0.5f;
    //self.emailIdTextFeild.layer.cornerRadius = 4.0f;
    self.emailIdTextFeild.clipsToBounds = YES;
    
    self.passwordTextFeild.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.passwordTextFeild.layer.borderWidth = 0.5f;
    //self.passwordTextFeild.layer.cornerRadius = 4.0f;
    self.passwordTextFeild.clipsToBounds = YES;
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 19, 20)];
    _emailIdTextFeild.leftView = paddingView;
    _emailIdTextFeild.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 19, 20)];
    _passwordTextFeild.leftView = paddingView1;
    _passwordTextFeild.leftViewMode = UITextFieldViewModeAlways;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        self.welcomeTextTopspace.constant  = 5;
    }
    
//    self.submitButton.layer.cornerRadius = 24.0f;
//    self.submitButton.clipsToBounds = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
     [self.navigationController.navigationBar setHidden:YES];
}


- (IBAction)submitButtonClicked:(id)sender {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (_emailIdTextFeild.text.length == 0 || _passwordTextFeild.text.length == 0)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert" message:@"Email/UserName And Password Should Not Be Blank " preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Dismiss"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert" message:@"Please check your internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Dismiss"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
    }
    else
    {
        NSLog(@"There IS internet connection");
        
    // call server to log in
        
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
//    [[AuthenticationManager sharedLoginManager] signInWithUsername:self.emailIdTextFeild.text andPassword:self.passwordTextFeild.text completion:^(LoginResponse *result, NSError *error) {
//        if (error) {
//            
//
//            
//        }else{
//            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                 [MBProgressHUD hideHUDForView:self.view animated:YES];
//                [self performSegueWithIdentifier:@"settingSegueIdentifier" sender:self];
//
//            });
//            
//            
//        }
//    }];
        
        [[LoginWebservice sharedService] loginWithUserName:self.emailIdTextFeild.text  Password:self.passwordTextFeild.text DeviceToken:[defaults stringForKey:@"DeviceToken"] DeviceType:@"ios" Completion:^(NSData *data, NSURLResponse *response, NSError *error) {

//        [[LoginWebservice sharedService] loginWithUserName:self.emailIdTextFeild.text  Password:self.passwordTextFeild.text DeviceToken:@"600b0b350c36a20e01e14d86bb0bf77686a0768e164ed2611555b0841b71dafa" DeviceType:@"ios" Completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        


                if (error) {
                
                
                
                        }else{
                            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
                
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // code here
        
                                
                                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                    options:NSJSONReadingMutableContainers
                                                                                                      error:nil];
                                NSLog(@"Data %@",jsonLoginDictionary);
                                
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                
                               
                                
    if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                                    
                                    
          [[AccessToken classMethodeOfAccessToken] setAccessTokenValue:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"auth_token"]];
          [[LoginStatus classMethodeOfLoginStatus] setLoginStatusValue:YES];
        
    
        
          [[LoginStatus classMethodeOfLoginStatus] setUserTypeValue:[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"customer_type"]]];
        
        if ([[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"customer_type"]] isEqualToString:@"admin"]) {
            [self performSegueWithIdentifier:@"homeSegueIdentifier" sender:self];
        }
        else
        {
          [self performSegueWithIdentifier:@"settingSegueIdentifier" sender:self];
        }
            
           
        
                                }
                                else
                                {
                                    
                                    NSLog(@"Error");
                                    
                                    [self.emailIdTextFeild resignFirstResponder];
                                    [self.passwordTextFeild resignFirstResponder];
                                    
                                    [TSMessage showNotificationWithTitle:@""
                                                                subtitle:@"Sign in failed! Wrong password or username."
                                                                    type:TSMessageNotificationTypeError];
                                    
                                }
                                
      
      
                
                            });
                            
                            
                        }
            
            
        }];
        
        
    }
    
}

#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailIdTextFeild resignFirstResponder];
    [self.passwordTextFeild resignFirstResponder];
    
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField:textField up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
  
        NSInteger nextTag = textField.tag + 1;
        // Try to find next responder
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (nextResponder) {
            // Found next responder, so set it.
            [nextResponder becomeFirstResponder];
        } else {
            // Not found, so remove keyboard.
            [textField resignFirstResponder];
        }
        return NO; // We do not want UITextField to insert line-breaks.

//    [textField resignFirstResponder];
  //  return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    return YES;
    
    
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


@end
