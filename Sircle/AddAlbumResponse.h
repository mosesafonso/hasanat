//
//  AddAlbumResponse.h
//  Sircle
//
//  Created by Soniya Gadekar on 16/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddAlbumResponse : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSDictionary *data;

- (NSDictionary*) dictionaryMapping;

@end
