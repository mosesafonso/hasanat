//
//  LinksListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"
#import "AddCallResponse.h"

@interface LinksListService : BaseService
+ (instancetype)sharedService;
-(void) fetchLinksListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;
- (void) addLink:(NSString*)title link:(NSString*)link Completion:(void (^)(AddCallResponse *, NSError *))callback;

-(void) fetchLinksListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) addLinkWithTitle:(NSString*)title Link:(NSString*)link Groups:(NSString*)groups Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
