//
//  NotificationsViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "NotificationsViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "NotificationsTableViewCell.h"
#import "NotificationListService.h"
#import "AddNotificationsViewController.h"
#import "MBProgressHUD.h"
#import "LoginStatus.h"

@interface NotificationsViewController ()<AddNotificationsViewControllerDelegate>
{
    NSMutableArray *data;
    NSInteger pageCount;
    UIActivityIndicatorView *spinnerFooter;
    UIRefreshControl *refreshControl;
}
@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Messages";
    
    if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
        [self.addMessages setEnabled:NO];
        [self.addMessages setTintColor: [UIColor clearColor]];
    }
    
    [self customSetup];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([NotificationsTableViewCell class])
                                    bundle:nil];
    [self.notificationsTableView registerNib:cellNib
                   forCellReuseIdentifier:@"NotificationsTableViewCell"];
    self.notificationsTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.notificationsTableView.estimatedRowHeight = 72;
    
    
    pageCount = 0;
    
    self.notificationsTableView.tableFooterView = [UIView new];
    
    
    
    data = [[NSMutableArray alloc] init];
    
    [self callWebservice];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startRefresh:)forControlEvents:UIControlEventValueChanged];
    
    [self.notificationsTableView addSubview:refreshControl];
}


-(IBAction)startRefresh:(id)sender
{
    // [refreshControl endRefreshing];
    
    pageCount = 0;
    [self callWebservice];
}

-(void)callWebservice
{
    pageCount = pageCount +1;
    
    if (pageCount==1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
//    [[NotificationListService sharedService] fetchNotificationListWithPage:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSDictionary *response, NSError *error) {
//        
//        NSLog(@"Response %@",response);
//        
//        
//        
//        [data addObjectsFromArray:[[response objectForKey:@"data"] objectForKey:@"notifications"]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [spinnerFooter removeFromSuperview];
//            [self.notificationsTableView reloadData];
//             [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//        
//        
//    }];
    
    [[NotificationListService sharedService] fetchNotificationsListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    if (refreshControl.isRefreshing) {
                        [data removeAllObjects];
                        [self.notificationsTableView reloadData];
                        [refreshControl endRefreshing];
                    }
                    
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"notifications"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        
                        [spinnerFooter removeFromSuperview];
                        [self.notificationsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.notificationsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
        
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationsTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    
    cell.subject.text = data[indexPath.row][@"notification_title"];
    
    cell.message.text = data[indexPath.row][@"notification_message"];

    
    NSString * str = data[indexPath.row][@"notification_time"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
    cell.dateLabel.text = arr[0];

    
    cell.timeLabel.text = arr[1];

    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        cell.subject.font = [UIFont fontWithName:@"Roboto-Light" size:25];
    }
    else
    {
        cell.subject.font = [UIFont fontWithName:@"Roboto-Light" size:20];
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 0.0) {
        // [spinnerFooter startAnimating];
        spinnerFooter = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinnerFooter startAnimating];
        spinnerFooter.frame = CGRectMake(0, 0,self.notificationsTableView.frame.size.width, 44);
        self.notificationsTableView.tableFooterView = spinnerFooter;
        [self callWebservice];
        
        //[self methodThatAddsDataAndReloadsTableView];
    }
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addNotificationsCalled:(id)sender {
    
    [self performSegueWithIdentifier:@"addNotificationsIdentifier" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addNotificationsIdentifier"]) {
        
        // Get destination view
        AddNotificationsViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        
        
    }
    
}

-(void)completionOfAddNotifications
{
    
    [data removeAllObjects];
    [self.notificationsTableView reloadData];
    
    
    pageCount = 1;
    
//    [[NotificationListService sharedService] fetchNotificationListWithPage:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSDictionary *response, NSError *error) {
//        
//        NSLog(@"Response %@",response);
//        
//        [data removeAllObjects];
//        
//        [data addObjectsFromArray:[[response objectForKey:@"data"] objectForKey:@"notifications"]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [spinnerFooter removeFromSuperview];
//            [self.notificationsTableView reloadData];
//        });
//        
//        
//    }];
    
    [[NotificationListService sharedService] fetchNotificationsListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"notifications"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.notificationsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.notificationsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
        
    }];


}

@end
