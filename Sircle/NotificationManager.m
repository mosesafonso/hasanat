//
//  NotificationManager.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "NotificationManager.h"
#import "NotificationService.h"

@implementation NotificationManager

static NotificationManager *notificationManager = nil;

+ (instancetype)sharedManager {
    
    if (nil ==  notificationManager) {
        notificationManager = [NotificationManager new];
    }
    return notificationManager;
}

-(void) fetchNotificationGroupsWithCompletion:(void (^)(SettingsResponse *, NSError *))callback{
    [[NotificationService sharedService] fetchNotificationGroupsWithCompletion:^(SettingsResponse *result, NSError *error) {
        if (error) {
            callback (nil, error);
        }else{
            callback(result, nil);
        }
    }];
}

@end
