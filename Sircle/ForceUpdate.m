//
//  ForceUpdate.m
//  Sircle
//
//  Created by MOSES AFONSO on 24/08/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "ForceUpdate.h"
#import "AccessToken.h"
#import "URLS.h"

@implementation ForceUpdate

static ForceUpdate *forceUpdate = nil;

+ (instancetype)sharedService
{
    if (nil ==  forceUpdate) {
        forceUpdate = [ForceUpdate new];
    }
    return forceUpdate;
}

-(void) getAppVersionWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))callback
{
    

    
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:ForceUpdateUrl];
    
    NSString* urlTextEscaped =  [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
   [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    [request setHTTPMethod:@"GET"];
  
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:callback] resume];
    
    
}

@end
