//
//  CheckUserStatus.m
//  Sircle
//
//  Created by MOSES AFONSO on 26/07/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "CheckUserStatus.h"
#import "AccessToken.h"
#import "URLS.h"

@implementation CheckUserStatus

static CheckUserStatus *loginWebservice = nil;
+ (instancetype)sharedService
{
    if (nil ==  loginWebservice) {
        loginWebservice = [CheckUserStatus new];
    }
    return loginWebservice;
}


-(void) checkUSerStatusWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))CallBack;
{
    
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:CheckUserStatusUrl];
    
    
    NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    //    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    //NSString* userUpdate=[NSString stringWithFormat:@"event_id=%@",eventID];
 //   NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
   // NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    //NSString* jsonString=nil;
    
  //  if (! jsonData)
    //{
        
    //}else
   // {
       // jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
     //   NSLog(@"URL jsonString::%@",jsonString);
   // }
   // [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
   // [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:CallBack] resume];

    
}
@end
