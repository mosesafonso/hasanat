//
//  MenuViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 10/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MenuViewController : UIViewController<MFMailComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    MFMailComposeViewController *mailComposer;
}
- (IBAction)openMailClient:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableViewShowMenu;
- (IBAction)settingsButtonClicked:(id)sender;


@end
