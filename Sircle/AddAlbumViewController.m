//
//  AddAlbumViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 16/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddAlbumViewController.h"
#import "SettingsHeaderTableViewCell.h"
#import "GroupManager.h"
#import "Group.h"
#import "GroupSaveService.h"
#import "PhotosListService.h"
#import "AddAlbumResponse.h"
#import "SCNavigationController.h"
#import "VPImageCropperViewController.h"
#import "MBProgressHUD.h"
#import "GroupService.h"
#import "PublishPhotoViewController.h"


@interface AddAlbumViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,SCNavigationControllerDelegate,VPImageCropperDelegate,UITextFieldDelegate,PublishPhotoViewControllerDelegate>
@property (nonatomic, strong) NSMutableArray *groupNames;
@property (nonatomic,strong) NSMutableArray *activeGroupIds;
@end

@implementation AddAlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([SettingsHeaderTableViewCell class])
                                    bundle:nil];
    [self.settingsTableView registerNib:cellNib
                 forCellReuseIdentifier:@"SettingsHeaderTableViewCell"];
    self.settingsTableView.rowHeight = 60;
    
    self.settingsTableView.estimatedRowHeight = 60;
    
    self.groupNames = [[NSMutableArray alloc] init];
    self.activeGroupIds = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    
    self.settingsTableView.tableFooterView = [UIView new];
    
    self.settingsTableView.layer.borderWidth = 1.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 19, 20)];
    _addNameTExtFeild.leftView = paddingView;
    _addNameTExtFeild.leftViewMode = UITextFieldViewModeAlways;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[GroupManager sharedManager] fetchGroupsWithCompletion:^(NSDictionary *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            // id obj = result.data;
//            //            for (Group* group in obj) {
//            //                [self.groupNames addObject:group.groupName];
//            //            }
//            
//            NSLog(@"Data %@",[result objectForKey:@"data"]);
//            
//            [self.groupNames addObjectsFromArray:[result objectForKey:@"data"]];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                [self.settingsTableView reloadData];
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
//            });
//            
//        }
//    }];
    
    [[GroupService sharedService] fetchGroupsListWithCompletion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    [self.groupNames addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"groups"]];
                    
                    for (int i =0; i<self.groupNames.count; i++) {
                        if ([[NSString stringWithFormat:@"%@",self.groupNames[i][@"select"]] isEqualToString:@"1"]) {
                            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //   [spinnerFooter removeFromSuperview];
                        //   [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //  [spinnerFooter removeFromSuperview];
                        //  [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
    }];

    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsHeaderTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    cell.groupNameLabel.text = self.groupNames[indexPath.row][@"name"];
    
    
    
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]]) {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    else
    {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    
    [cell.radioButton setTag:indexPath.row];
    [cell.radioButton addTarget:self action:@selector(checkOrUncheckGroup:) forControlEvents: UIControlEventTouchUpInside];
    
    return cell;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    
    headerCell.groupNameLabel.text = @"ALL";
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    else
    {
        
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    
    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
    
    // [headerCell setTag:section];
    return headerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(IBAction)selectAllGroups:(id)sender
{
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [self.activeGroupIds removeAllObjects];
        for (int i=0; i<self.groupNames.count; i++) {
            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
            [self.settingsTableView reloadData];
        }
    }
    else
    {
        [self.activeGroupIds removeAllObjects];
        [self.settingsTableView reloadData];
        
    }
}





-(IBAction)checkOrUncheckGroup:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    SettingsHeaderTableViewCell *cell = (SettingsHeaderTableViewCell*)[self.settingsTableView cellForRowAtIndexPath:indexPath];
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]])
    {
        [self.activeGroupIds removeObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
        
    }
    else
    {
        [self.activeGroupIds addObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    // 1. Dequeue the custom header cell
//    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
//    }
//    else
//    {
//
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
//    }
//
//    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
//
//    // [headerCell setTag:section];
//    return headerCell;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 60;
//}


//-(IBAction)selectAllGroups:(id)sender
//{
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [self.activeGroupIds removeAllObjects];
//        for (int i=0; i<self.groupNames.count; i++) {
//            [self.activeGroupIds addObject:self.groupNames[i][@"group_id"]];
//            [self.settingsTableView reloadData];
//        }
//    }
//    else
//    {
//        [self.activeGroupIds removeAllObjects];
//        [self.settingsTableView reloadData];
//
//    }
//}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)submitButtonClicked:(id)sender {
    
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[PhotosListService sharedService] addAlbum: Completion:^(AddAlbumResponse *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            NSNumber *status = result.status;//[result valueForKey:@"status"];
//          //  NSString *message = result.message;
//            if ([status integerValue] == 200) {
//                // use this album id for add photo api - for which photo is to be added
//              //  NSNumber *albumId = [result.data objectForKey:@"alb_id"];
//               // [[NSUserDefaults standardUserDefaults] setValue:albumId forKey:@"alb_id"];
//                //[self takePhoto];
//                //[self performSegueWithIdentifier:@"publishPhotoIdentifier" sender:self];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    //Code that presents or dismisses a view controller here
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
//                    [self.navigationController popViewControllerAnimated:YES];
//                    [self.delegate completionOfAddalbum];
//                });
//              
//            }
//        }
//    }];
    
    NSString *joinedComponents = [self.activeGroupIds componentsJoinedByString:@","];
    
    [[PhotosListService sharedService] addAlbumWithTitle:self.addNameTExtFeild.text Groups:joinedComponents Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.albumID = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"album_id"]];
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                   // [self.navigationController popViewControllerAnimated:YES];
                    [self.delegate completionOfAddalbum];
                    [self takePhoto];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }
        
    }];

    
}



-(void)takePhoto
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoAction  = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSLog(@"Reset action");
        SCNavigationController *nav = [[SCNavigationController alloc] init];
        nav.scNaigationDelegate = self;
        [nav showCameraWithParentController:self];
        
    }];
    
    
    
    UIAlertAction *choosePhotoAction  = [UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        //  NSLog(@"Reset action");
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        // imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction  = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"Reset action");
    }];
    
    
    
    [alertController addAction:takePhotoAction];
    [alertController addAction:choosePhotoAction];
    [alertController addAction:cancelAction];
    
    //    UIPopoverPresentationController *popPresenter = [alertController
    //                                                     popoverPresentationController];
    //    UIButton *button = (UIButton*)sender;
    //
    //    popPresenter.sourceView = (UIButton*)sender;
    //    popPresenter.sourceRect = button.bounds;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        alertController.popoverPresentationController.sourceView = self.view;
        //    alertController.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
        alertController.popoverPresentationController.sourceRect = CGRectMake(35, self.view.bounds.size.height-100, self.view.bounds.size.width, 300);
    }
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
//    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        //    portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // present the cropper view controller
        VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.view.frame.size.width, self.view.frame.size.width) limitScaleRatio:3.0];
        imgCropperVC.delegate = self;
        [self presentViewController:imgCropperVC animated:YES completion:^{
            // TO DO
        }];
    }];
    
    
}

#pragma mark - SCNavigationController delegate
- (void)didTakePicture:(SCNavigationController *)navigationController image:(UIImage *)image {
    // PostViewController *con = [[PostViewController alloc] init];
    // con.postImage = image;
    // [navigationController pushViewController:con animated:YES];
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    CapturedImageViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"capturedImageIdentifier"];
    //    vc.postImage = image;
    //    [navigationController pushViewController:vc animated:YES];
    self.sImage = image;
    //self.albumID = self.albumID;
    [self performSegueWithIdentifier:@"NewAlbumPublishPhotoIdentifier" sender:self];
    
}


#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    // self.myImage.image = editedImage;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // TO DO
        
        self.sImage = editedImage;
        
        [self performSegueWithIdentifier:@"NewAlbumPublishPhotoIdentifier" sender:self];
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}






#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"NewAlbumPublishPhotoIdentifier"]) {
        PublishPhotoViewController *controller = ([segue.destinationViewController isKindOfClass:[PublishPhotoViewController class]]) ? segue.destinationViewController : nil;
        controller.sImage=self.sImage;
        controller.albumID =self.albumID;
        controller.delegate = self;
    }
}

#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.addNameTExtFeild resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
    
    if (textField==self.addNameTExtFeild) {
        
        
        
        if([[textField text] length] > 72)
        {
            return NO;
        }
        
    }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
    
    
    return YES;
}


-(void)completionOfUploadImage
{
    
    //    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID andPage:@"1" Completion:^(NSDictionary *response, NSError *error) {
    //        NSLog(@"response %@",response);
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            // code here
    //            dataArray = [[response objectForKey:@"data"] objectForKey:@"photos"];
    //            [self.cView reloadData];
    //        });
    //    }];
    
    
    
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Success"
                                  message:@"Photo Saved Successfully"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                           
                             
                             [self.navigationController popViewControllerAnimated:YES];
                             
                               [self.delegate completionOfAddalbum];
                             
                             
                         }];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
//    [dataArray removeAllObjects];
//    [self.cView reloadData];
//    
//    
//    [[AlbumDetailsService sharedService] fetchAlbumDetailsWithAlbumId:self.albumID Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
//        
//        
//        if (error) {
//            
//            
//            
//        }else{
//            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                
//                
//                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
//                                                                                    options:NSJSONReadingMutableContainers
//                                                                                      error:nil];
//                NSLog(@"Data %@",jsonLoginDictionary);
//                
//                
//                
//                
//                
//                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // code here
//                        dataArray = [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"album_images"];
//                        [self.cView reloadData];
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                    });
//                    
//                }
//                else
//                {
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // code here
//                        [self.cView reloadData];
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                    });
//                    
//                }
//                
//                
//                
//                
//            });
//            
//            
//        }
//        
//        
//        
//    }];
    
    
}



@end
