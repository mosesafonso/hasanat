//
//  GalleryViewController.m
//  photogallerytest
//
//  Created by K Rummler on 5/29/13.
//

#import "GalleryViewController.h"
#import "GalleryImageCell.h"
//#import "ImageContext.h"
#import "UIImageView+WebCache.h"

@interface GalleryViewController ()
{
    NSIndexPath *currentImageIndexPath;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@end

@implementation GalleryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //  self.view.frame = [[UIApplication sharedApplication] keyWindow].frame;
    
    [self setupCollectionView];
    
    UIBarButtonItem * item2= [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"downloadicon"] style:UIBarButtonItemStylePlain target:self action:@selector(savePhoto:)];
    item2.tintColor = [UIColor whiteColor];
    
    
    
    
    NSArray * buttonArray =[NSArray arrayWithObjects:item2 ,nil];
    self.navigationItem.rightBarButtonItems =buttonArray;
    
    
 currentImageIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
   
}


-(IBAction)savePhoto:(id)sender
{
    // GalleryImageCell *cell1 = (GalleryImageCell*)[self.collectionView cellForItemAtIndexPath:currentImageIndexPath];
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    GalleryImageCell *cell = (GalleryImageCell*)[self.collectionView cellForItemAtIndexPath:currentImageIndexPath];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImageWriteToSavedPhotosAlbum(cell.image.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        });
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertController * alert;
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    
    if (error)
    {
        alert = [UIAlertController
                 alertControllerWithTitle:@"Alert"
                 message:@"Unable to save image to Photo Album"
                 preferredStyle:UIAlertControllerStyleAlert];
        
        
        
    }
    else
    {
        alert = [UIAlertController
                 alertControllerWithTitle:@"Alert"
                 message:@"Image saved to Photo Album."
                 preferredStyle:UIAlertControllerStyleAlert];
    }
    
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)setupCollectionView {
    //  [self.photosCollectionView registerClass:[SinglePhotoCollectionViewCell class] forCellWithReuseIdentifier:@"SinglePhotoCollectionViewCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"GalleryImageCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:@"galleryImageCell"];
    
    [self.collectionView setPagingEnabled:YES];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GalleryImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"galleryImageCell" forIndexPath:indexPath];

   // ImageContext *imageContext = [self.images objectAtIndex:indexPath.row];
    
  //  cell.imageContext = imageContext;
    
    NSString * urlTextEscaped = [[NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"file_path"]]  stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];

    
   [cell.image sd_setImageWithURL:[NSURL URLWithString:urlTextEscaped] placeholderImage: [UIImage imageNamed:@"imagePlaceholder"]];
    [self.collectionView addGestureRecognizer:cell.scrollView.pinchGestureRecognizer];
    [self.collectionView addGestureRecognizer:cell.scrollView.panGestureRecognizer];
    
    cell.photoCaption.text = [NSString stringWithFormat:@"%@",self.dataArray[indexPath.row][@"file_name"]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(GalleryImageCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    currentImageIndexPath = indexPath;

    [self.collectionView removeGestureRecognizer:cell.scrollView.pinchGestureRecognizer];
    [self.collectionView removeGestureRecognizer:cell.scrollView.panGestureRecognizer];
    cell.scrollView.zoomScale = 1.0;
}

//- (void)setSelectedImage:(NSUInteger)selectedImage
//{
//    _selectedImage = selectedImage;
//    
//    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.selectedImage inSection:0] atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally | UICollectionViewScrollPositionCenteredVertically) animated:YES];
//}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.collectionView.frame.size;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
        currentImageIndexPath = [self.collectionView indexPathForCell:cell];
        //  NSLog(@"%d",indexPath.row);
        
       // self.myPageControl.currentPage = indexPath.row;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
