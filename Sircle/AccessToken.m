#import "AccessToken.h"

@implementation AccessToken

static AccessToken* _accessToken;

+(AccessToken*)classMethodeOfAccessToken
{
    if (_accessToken==nil)
    {
        _accessToken=[[AccessToken alloc]init];
        
    }
    return _accessToken;
}


-(void)setAccessTokenValue:(NSString*)accessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:accessToken forKey:@"AccessToken"];
    
    [defaults synchronize];
}

-(NSString*)getAccessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults stringForKey:@"AccessToken"];
    
}

-(void)setRefreshTokenValue:(NSString*)accessToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:accessToken forKey:@"RefreshToken"];
    
    [defaults synchronize];
    
}

-(NSString*)getRefreshToken
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults stringForKey:@"RefreshToken"];
    
    
}

@end