//
//  main.m
//  Sircle
//
//  Created by MOSES AFONSO on 04/09/15.
//  Copyright (c) 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
