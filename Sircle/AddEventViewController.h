//
//  AddEventViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 18/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbstractActionSheetPicker;
@interface AddEventViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UITextField *detailsTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;

- (IBAction)startDatePressed:(id)sender;
- (IBAction)endDatePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UIButton *endDate;
- (IBAction)categoryButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *startTimeButton;
- (IBAction)startTimeButtonPressed:(id)sender;
- (IBAction)endTimeButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *endTimeButton;
- (IBAction)dayReminderButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dayReminderButton;
@property (weak, nonatomic) IBOutlet UIButton *hourReminderButton;
- (IBAction)hourReminderButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *minuteReminderButton;
- (IBAction)minuteReminderButtonClicked:(id)sender;


@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@end
