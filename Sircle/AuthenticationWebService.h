//
//  AuthenticationWebService.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseService.h"
#import "LoginResponse.h"

@interface AuthenticationWebService : BaseService

+ (instancetype)sharedLoginService;
//- (void) signInWithUsername:(NSString*) username andPassword:(NSString*) password completion:(void(^)(id result,NSError *error))callback;
- (void) signInWithUsername:(NSString*) username andPassword:(NSString*) password completion:(void(^)(LoginResponse *result,NSError *error))callback;
@end
