//
//  NotifcationData.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Notification.h"

@interface NotifcationData : NSObject

// data -> total_records, page, page_records, notifications
@property (nonatomic, strong) NSNumber *totalRecords;
@property (nonatomic, strong) NSNumber *page;
@property (nonatomic, strong) NSNumber *pageRecords;
@property (nonatomic, strong) NSArray *notifications;

- (NSDictionary*) dictionaryMapping;

@end
