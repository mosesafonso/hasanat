//
//  OAuthData.m
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "OAuthData.h"

@implementation OAuthData

//access_token, expires_in, token_type, scope, refresh_token

@synthesize refreshToken, expiresIn, accessToken, scope, tokenType;

- (NSDictionary*) dictionaryMapping{
    return @{@"refresh_token": self.refreshToken, @"expires_in": expiresIn, @"access_token": accessToken, @"scope": scope, @"token_type" : tokenType};
}

@end
