//
//  GroupResponse.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "GroupResponse.h"

@implementation GroupResponse

@synthesize status, message, data;

- (NSDictionary*) dictionaryMapping{
    
    return @{@"code": self.status, @"message": message, @"data": data};
    
}

@end
