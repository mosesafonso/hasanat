//
//  InstituteInfoViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 23/04/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface InstituteInfoViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@end
