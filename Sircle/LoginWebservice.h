//
//  LoginWebservice.h
//  Sircle
//
//  Created by MOSES AFONSO on 26/05/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginWebservice : NSObject
+ (instancetype)sharedService;

-(void) loginWithUserName:(NSString*)userName Password:(NSString*)password DeviceToken:(NSString*)deviceToken DeviceType:(NSString*)deviceType Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;

-(void) logoutWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))loginCallBack;

@end
