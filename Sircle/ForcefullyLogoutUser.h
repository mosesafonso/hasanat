//
//  ForcefullyLogoutUser.h
//  Sircle
//
//  Created by MOSES AFONSO on 26/07/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForcefullyLogoutUser : NSObject
+ (instancetype)sharedService;


-(void) forcefullyLogoutUserWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))CallBack;

@end
