//
//  PDFViewerViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 04/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFViewerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) NSString *urlString;
@end
