//
//  UploadAlbumResponse.h
//  Sircle
//
//  Created by Soniya Gadekar on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadAlbumResponse : NSObject

@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) NSDictionary *error;

- (NSDictionary*) dictionaryMapping;

@end
