//
//  YouTubeViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@interface YouTubeViewController : UIViewController
@property (weak, nonatomic) NSString *videoID;
@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;
@end
