//
//  PhotosListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"
#import "UploadAlbumResponse.h"
#import "AddAlbumResponse.h"

@interface PhotosListService : BaseService
+ (instancetype)sharedService;
-(void) fetchPhotosListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;
- (void) addAlbum:(NSString*)albumName Completion:(void (^)(AddAlbumResponse *, NSError *))callback;
- (void) addPhotoWithDesc:(NSString*)description  albumId:(NSString*)albumId andData:(NSData*)data  Completion:(void (^)(UploadAlbumResponse *, NSError *))callback;

-(void) fetchPhotosListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;


-(void) addAlbumWithTitle:(NSString*)title Groups:(NSString*)groups Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) addPhotoWithCaption:(NSString*)caption AlbumId:(NSString*)albumID File:(NSData*)data Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
