//
//  AlbumDetailsService.h
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface AlbumDetailsService : BaseService
+ (instancetype)sharedService;
-(void) fetchAlbumDetailsWithAlbumId:(NSString*)Id andPage:(NSString*)page Completion:(void (^)(NSDictionary *, NSError *))callback;


-(void) fetchAlbumDetailsWithAlbumId:(NSString*)albumID Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
