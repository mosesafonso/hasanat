//
//  SettingsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 11/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
- (IBAction)saveGroups:(id)sender;

@end
