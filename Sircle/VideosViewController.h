//
//  VideosViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "YTVimeoExtractor.h"

@interface VideosViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) IBOutlet UITableView *videosTableView;
@property (weak, nonatomic) NSString *videoID;
@end
