//
//  AlbumGridViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumGridViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *cView;
@property (weak, nonatomic) NSString *albumID;
@property(nonatomic,strong) UIImage *sImage;
@end
