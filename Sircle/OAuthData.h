//
//  OAuthData.h
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAuthData : NSObject

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSNumber *expiresIn;
@property (nonatomic, strong) NSString *tokenType;
@property (nonatomic, strong) NSString *scope;
@property (nonatomic, strong) NSString *refreshToken;

- (NSMutableDictionary*) dictionaryMapping;
@end
