//
//  TermsListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface TermsListService : BaseService
+ (instancetype)sharedService;
-(void) fetchTermsWithCompletion:(void (^)(NSDictionary *, NSError *))callback;

-(void) fetchTermsWithCompletionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;
@end
