//
//  PhotosViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) IBOutlet UITableView *photosTableView;
- (IBAction)addAlbum:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addAlbum;
-(void)goToAlbum;
@property (weak, nonatomic) NSString *albumID;
@end
