//
//  SettingsHeaderTableViewCell.h
//  Sircle
//
//  Created by MOSES AFONSO on 11/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioButtonImage;
@property (weak, nonatomic) IBOutlet UIButton *radioButton;

@end
