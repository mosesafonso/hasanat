//
//  AddHolidayEventViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddHolidayEventViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "ActionSheetPicker.h"
#import "NSDate+TCUtils.h"
#import "CalendarViewService.h"
#import "MBProgressHUD.h"

@interface AddHolidayEventViewController ()<UITextFieldDelegate>

@end

@implementation AddHolidayEventViewController
@synthesize actionSheetPicker = _actionSheetPicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
    
    navigationControllerObject.navigationView.hidden=YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
   // self.navigationController.navigationBar.topItem.title = @"Holiday";
    
    self.navigationItem.title = @"Holiday";
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)startDatePressed:(id)sender {

    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(StartDateWasSelected:) origin:sender];
    

    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)endDatePressed:(id)sender {
    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(EndDateWasSelected:) origin:sender];
    
    
    [self.actionSheetPicker showActionSheetPicker];
}

- (void)StartDateWasSelected:(NSDate *)selectedDate{
   // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
   // self.dateTextField.text = [self.selectedDate description];
    
  
    
    [self.startDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (void)EndDateWasSelected:(NSDate *)selectedDate{
    // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    // self.dateTextField.text = [self.selectedDate description];
    
    [self.endDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (IBAction)addEvent:(id)sender {
    
    // {event_type:event_type_id,title:title,strdate:strdate,enddate:enddate}
    
    if (self.titleTextField.text != nil) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
//        NSDictionary *dict = @{@"event_type" : @1, @"title" : self.titleTextField.text, @"strdate" : self.startDate.titleLabel.text, @"enddate" : self.endDate.titleLabel.text};
//        
//        [[CalendarViewService sharedService] addEventWithRequest:dict Completion:^(AddCallResponse *result, NSError *error) {
//            if (error) {
//                
//            }else{
//                int status = [result.status intValue];
//                NSString *message = result.message;
//                if (status == 200){
//                    // success
//                    NSLog(@"success add event %@", message);
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // code here
//                     //   [_calendarManager reload];
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        [self.navigationController popViewControllerAnimated:YES];
//                        
//                        
//                    });
//                }
//            }
//        }];
        [[CalendarViewService sharedService] AddPublicHolidayEventWithTitle:self.titleTextField.text StartDate:self.startDate.titleLabel.text EndDate:self.endDate.titleLabel.text Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
            
            if (error) {
            }else{
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                          [self.navigationController popViewControllerAnimated:YES];
                        
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
            }

            
        }];
        
        
    }else{
        // actionsheet
    }

    
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm:ss aa";
    }
    
    return dateFormatter;
}

#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.titleTextField resignFirstResponder];
  //  [self.passwordTextFeild resignFirstResponder];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
   
    if (textField==self.titleTextField) {
        
    
    
    if([[textField text] length] > 72)
    {
        return NO;
    }
        
    }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
    
    
    return YES;
}



@end
