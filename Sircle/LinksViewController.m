//
//  LinksViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "LinksViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "LinksTableViewCell.h"
#import <SafariServices/SafariServices.h>
#import "LinksListService.h"
#import "UIImageView+WebCache.h"
#import "AddLinksViewController.h"
#import "MBProgressHUD.h"
#import "LoginStatus.h"

@interface LinksViewController ()<AddLinksViewControllerDelegate>
{
    NSMutableArray *data;
    NSInteger pageCount;
    UIActivityIndicatorView *spinnerFooter;
     UIRefreshControl *refreshControl;
}
@end

@implementation LinksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Links";
    
    if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
        [self.addLinksButton setEnabled:NO];
        [self.addLinksButton setTintColor: [UIColor clearColor]];
    }
    
    [self customSetup];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([LinksTableViewCell class])
                                    bundle:nil];
    [self.linksTableView registerNib:cellNib
               forCellReuseIdentifier:@"LinksTableViewCell"];
    self.linksTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.linksTableView.estimatedRowHeight = 72;
    
    pageCount = 0;
    
    self.linksTableView.tableFooterView = [UIView new];
    
    
    
    data = [[NSMutableArray alloc] init];
    
    [self callWebservice];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startRefresh:)forControlEvents:UIControlEventValueChanged];
    
    [self.linksTableView addSubview:refreshControl];


}

-(IBAction)startRefresh:(id)sender
{
    // [refreshControl endRefreshing];
    
    pageCount = 0;
    [self callWebservice];
}


-(void)callWebservice
{
    pageCount = pageCount +1;
    
    if (pageCount==1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
//    
//    [[LinksListService sharedService] fetchLinksListWithPage:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSDictionary *response, NSError *error) {
//        
//        NSLog(@"Response %@",response);
//        
//        
//        
//        [data addObjectsFromArray:[[response objectForKey:@"data"] objectForKey:@"links"]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [spinnerFooter removeFromSuperview];
//            [self.linksTableView reloadData];
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//        
//        
//    }];
    
    [[LinksListService sharedService] fetchLinksListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    if (refreshControl.isRefreshing) {
                        [data removeAllObjects];
                        [self.linksTableView reloadData];
                        [refreshControl endRefreshing];
                    }
                    
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"links"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.linksTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.linksTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
       // [self.addLinksButton setAction:@selector(addLinksButtonClicked:)];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(IBAction)addLinksButtonClicked:(id)sender
{
  // add link service call
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LinksTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"LinksTableViewCell"];
    
    cell.linkLabel.text = data [indexPath.row][@"link_url"];
     cell.name.text = data [indexPath.row][@"link_name"];
    
    NSString * str = data[indexPath.row][@"date_added"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
     cell.createdOn.text = arr[0];
     cell.timeLabel.text = arr[1];
    
   
    
     [cell.favicon sd_setImageWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://www.google.com/s2/favicons?domain=%@",data [indexPath.row][@"link_url"]]] placeholderImage:[UIImage imageNamed:@"imagePlaceholder"]];
    
    cell.linkLabel.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        NSLog(@"User tapped %@", string);
        
        NSURL *url = [[NSURL alloc] initWithString:string];
        
        
        SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:url];
        
        [self presentViewController:safari animated:YES completion:nil];
    };
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:25];
    }
    else
    {
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:20];
    }

    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSURL *urlToSend = [[NSURL alloc]initWithString:[[data objectAtIndex:indexPath.row] objectForKey:@"link_url"]];
    
    NSLog(@"Link Tapped %@",[[data objectAtIndex:indexPath.row] objectForKey:@"link_url"]);
    
    SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:urlToSend];
    
    [self presentViewController:safari animated:YES completion:nil];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 0.0) {
        // [spinnerFooter startAnimating];
        spinnerFooter = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinnerFooter startAnimating];
        spinnerFooter.frame = CGRectMake(0, 0,self.linksTableView.frame.size.width, 44);
        self.linksTableView.tableFooterView = spinnerFooter;
        [self callWebservice];
        
        //[self methodThatAddsDataAndReloadsTableView];
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)callAddLinks:(id)sender {
      [self performSegueWithIdentifier:@"addLinksIdentifier" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"addLinksIdentifier"]) {
        
        // Get destination view
        AddLinksViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        
        
    }
    
}

-(void)completionOfAddLinks
{
    
    [data removeAllObjects];
    [self.linksTableView reloadData];
    
    
    
    pageCount = 1;
    
    [[LinksListService sharedService] fetchLinksListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"links"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.linksTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.linksTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
        
    }];

}

@end
