//
//  AuthenticationManager.m
//  Sircle
//
//  Created by Soniya Gadekar on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AuthenticationManager.h"
#import "AuthenticationWebService.h"

@implementation AuthenticationManager

static AuthenticationManager *authenticationManager = nil;

+ (instancetype)sharedLoginManager {
    
    if (nil ==  authenticationManager) {
        authenticationManager = [AuthenticationManager new];
    }    
    return authenticationManager;
}


- (void) signInWithUsername:(NSString*) username andPassword:(NSString*) password completion:(void(^)(id result,NSError *error))callback{
    [[AuthenticationWebService sharedLoginService] signInWithUsername:username andPassword:password completion:^(id loginResponse, NSError *error) {
        if (error) {
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
              callback(loginResponse, error);
        }
    }];
}


@end
