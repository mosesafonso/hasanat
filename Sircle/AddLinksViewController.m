//
//  AddLinksViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 15/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddLinksViewController.h"
#import "SettingsHeaderTableViewCell.h"
#import "GroupManager.h"
#import "Group.h"
#import "GroupSaveService.h"
#import "LinksListService.h"
#import "AddCallResponse.h"
#import "MBProgressHUD.h"
#import "GroupService.h"

@interface AddLinksViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *groupNames;
@property (nonatomic,strong) NSMutableArray *activeGroupIds;
@end

@implementation AddLinksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([SettingsHeaderTableViewCell class])
                                    bundle:nil];
    [self.settingsTableView registerNib:cellNib
                 forCellReuseIdentifier:@"SettingsHeaderTableViewCell"];
    self.settingsTableView.rowHeight = 60;
    
    self.settingsTableView.estimatedRowHeight = 60;
    
    self.groupNames = [[NSMutableArray alloc] init];
    self.activeGroupIds = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    
    self.settingsTableView.tableFooterView = [UIView new];
    
    self.settingsTableView.layer.borderWidth = 1.0;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [[GroupService sharedService] fetchGroupsListWithCompletion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    [self.groupNames addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"groups"]];
                    
                    for (int i =0; i<self.groupNames.count; i++) {
                        if ([[NSString stringWithFormat:@"%@",self.groupNames[i][@"select"]] isEqualToString:@"1"]) {
                            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //   [spinnerFooter removeFromSuperview];
                        //   [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //  [spinnerFooter removeFromSuperview];
                        //  [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
    }];
 
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsHeaderTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    cell.groupNameLabel.text = self.groupNames[indexPath.row][@"name"];
    
    
    
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]]) {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    else
    {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    
    [cell.radioButton setTag:indexPath.row];
    [cell.radioButton addTarget:self action:@selector(checkOrUncheckGroup:) forControlEvents: UIControlEventTouchUpInside];
    
    return cell;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    
    headerCell.groupNameLabel.text = @"ALL";
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    else
    {
        
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    
    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
    
    // [headerCell setTag:section];
    return headerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(IBAction)selectAllGroups:(id)sender
{
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [self.activeGroupIds removeAllObjects];
        for (int i=0; i<self.groupNames.count; i++) {
            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
            [self.settingsTableView reloadData];
        }
    }
    else
    {
        [self.activeGroupIds removeAllObjects];
        [self.settingsTableView reloadData];
        
    }
}


-(IBAction)checkOrUncheckGroup:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    SettingsHeaderTableViewCell *cell = (SettingsHeaderTableViewCell*)[self.settingsTableView cellForRowAtIndexPath:indexPath];
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]])
    {
        [self.activeGroupIds removeObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
        
    }
    else
    {
        [self.activeGroupIds addObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    // 1. Dequeue the custom header cell
//    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
//    
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
//    }
//    else
//    {
//        
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
//    }
//    
//    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
//    
//    // [headerCell setTag:section];
//    return headerCell;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 60;
//}


//-(IBAction)selectAllGroups:(id)sender
//{
//    
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [self.activeGroupIds removeAllObjects];
//        for (int i=0; i<self.groupNames.count; i++) {
//            [self.activeGroupIds addObject:self.groupNames[i][@"group_id"]];
//            [self.settingsTableView reloadData];
//        }
//    }
//    else
//    {
//        [self.activeGroupIds removeAllObjects];
//        [self.settingsTableView reloadData];
//        
//    }
//}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (IBAction)submitButtonClicked:(id)sender {
    // add links
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    
//    [[LinksListService sharedService] addLink:self.linkTitleTextFeild.text link:self.linkUrlTextFeild.text Completion:^(AddCallResponse *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            NSNumber *status = result.status;//[result valueForKey:@"status"];
//           // NSString *message = result.message;
//            if ([status integerValue] == 200) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                     [MBProgressHUD hideHUDForView:self.view animated:YES];
//                    [self.navigationController popViewControllerAnimated:YES];
//                    [self.delegate completionOfAddLinks];
//                });
//                
//                
//            }
//        }
//    }];
    
    NSString *myURLString = self.linkUrlTextFeild.text;
   // NSURL *myURL;
    if ([myURLString.lowercaseString hasPrefix:@"http://"]) {
       
    } else {
        myURLString = [NSString stringWithFormat:@"http://%@",myURLString];
    }
    
    NSString *joinedComponents = [self.activeGroupIds componentsJoinedByString:@","];
    
    [[LinksListService sharedService] addLinkWithTitle:self.linkTitleTextFeild.text  Link:myURLString Groups:joinedComponents Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
            
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.delegate completionOfAddLinks];
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                });
                
            }
            
            
        }
        
    }];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.linkTitleTextFeild resignFirstResponder];
    
    [self.linkUrlTextFeild resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
    
    if (textField==self.linkTitleTextFeild) {
        
        
        
        if([[textField text] length] > 72)
        {
            return NO;
        }
        
    }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
    
    
    return YES;
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
