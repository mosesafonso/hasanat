//
//  ForgotPasswordViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "MBProgressHUD.h"
#import "ForgotPasswordWebservice.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    
    self.emailIdTextFeild.layer.borderColor = [UIColor colorWithHexString:@"CFD0D1"].CGColor;
    self.emailIdTextFeild.layer.borderWidth = 0.5f;
    //self.emailIdTextFeild.layer.cornerRadius = 4.0f;
    self.emailIdTextFeild.clipsToBounds = YES;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 19, 20)];
    _emailIdTextFeild.leftView = paddingView;
    _emailIdTextFeild.leftViewMode = UITextFieldViewModeAlways;
    
    [self.navigationController.navigationBar setHidden:NO];
      self.navigationController.navigationBar.tintColor = [UIColor colorWithHexString:@"FFFFFF"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailIdTextFeild resignFirstResponder];
   
    
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField:textField up:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
    
    //    [textField resignFirstResponder];
    //  return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@"\n"])
    {
        [textField resignFirstResponder];
        return NO;
    }
    
    return YES;
    
    
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (IBAction)loginButtonClicked:(id)sender {
    
    //  [self.navigationController popViewControllerAnimated:YES];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    [[ForgotPasswordWebservice sharedService] forgotPasswordWithUserName:self.emailIdTextFeild.text Completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    
                 
                    
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Success"
                                                  message:@"Please check your email for a reset link"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Do some thing here
                                                [self.navigationController popViewControllerAnimated:YES];
                                             
                                         }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                else
                {
                    
                    NSLog(@"Error");
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Alert"
                                                  message:@"Something Went Wrong Please Try Again"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Do some thing here
                                             
                                             
                                         }];
                    [alert addAction:ok];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                }
                
                
                
                
            });
            
            
        }
        
        
    }];
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
