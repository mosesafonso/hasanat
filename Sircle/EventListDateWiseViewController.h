//
//  EventListDateWiseViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 20/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListDateWiseViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;
@property (strong, nonatomic) IBOutlet UILabel *messageLbl;
@property (weak, nonatomic) NSString *eventID;
@property (weak, nonatomic) NSDate *selectedDate;
@end
