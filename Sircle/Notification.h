//
//  Notification.h
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject
//notifications -> (Array) subject, message, date_string, time_string

@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *timeString;

- (NSDictionary*) dictionaryMapping;

@end
