//
//  GroupSaveService.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "GroupSaveService.h"
#import "GroupSaveRequest.h"
#define groupSaveURL @"user/device_group_save"
#import "AccessToken.h"
#import "URLS.h"

@implementation GroupSaveService

static GroupSaveService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [GroupSaveService new];
    }
    return groupService;
}

- (void) saveGroupsWithCompletion:(NSDictionary*)values  completion:(void(^)(NSDictionary *,NSError *error))callback
{
    
    GroupSaveRequest *request = [[GroupSaveRequest alloc] init];
    request.regId = [values objectForKey:@"regId"];
    request.groupValString = [values objectForKey:@"groupValString"];
    
     NSDictionary *dict = @{ @"groupValString":[values objectForKey:@"groupValString"], @"regId" : [values objectForKey:@"regId"]};
    
    [self.serviceDelegate executePostWithUrl:groupSaveURL request:dict responseClass:NULL callBack:^(id result, NSError *error) {
        
        if (error == nil) {
            
            callback(result, error);
        }
    }];

    
   
}

-(void) saveGroupsWithIds:(NSString*)groupIds Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:GroupsListURL];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"group_id=%@",groupIds];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
    
}


@end
