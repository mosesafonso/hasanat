//
//  UploadAlbumResponse.m
//  Sircle
//
//  Created by Soniya Gadekar on 17/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "UploadAlbumResponse.h"

@implementation UploadAlbumResponse

@synthesize code, message, data,error;

- (NSDictionary*) dictionaryMapping{
    return @{@"code": self.code, @"message": message, @"data" : data,@"error" :error};
    
}
@end
