//
//  SignUpViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextFeild;
- (IBAction)signUpButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextFeild;
- (IBAction)loginButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextFeild;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *welcomeTextTopspace;

@end
