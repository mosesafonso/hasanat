//
//  PhotosTableViewCell.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UILabel *albumDisplayName;
@property (weak, nonatomic) IBOutlet UILabel *fileCount;
@property (weak, nonatomic) IBOutlet UILabel *createdOn;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
