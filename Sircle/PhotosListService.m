//
//  PhotosListService.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "PhotosListService.h"
#import <UIKit/UIKit.h>
#import "URLS.h"
#import "AccessToken.h"

#define photoListURL @"user/album_group"
#define addAlbumListURL @"user/add_album"


@implementation PhotosListService
static PhotosListService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [PhotosListService new];
    }
    return groupService;
}

-(void) fetchPhotosListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback{
    
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

     NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
    
    NSDictionary *dict = @{@"regId" : uniqueIdentifier,@"page":pageNumber,@"groupId":joinedComponents};
    
 
    
    [self.serviceDelegate executeGetWithUrl:photoListURL queryParams:dict requestHeaders:nil responseClass:NULL callBack:^(NSDictionary *json, NSError *error) {
        if (error) {
            
        }else{
            callback(json, nil);
        }
    }];
}

-(void) fetchPhotosListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:PhotosListUrl];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
//    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"page=%@",pageNumber];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
}



- (void) addAlbum:(NSString*)albumName Completion:(void (^)(AddAlbumResponse *, NSError *))callback{
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
//    
//    NSDictionary *dict = @{ @"grp":joinedComponents, @"albumName" : albumName};
//    [self.serviceDelegate executePostWithUrl:addAlbumListURL request:dict responseClass:[AddAlbumResponse class] callBack:^(id result, NSError *error) {
//        if (error) {
//            
//        }else{
//            callback(result, nil);
//        }
//    }];
}

-(void) addAlbumWithTitle:(NSString*)title Groups:(NSString*)groups Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:AddAlbum];
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"album_name=%@&group_id=%@",title,groups];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];

}

- (void) addPhotoWithDesc:(NSString*)description  albumId:(NSString*)albumId andData:(NSData*)data  Completion:(void (^)(UploadAlbumResponse *, NSError *))callback{
 
    //NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    NSDictionary *dict = @{ @"caption_name":description, @"album_id" : albumId, @"file" : data};
    [self.serviceDelegate executePostWithUrl:addPhotoURL request:dict data:data responseClass:[UploadAlbumResponse class] callBack:^(id result, NSError *error) {
        if (error) {
            
        }else{
            callback(result, nil);
        }
    }];
    
    
}

-(void) addPhotoWithCaption:(NSString*)caption AlbumId:(NSString*)albumID File:(NSData*)data Completion:(void (^)(NSData *dataUrl, NSURLResponse *response, NSError *error))documentsListCallBack
{
//    [self setCustomHeader:[[AccessToken classMethodeOfAccessToken] getAccessToken]];
//    [self.urlRequest setURL:[NSURL URLWithString:[self requestURLString:url]]];
//    [self.urlRequest setHTTPMethod:@"POST"];
//    
//    NSString *boundary = @"_187934598797439873422234";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [self.urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
//    
//    NSMutableData *body = [NSMutableData data];
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"files.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[NSData dataWithData:data]];
//    
//    
//    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"album_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    NSString *alb_id = [NSString stringWithFormat:@"%@",[requestObject objectForKey:@"album_id"]];
//    [body appendData:[alb_id dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@""dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"caption_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [body appendData:[[requestObject objectForKey:@"caption_name"] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    
//    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    [self.urlRequest setHTTPBody:body];
//    [self.urlRequest addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
//    
//    NSURLSessionUploadTask *uploadImageTask = [self.session uploadTaskWithRequest:self.urlRequest fromData:body completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        id responseObject = [[responseClass alloc] init];
//        NSDictionary *json;
//        NSInteger statusCode = ((NSHTTPURLResponse*) response).statusCode;
//        
//        if (!error && (statusCode == 200 || statusCode == 201)) {
//            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//            [responseObject setValuesForKeysWithDictionary:json];
//            callBack(responseObject, nil);
//            
//        }else if(statusCode == 200 || statusCode == 201){
//            if (error == nil) {
//                json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//                [responseObject setValuesForKeysWithDictionary:json];
//                callBack(responseObject, error);
//            }else{
//                callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
//            }
//            
//        }else{
//            callBack(nil, [[NSError alloc]initWithDomain:@"Error" code:statusCode userInfo:nil]);
//        }
//    }];
//    
//    [uploadImageTask resume];
    
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:addPhotoURL];
    
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
     [request setHTTPMethod:@"POST"];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    
        NSString *boundary = @"_187934598797439873422234";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"test.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[NSData dataWithData:data]];
    
    
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"album_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
       // NSString *alb_id = [NSString stringWithFormat:@"%@",[requestObject objectForKey:@"album_id"]];
        [body appendData:[albumID dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@""dataUsingEncoding:NSUTF8StringEncoding]];
    
    
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"caption_name\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[caption dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [request setHTTPBody:body];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];

    
    //13a1aca03407953556ddd21889058833
    
//    NSString* userUpdate=[NSString stringWithFormat:@"album_name=%@&group_id=%@",title,groups];
//    NSLog(@"URL::%@",userUpdate);
//    
//    [request setHTTPMethod:@"POST"];
//    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
//    NSString* jsonString=nil;
//    
//    if (! jsonData)
//    {
//        
//    }else
//    {
//        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
//        NSLog(@"URL jsonString::%@",jsonString);
//    }
    //[request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
   // [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
   // [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];


}

@end
