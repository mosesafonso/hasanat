//
//  AppDelegate.m
//  Sircle
//
//  Created by MOSES AFONSO on 04/09/15.
//  Copyright (c) 2015 MOSES AFONSO. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginStatus.h"
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterSmokeStyle.h"
#import "JCNotificationBannerPresenterIOSStyle.h"
#import "JCNotificationBannerPresenterIOS7Style.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "NotificationsViewController.h"
#import "AlbumGridViewController.h"
#import "EventDetailsViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "PhotosViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "CalendarViewController.h"
#import "HomeViewController.h"
#import "ForceUpdate.h"
#import "CheckUserStatus.h"
#import "ForcefullyLogoutUser.h"


@interface AppDelegate ()
{
    UIStoryboard *storyboard;

}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self checkVersion];
//    [self userTokenExpired];
    
        [Fabric with:@[[Crashlytics class]]];
    
     [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    BOOL success = [[LoginStatus classMethodeOfLoginStatus] getLoginStatus];
    NSString *segueId = success ? @"LoggedIn": @"NotLoggedIn"  ;
    
    if (success) {
//        if ([[ExpiryDate classMethodeOfExpiryDate] checkExpiryDate]) {
//            NSLog(@"Expired");
//            [[NewAccessToken classMethodeOfNewAccessToken] requestNewAccessToken];
//        } else {
//            // [[NewAccessToken classMethodeOfNewAccessToken] requestNewAccessToken];
//        }
      UINavigationController *navigationController =  (UINavigationController*)self.window.rootViewController;
        
        navigationController.navigationBarHidden = YES;
        
          [self checkUserStatus];
    }
    
    
    
    [self.window.rootViewController performSegueWithIdentifier:segueId sender:self];
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        
        //        storyboard = [UIStoryboard storyboardWithName:@"iPad.storyboard" bundle:nil];
        storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
        
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
    }
    
   
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
    
   // [self callPhotosView];
    
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if(remoteNotification)
    {
//        [self performSelector:@selector(postNotificationToPresentPushMessagesVC:)
//                   withObject:remoteNotification
//                   afterDelay:1];
        
         [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        
        
      //  {"title":"ALBUM","message":"PHOTO: Birthday","payload":{"id":51,"type":"album","other_id":51}}
        
        if (success) {
       NSString* type = [[[[remoteNotification objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"type"];
            
            if ([type isEqualToString:@"notification"]) {
                
                [self callNotificationsView];
                
            }
            
            if ([type isEqualToString:@"document"]) {
                
                [self callDocumentsView];
                
            }
            
            if ([type isEqualToString:@"newsletter"]) {
                
                [self callNewslettersView];
                
            }
            
            
            if ([type isEqualToString:@"link"]) {
                
                [self callLinksView];
                
            }
            
            if ([type isEqualToString:@"video"]) {
                
                [self callVideosView];
                
            }
            
            if ([type isEqualToString:@"album"]) {
                
                // [self callAlbumView];
                
                
                
                self.albumID = [NSString stringWithFormat:@"%@",[[[[remoteNotification objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]] ;
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"PhotoListViewPage"]) {
                
                self.albumID = [NSString stringWithFormat:@"%@",[[[[remoteNotification objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"calendar"]) {
                
                self.eventID = [NSString stringWithFormat:@"%@",[[[[remoteNotification objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callEventView];
                
            }
        
            
        }

        
    }
    else
    {
        
    }
    
    return YES;
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
    
    
    NSString* newdeviceToken = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""] ;
    
    NSLog(@"%@",newdeviceToken);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:newdeviceToken forKey:@"DeviceToken"];
    
    [defaults synchronize];
    
}

-(void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    BOOL success = [[LoginStatus classMethodeOfLoginStatus] getLoginStatus];
    if (success)
    {
        
        NSLog(@"User Info %@",userInfo);
        
        
       
        
//        UIAlertController * alert =   [UIAlertController
//                                      alertControllerWithTitle:@"User Info"
//                                      message:[NSString stringWithFormat:@"%@",userInfo]
//                                      preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//                                 //Do some thing here
//                                 
//                                 
//                                 
//                               //  [self.navigationController popViewControllerAnimated:YES];
//                                 
//                               //  [self.delegate completionOfAddalbum];
//                                 
//                                 
//                             }];
//        [alert addAction:ok];
//        
//        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
//        

        
       if([app applicationState] == UIApplicationStateInactive)
        {
            
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            NSString* type = [[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"type"];
            
            if ([type isEqualToString:@"notification"]) {
                
                [self callNotificationsView];
                
            }
            
            
            
            if ([type isEqualToString:@"document"]) {
                
                [self callDocumentsView];
                
            }
            
            if ([type isEqualToString:@"newsletter"]) {
                
                [self callNewslettersView];
                
            }
            
            if ([type isEqualToString:@"link"]) {
                
                [self callLinksView];
                
            }
            
            if ([type isEqualToString:@"video"]) {
                
                [self callVideosView];
                
            }
            
            if ([type isEqualToString:@"album"]) {
                
               // [self callAlbumView];
                
               
                
                self.albumID =  [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]] ;
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"PhotoListViewPage"]) {
                
                self.albumID = [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"calendar"]) {
                
              self.eventID =  [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callEventView];
                
            }

            

        }
        else if ([app applicationState] == UIApplicationStateActive)
        {
            
            
//            {
//                aps =     {
//                    alert = ALBUM;
//                    "all_details" =         {
//                        message = "PHOTOS: yest";
//                        payload =             {
//                            id = 68;
//                            "other_id" = 68;
//                            type = album;
//                        };
//                        title = ALBUM;
//                    };
//                    badge = 0;
//                    sound = default;
//                };
//            }
            
//             {"aps":{"alert":"PHOTO: TEST PLS","all_details":{"title":"PHOTO: TEST PLS","message":"PHOTO: TEST PLS","payload":{"id":9,"type":"album","other_id":9}},"badge":0,"sound":"default"}}
            
            
             [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            NSString* title = @"";
            NSString* message =  [[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"message"];
            
            [JCNotificationCenter sharedCenter].presenter = [JCNotificationBannerPresenterIOS7Style new];
            [JCNotificationCenter
             enqueueNotificationWithTitle:title
             message:message
             tapHandler:^{
                 
                NSString* type = [[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"type"];
                 
                 if ([type isEqualToString:@"notification"]) {
                     
                      [self callNotificationsView];
                     
                 }
                 
                 if ([type isEqualToString:@"document"]) {
                     
                     [self callDocumentsView];
                     
                 }
                 
                 if ([type isEqualToString:@"newsletter"]) {
                     
                     [self callNewslettersView];
                     
                 }
                 
                 
                 if ([type isEqualToString:@"link"]) {
                     
                     [self callLinksView];
                     
                 }
                 
                 if ([type isEqualToString:@"video"]) {
                     
                     [self callVideosView];
                     
                 }
                 
                 if ([type isEqualToString:@"album"]) {
                     
                    // [self callAlbumView];
                     
                     self.albumID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                     [self callPhotosView];
                     
                 }
                 
                 if ([type isEqualToString:@"PhotoListViewPage"]) {
                     
                   //  self.albumID =  [[userInfo objectForKey:@"metaData"] objectForKey:@"albumId"];
                       self.albumID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                     [self callPhotosView];
                     
                 }
                 
                 if ([type isEqualToString:@"calendar"]) {
                     
                      self.eventID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                     [self callEventView];
                     
                 }

                 
                 
             }];
        }
        else
        {
            
             [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            NSString* type = [[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"type"];
            
            if ([type isEqualToString:@"notification"]) {
                
                [self callNotificationsView];
                
            }
            
            if ([type isEqualToString:@"document"]) {
                
                [self callDocumentsView];
                
            }
            
            if ([type isEqualToString:@"newsletter"]) {
                
                [self callNewslettersView];
                
            }
            
            
            if ([type isEqualToString:@"link"]) {
                
                [self callLinksView];
                
            }
            
            if ([type isEqualToString:@"video"]) {
                
                [self callVideosView];
                
            }
            
            if ([type isEqualToString:@"album"]) {
                
                // [self callAlbumView];
                
                self.albumID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"PhotoListViewPage"]) {
                
                 self.albumID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callPhotosView];
                
            }
            
            if ([type isEqualToString:@"calendar"]) {
                
                self.eventID =   [NSString stringWithFormat:@"%@",[[[[userInfo objectForKey:@"aps"] objectForKey:@"all_details"] objectForKey:@"payload"] objectForKey:@"id"]];
                [self callEventView];
                
            }
            
          
            
        }
    }
}


- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        
        //   return [self topViewController:lastViewController];
        return lastViewController;
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    BOOL success = [[LoginStatus classMethodeOfLoginStatus] getLoginStatus];
    
    
    
    if (success) {
        
        [self checkUserStatus];
        
        
    }
   [self checkVersion];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)callNotificationsView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NotificationsViewController *descController = (NotificationsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"NotificationsViewIdentifier"];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}


-(void)callNewslettersView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NotificationsViewController *descController = (NotificationsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"NewslettersViewIdentifier"];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}

-(void)callDocumentsView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NotificationsViewController *descController = (NotificationsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"DocumentsViewIdentifier"];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}

-(void)callLinksView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NotificationsViewController *descController = (NotificationsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"LinksViewIdentifier"];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}

-(void)callVideosView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    NotificationsViewController *descController = (NotificationsViewController*)[storyboard instantiateViewControllerWithIdentifier: @"VideosViewIdentifier"];
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}

//-(void)callAlbumView
//{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    NotificationsViewController *descController = (NotificationsViewController*)[st instantiateViewControllerWithIdentifier: @"AlbumViewIdentifier"];
//    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
//    MenuViewController *rearViewController = (MenuViewController *)[st instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
//    
//    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
//    
//    mainRevealController.rearViewController = rearViewController;
//    mainRevealController.frontViewController= frontNavigationController;
//    
//    self.window.rootViewController = mainRevealController;
//}

-(void)callPhotosView
{
    
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    HomeViewController *descController = (HomeViewController*)[storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
    descController.albumNotiID = self.albumID;
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;

}

-(void)callEventView
{
//    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

     HomeViewController *descController = (HomeViewController*)[storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
     descController.eventID = self.eventID;
    
       UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:descController];
    MenuViewController *rearViewController = (MenuViewController *)[storyboard instantiateViewControllerWithIdentifier: @"MenuViewIdentifier"];
    
    SWRevealViewController *mainRevealController = [[SWRevealViewController alloc]  init];
    
    mainRevealController.rearViewController = rearViewController;
    mainRevealController.frontViewController= frontNavigationController;
    
    self.window.rootViewController = mainRevealController;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AlbumGridIdentifier"]) {
        
        // Get destination view
        AlbumGridViewController *vc = [segue destinationViewController];
        vc.albumID = self.albumID;
        
        
    }
    
}

-(void)checkVersion

{
    
    [[ForceUpdate sharedService] getAppVersionWithCompletion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        
        
        
        if (error) {
            
            
            
            
            
            
            
        }else{
            
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // code here
                
                
                
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                     
                                                                                    options:NSJSONReadingMutableContainers
                                                     
                                                                                      error:nil];
                
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        NSString *string = [NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"ios_version"]];
                        int value = [string intValue];
                        
                            if (value <= 5)
                        {
                            
                        }
                        else
                        {
                            if ([[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"force_update_ios"]] isEqualToString:@"0"])
                                
                            {
                                
                                [self showOptionalUpdateDialog];
                                
                            }
                            
                            else
                                
                            {
                                
                                [self showForceUpdateDialog];
                                
                            }
                        }
                        
                        
                        
                        // code here
                        
                    });
                    
                    
                    
                }
                else if([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"401"])
                {
                    BOOL success = [[LoginStatus classMethodeOfLoginStatus] getLoginStatus];
                   
                    
                    if (success) {
                     
                         [self userTokenExpired];
                    }
                 
                   
                }
                
                else
                    
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        // code here
                        
                        
                        
                        
                        
                        
                    });
                    
                    
                    
                }
                
                
                
                
                
                
                
                
                
            });
            
            
            
            
            
        }
        
        
        
        
        
        
        
        
        
    }];
    
    
    
    
    
    
}



-(void)showForceUpdateDialog

{
    
    UIAlertController * alert =   [UIAlertController
                                   
                                   alertControllerWithTitle:@""
                                   
                                   message:@"Please Update to continue using the application. This should only take a few moments."
                                   
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* ok = [UIAlertAction
                         
                         actionWithTitle:@"Update"
                         
                         style:UIAlertActionStyleDefault
                         
                         handler:^(UIAlertAction * action)
                         
                         {
                             
                             //Do some thing here
                             
                             
                             
                             //  NSString *iTunesLink =   @"https://itunes.apple.com/us/app/emissio/id971055786?ls=1&mt=8";
                             
                             
                             
                             NSString *iTunesLink = @"itms://itunes.apple.com/us/app/hasanat-high-school/id852795870?ls=1&mt=8";

                             
                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                             
                             
                             
                             //  [self.navigationController popViewControllerAnimated:YES];
                             
                             
                             
                             //  [self.delegate completionOfAddalbum];
                             
                             
                             
                             
                             
                         }];
    
    [alert addAction:ok];
    
    
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
}

-(void)userTokenExpired

{
    
    UIAlertController * alert =   [UIAlertController
                                   
                                   alertControllerWithTitle:@"Alert"
                                   
                                   message:@"Please Login Again"
                                   
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* ok = [UIAlertAction
                         
                         actionWithTitle:@"Ok"
                         
                         style:UIAlertActionStyleDefault
                         
                         handler:^(UIAlertAction * action)
                         
                         {
                             
                             [[LoginStatus classMethodeOfLoginStatus] setLoginStatusValue:
                              NO];

                             
                            [self.window setRootViewController:nil];
                             
                             
//                             UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                             
                             UINavigationController *welcomeView = [storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
                             
                             [self.window setRootViewController:welcomeView];
                             
                             NSString *segueId =  @"NotLoggedIn"  ;
                             
                             [welcomeView performSegueWithIdentifier:segueId sender:self];
                            
                         }];
    
    [alert addAction:ok];
    
    
    
    
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
}



-(void)showOptionalUpdateDialog

{
    
    UIAlertController * alert =   [UIAlertController
                                   
                                   alertControllerWithTitle:@""
                                   
                                   message:@"A new version of the application is available. Would you like to update it?"
                                   
                                   preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* ok = [UIAlertAction
                         
                         actionWithTitle:@"Update"
                         
                         style:UIAlertActionStyleDefault
                         
                         handler:^(UIAlertAction * action)
                         
                         {
                             
                             //Do some thing here
                             
                             
                             
                             //NSString *iTunesLink =   @"https://itunes.apple.com/us/app/emissio/id971055786?ls=1&mt=8";
                             
                             
                             
                             NSString *iTunesLink = @"itms://itunes.apple.com/us/app/hasanat-high-school/id852795870?ls=1&mt=8";

                             
                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                             
                             
                             
                             //  [self.navigationController popViewControllerAnimated:YES];
                             
                             
                             
                             //  [self.delegate completionOfAddalbum];
                             
                             
                             
                             
                             
                         }];
    
    [alert addAction:ok];
    
    
    
    UIAlertAction* cancel = [UIAlertAction
                             
                             actionWithTitle:@"Cancel"
                             
                             style:UIAlertActionStyleCancel
                             
                             handler:^(UIAlertAction * action)
                             
                             {
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                             }];
    
    [alert addAction:cancel];
    
    
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
}

-(void)checkUserStatus

{
    
    [[CheckUserStatus sharedService] checkUSerStatusWithCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        
        
        if (error) {
            
            
            
            
            
            
            
        }else{
            
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // code here
                
                
                
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                     
                                                                                    options:NSJSONReadingMutableContainers
                                                     
                                                                                      error:nil];
                
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    
                    if ([[NSString stringWithFormat:@"%@",[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"logout"]] isEqualToString:@"1"]) {
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        [[ForcefullyLogoutUser sharedService] forcefullyLogoutUserWithCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
                            
                            
                            
                        }];
                        
                        
                        
                        [[LoginStatus classMethodeOfLoginStatus] setLoginStatusValue:
                         
                         NO];
                        
                        
                        
                        
                        
                        UIAlertController * alert=   [UIAlertController
                                                      
                                                      alertControllerWithTitle:@"Alert"
                                                      
                                                      message:@"You have been logged Out"
                                                      
                                                      preferredStyle:UIAlertControllerStyleAlert];
                        
                        
                        
                        UIAlertAction* ok = [UIAlertAction
                                             
                                             actionWithTitle:@"OK"
                                             
                                             style:UIAlertActionStyleDefault
                                             
                                             handler:^(UIAlertAction * action)
                                             
                                             {
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 [self.window setRootViewController:nil];
                                                 
                                                 
//                                                 UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                                                 
                                                 UINavigationController *welcomeView = [storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
                                                 
                                                 [self.window setRootViewController:welcomeView];
                                                 
                                                 NSString *segueId =  @"NotLoggedIn"  ;
                                                 
                                                 [welcomeView performSegueWithIdentifier:segueId sender:self];
                                                 
                                                 
                                                 
                                             }];
                        
                        [alert addAction:ok];
                        
                        
                        
                        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
                        
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
                
                else
                    
                {
                    
                    
                    
                    
                    
                }
                
                
                
                
                
                
                
                
                
            });
            
            
            
            
            
        }
        
        
        
        
        
    }];
    
    
    
    
}

@end
