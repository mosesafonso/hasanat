//
//  YouTubeViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "YouTubeViewController.h"

@interface YouTubeViewController ()

@end

@implementation YouTubeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    NSLog(@"Video %@",self.videoID);
    
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 };
    
    [self.playerView loadWithVideoId:self.videoID playerVars:playerVars];
}

-(void)viewDidAppear:(BOOL)animated
{
  //  NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
    //[[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
