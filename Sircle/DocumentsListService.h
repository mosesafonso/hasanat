//
//  DocumentsListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface DocumentsListService : BaseService
+ (instancetype)sharedService;
-(void) fetchDocumentsListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;

-(void) fetchDocumentsListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
