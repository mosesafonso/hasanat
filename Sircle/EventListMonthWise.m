//
//  EventListMonthWise.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "EventListMonthWise.h"

@implementation EventListMonthWise

static EventListMonthWise* _eventListMonthWise;


+(EventListMonthWise*)classMethodeOfEventListMonthWise
{
    if (_eventListMonthWise==nil)
    {
        _eventListMonthWise=[[EventListMonthWise alloc]init];
        
    }
    return _eventListMonthWise;
    
}

-(void)setEventListMonthWise:(NSMutableArray*)dataArray
{
    data = [[NSMutableArray alloc] init];
    
    data = dataArray;
}

-(NSMutableArray*)returnData;
{
    return data;
}


@end
