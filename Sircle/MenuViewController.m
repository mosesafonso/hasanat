//
//  MenuViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 10/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "MenuViewController.h"
#import "LoginStatus.h"
#import "AppDelegate.h"
#import "LoginWebservice.h"
#import "MenuViewController.h"
#import "MenuTableViewCell.h"

@interface MenuViewController ()
{
    NSMutableArray *arrMenuImages,*arrMenuNames;

}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    // Do any additional setup after loading the view.
  //  self.navigationController.navigationBar.hidden = NO;
    
//    arrMenuNames = [[NSMutableArray alloc]initWithObjects:@"Home",@"Calendar",@"Photos",@"Messages",@"Information",@"Circulars",@"Videos",@"Links",@"Settings",@"Contact Us",@"Support",@"Sign Out", nil];
    
    arrMenuNames = [[NSMutableArray alloc]initWithObjects:@"Home",@"Calendar",@"Photos",@"Messages",@"Newsletters",@"Documents",@"Videos",@"Links",@"Settings",@"Institute Info",@"Support",@"Sign Out", nil];
    
    
    arrMenuImages = [[NSMutableArray alloc]initWithObjects:@"HomeMenu",@"calendarIcon",@"photos",@"notifications",@"newsletter",@"documents",@"videos",@"links",@"settings",@"information",@"FeedbackMenu",@"signout", nil];
    
    
}
- (IBAction)signOutButtonPressed:(id)sender {
    
    
    
    [[LoginWebservice sharedService] logoutWithCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        
    }];
    
    [[LoginStatus classMethodeOfLoginStatus] setLoginStatusValue:
     NO];
    AppDelegate * appsDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appsDelegate.window setRootViewController:nil];
    UINavigationController *welcomeView = [self.storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
    [appsDelegate.window setRootViewController:welcomeView];
    NSString *segueId =  @"NotLoggedIn"  ;
    [welcomeView performSegueWithIdentifier:segueId sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)openMailClient:(id)sender {
    
    [self callMailClient];
}

-(void)callMailClient
{
    if ([MFMailComposeViewController canSendMail]) {
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"support@snaptech.in"];
        [mailComposer setToRecipients:toRecipents];
        //  [mailComposer setSubject:@""];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
        
    }
    else
    {
        NSString *recipients = @"mailto:support@snaptech.in";
        
        //?subject=Feedback";
        
        
        NSString *email = [NSString stringWithFormat:@"%@", recipients];
        
          email = [email stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
      //  email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
        
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)mailComposeController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{

    [self dismissViewControllerAnimated:YES completion:nil];
}
//#pragma mark - mail compose delegate
//
//- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
//{
//    switch (result)
//    {
//        case MFMailComposeResultCancelled:
//            NSLog(@"Mail cancelled");
//            break;
//        case MFMailComposeResultSaved:
//            NSLog(@"Mail saved");
//            break;
//        case MFMailComposeResultSent:
//            NSLog(@"Mail sent");
//            break;
//        case MFMailComposeResultFailed:
//            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
//            break;
//        default:
//            break;
//    }
//    
//    
//    [self dismissViewControllerAnimated:YES completion:NULL];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"MenuTableViewCell";
    
    MenuTableViewCell *cell = (MenuTableViewCell *)[_tableViewShowMenu dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.labelMenu.text = [arrMenuNames objectAtIndex:indexPath.row];
    //    NSLog(@"Names %@",[arrMenuNames objectAtIndex:indexPath.row]);
    cell.imageViewMenu.image = [UIImage imageNamed:[arrMenuImages objectAtIndex:indexPath.row]];
    
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrMenuNames.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        [self performSegueWithIdentifier:@"HomeViewSegue" sender:self];
    }
    if (indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"ShowCalenderIdentifier" sender:self];
    }
    if (indexPath.row == 2)
    {
        [self performSegueWithIdentifier:@"ShowPhotosIdentifier" sender:self];
    }
    if (indexPath.row == 3)
    {
        [self performSegueWithIdentifier:@"ShowMessagesIdentifier" sender:self];
    }
    if (indexPath.row == 4)
    {
        [self performSegueWithIdentifier:@"ShowNewsLettersIdentifier" sender:self];
    }
    if (indexPath.row == 5)
    {
        [self performSegueWithIdentifier:@"ShowDocumentsIdentifier" sender:self];
    }
    if (indexPath.row == 6)
    {
        [self performSegueWithIdentifier:@"ShowVideosIdentifier" sender:self];
    }
    if (indexPath.row == 7)
    {
        [self performSegueWithIdentifier:@"ShowLinksIdentifier" sender:self];
    }
    if (indexPath.row == 8)
    {
        if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
        {
            [self performSegueWithIdentifier:@"ShowMenuIdentifier" sender:self];
        }
        
        else
        {
            //        [self performSegueWithIdentifier:@"ShowMenuIdentifier" sender:self];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Alert"
                                         message:@"This feature is not available for admin"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Dismiss"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    if (indexPath.row == 9)
    {
        [self performSegueWithIdentifier:@"ShowInstituteInfoIdentifier" sender:self];
    }
    if (indexPath.row == 10)
    {
        [self callMailClient];
        
    }
    if (indexPath.row == 11)
    {
        [[LoginWebservice sharedService] logoutWithCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
            
        }];
        
        [[LoginStatus classMethodeOfLoginStatus] setLoginStatusValue:
         NO];
        AppDelegate * appsDelegate =[[UIApplication sharedApplication] delegate];
        [appsDelegate.window setRootViewController:nil];
        UINavigationController *welcomeView = [self.storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
        [appsDelegate.window setRootViewController:welcomeView];
        NSString *segueId =  @"NotLoggedIn"  ;
        [welcomeView performSegueWithIdentifier:segueId sender:self];
    }
}
- (IBAction)settingsButtonClicked:(id)sender {
    
    //
    if(![[[LoginStatus classMethodeOfLoginStatus] getUserType] isEqualToString:@"admin"])
    {
        [self performSegueWithIdentifier:@"settingsFromMenuIdentifier" sender:self];
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Alert"
                                      message:@"This Feature is not available to the Admin"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Dismiss"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        
        
        
    }
}

@end
