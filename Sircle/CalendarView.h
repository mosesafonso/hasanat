//
//  CalendarView.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>


@interface CalendarView : UIViewController<JTCalendarDelegate>
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;

@property (strong, nonatomic) JTCalendarManager *calendarManager;
- (IBAction)TodayButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *todayButton;

-(void)TermDateSelected:(NSString*)termDate TermNumber:(NSString*)termNumber TermPeriod:(NSString*)termPeriod;
@end
