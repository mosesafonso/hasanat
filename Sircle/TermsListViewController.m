//
//  TermsListViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "TermsListViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "TermsListTableViewCell.h"
#import "TermsListService.h"
#import "MBProgressHUD.h"

@interface TermsListViewController ()<UIGestureRecognizerDelegate>
{
    NSMutableArray *data;
}
@end

@implementation TermsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([TermsListTableViewCell class])
                                    bundle:nil];
    [self.termsTableView registerNib:cellNib
               forCellReuseIdentifier:@"TermsListTableViewCell"];
    self.termsTableView.rowHeight = 72;
    
    self.termsTableView.estimatedRowHeight = 72;
    
     self.termsTableView.tableFooterView = [UIView new];
    
    data = [[NSMutableArray alloc] init];
    
    
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[TermsListService sharedService] fetchTermsWithCompletion:^(NSDictionary *response, NSError *error) {
        NSLog(@"Response %@",response);
        
     
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // code here
            
            
        });
        
        
        
    }];
    
    
    [[TermsListService sharedService] fetchTermsWithCompletionHandler:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
        }else{
            
            NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:nil];
            NSLog(@"Data %@",jsonLoginDictionary);
            
            
               data = [jsonLoginDictionary objectForKey:@"data"];
            
            
            if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.termsTableView reloadData];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];

                
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.termsTableView reloadData];
                    [MBProgressHUD hideHUDForView:self.view animated:YES];

                });
                
            }
            
            
        }
        

        
    } ];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TermsListTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"TermsListTableViewCell"];
    
      NSString *startDate = data[indexPath.row][@"from"];
      NSString *endDate = data[indexPath.row][@"to"];
    
   
    
      cell.termName.text =  [NSString stringWithFormat:@"Term %@",data[indexPath.row][@"term"]];
//     cell.startDate.text =  [startDate substringToIndex: MIN(10, [startDate length])];
//      cell.endDate.text = [endDate substringToIndex: MIN(10, [endDate length])];
    
    cell.startDate.text = startDate;
     cell.endDate.text = endDate;

    
    [cell.startDate setTag:indexPath.row];
    [cell.endDate setTag:indexPath.row];
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFromStartDate:)];
    [tapGestureRecognizer setCancelsTouchesInView:NO];
    tapGestureRecognizer.delegate = self;
   // [self.imageView addGestureRecognizer:tapGestureRecognizer];
[cell.startDate addGestureRecognizer:tapGestureRecognizer];
    
    
    UITapGestureRecognizer *tapGestureRecognizer1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFromEndDate:)];
    [tapGestureRecognizer1 setCancelsTouchesInView:NO];
    tapGestureRecognizer1.delegate = self;
    // [self.imageView addGestureRecognizer:tapGestureRecognizer];
    [cell.endDate addGestureRecognizer:tapGestureRecognizer1];

    
    return cell;
}




- (void) handleTapFromStartDate: (UITapGestureRecognizer *)recognizer
{
    UILabel *currentLabel = (UILabel *) recognizer.view;
    NSString *labelText= currentLabel.text;
    [self.delegate TermDateSelected:labelText TermNumber:[NSString stringWithFormat:@"T%ld",(long)currentLabel.tag+1] TermPeriod:@"Start"];
    //Code to handle the gesture
}

- (void) handleTapFromEndDate: (UITapGestureRecognizer *)recognizer
{
    UILabel *currentLabel = (UILabel *) recognizer.view;
    NSString *labelText= currentLabel.text;
    [self.delegate TermDateSelected:labelText  TermNumber:[NSString stringWithFormat:@"T%ld",(long)currentLabel.tag+1] TermPeriod:@"End"];
    //Code to handle the gesture
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
