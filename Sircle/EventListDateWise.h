//
//  EventListDateWise.h
//  Sircle
//
//  Created by Soniya Gadekar on 20/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventListDateWise : NSObject
{
    NSMutableArray *data;
}
+(EventListDateWise*)classMethodeOfEventListDateWise;

//-(void)setEventListDateWise:(NSMutableArray*)data;

-(NSMutableArray*)returnData;
@end
