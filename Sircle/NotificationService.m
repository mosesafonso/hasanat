//
//  NotificationService.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "NotificationService.h"
#import <UIKit/UIKit.h>

#define notifucationURL @"user/notifications_group"

@implementation NotificationService

static NotificationService *notificationService = nil;

+ (instancetype)sharedService {
    
    if (nil ==  notificationService) {
        notificationService = [NotificationService new];
    }
    return notificationService;
}

-(void) fetchNotificationGroupsWithCompletion:(void(^)(SettingsResponse *result,NSError *error))callback{
    NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSDictionary *dict = @{@"regId" : uniqueIdentifier};
    [self.serviceDelegate executeGetWithUrl:notifucationURL queryParams:dict requestHeaders:nil responseClass:[SettingsResponse class] callBack:^(id result, NSError *error) {
        if (error) {
            callback(nil, result);
        }else{
            callback(result, nil);
        }
    }];
}

@end
