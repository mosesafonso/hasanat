//
//  AlbumDetailsService.m
//  Sircle
//
//  Created by MOSES AFONSO on 14/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AlbumDetailsService.h"
#define photoListURL @"user/photos"
#import "AccessToken.h"
#import "URLS.h"


@implementation AlbumDetailsService
static AlbumDetailsService *groupService = nil;
+ (instancetype)sharedService {
    
    if (nil ==  groupService) {
        groupService = [AlbumDetailsService new];
    }
    return groupService;
}

-(void) fetchAlbumDetailsWithAlbumId:(NSString*)Id andPage:(NSString*)page Completion:(void (^)(NSDictionary *, NSError *))callback
{
    
   // NSString* uniqueIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
   // NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    
   // NSString *joinedComponents = [[defaults arrayForKey:@"ActiveGroups"] componentsJoinedByString:@","];
    
    NSDictionary *dict = @{@"album_id":Id,@"page":page};
    
    
    [self.serviceDelegate executeGetWithUrl:photoListURL queryParams:dict requestHeaders:nil responseClass:NULL callBack:^(NSDictionary *json, NSError *error) {
        if (error) {
            
        }else{
            callback(json, nil);
        }
    }];
}

-(void) fetchAlbumDetailsWithAlbumId:(NSString*)albumID Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack
{
    NSString *accessToken=[[AccessToken classMethodeOfAccessToken] getAccessToken];
    
    NSString* urlText = [NSString stringWithFormat:GetAlbumImages];
    
    
      NSString * urlTextEscaped = [urlText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
//    NSString* urlTextEscaped = [urlText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlTextEscaped] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:Time_Interval];
    
    [request addValue:[NSString stringWithFormat:@"%@",accessToken] forHTTPHeaderField:@"Authorization"];
    
    //13a1aca03407953556ddd21889058833
    
    NSString* userUpdate=[NSString stringWithFormat:@"album_id=%@",albumID];
    NSLog(@"URL::%@",userUpdate);
    
    [request setHTTPMethod:@"POST"];
    NSData* jsonData=[userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    NSString* jsonString=nil;
    
    if (! jsonData)
    {
        
    }else
    {
        jsonString=[NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]];
        NSLog(@"URL jsonString::%@",jsonString);
    }
    [request setValue:jsonString forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:documentsListCallBack] resume];
}

@end
