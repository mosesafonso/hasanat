//
//  AddSchoolEventViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 18/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AbstractActionSheetPicker;
@interface AddSchoolEventViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;

- (IBAction)startDatePressed:(id)sender;
- (IBAction)endDatePressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *startDate;
@property (weak, nonatomic) IBOutlet UIButton *endDate;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;

@end
