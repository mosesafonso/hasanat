//
//  AddNotificationsViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 15/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddNotificationsViewControllerDelegate <NSObject>

-(void)completionOfAddNotifications;

@end


@interface AddNotificationsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *notificationTitleTextFeild;
@property (weak, nonatomic) IBOutlet UITextView *noificationsDetailsTextFeild;
@property(nonatomic,weak)id<AddNotificationsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *titleCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsCountLabel;
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@end
