//
//  LinksViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinksViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addLinksButton;
- (IBAction)callAddLinks:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@end
