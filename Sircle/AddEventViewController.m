//
//  AddEventViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 18/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "AddEventViewController.h"
#import "RKSwipeBetweenViewControllers.h"
#import "SettingsHeaderTableViewCell.h"
#import "GroupManager.h"
#import "Group.h"
#import "GroupSaveService.h"
#import "ActionSheetPicker.h"
#import "NSDate+TCUtils.h"
#import "GetCategoryListService.h"
#import "CalendarViewService.h"
#import "MBProgressHUD.h"
#import "GroupService.h"



@interface AddEventViewController ()<UITextFieldDelegate>
@property (nonatomic, strong) NSMutableArray *groupNames;
@property (nonatomic,strong) NSMutableArray *activeGroupIds;
@property (nonatomic,strong) NSMutableArray *categories;
@property (nonatomic,strong) NSMutableArray *categoriesID;
@property (nonatomic,strong) NSMutableArray *dayReminderArray;
@property (nonatomic,strong) NSMutableArray *hourReminderArray;
@property (nonatomic, strong) NSString *selectedCategory;
@property (nonatomic,strong) NSString *eventRecurring;
@property (nonatomic,strong) NSString *eventNotification;
@property (nonatomic,strong) NSMutableArray *minuteReminderArray;
@property (nonatomic,strong) NSMutableArray *repeatsSelectionArray;
@property (nonatomic,strong) NSMutableArray *repeatForArray;

@property (nonatomic,strong) NSMutableArray *weeklyVerticalSelectionArray;

@end

@implementation AddEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initializeUI];
    
    
    RKSwipeBetweenViewControllers *navigationControllerObject = (RKSwipeBetweenViewControllers*)self.navigationController;
    
    navigationControllerObject.navigationView.hidden=YES;
    
      self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    // self.navigationController.navigationBar.topItem.title = @"Holiday";
    
    self.navigationItem.title = @"Event";
    
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([SettingsHeaderTableViewCell class])
                                    bundle:nil];
    [self.settingsTableView registerNib:cellNib
                 forCellReuseIdentifier:@"SettingsHeaderTableViewCell"];
    self.settingsTableView.rowHeight = 60;
    
    self.settingsTableView.estimatedRowHeight = 60;
    
    [self initialiseData];
    
    // Do any additional setup after loading the view.
    
    self.settingsTableView.tableFooterView = [UIView new];
    
    self.settingsTableView.layer.borderWidth = 1.0;
    
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    [[GetCategoryListService sharedService] fetchCategoriesWithCompletion:^(NSDictionary *result, NSError *error){
//        
//        if (error) {
//            
//        }else{
//            NSArray *array = [result objectForKey:@"data"];
//            for (int i = 0; i<array.count; i++) {
//                [self.categories addObject:array[i][@"category"]];
//            }
//        }
//    }];
    
    [[GetCategoryListService sharedService] fetchCategoriesWithCompletionHandler:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        NSArray *array = [[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"categories"];
                                    for (int i = 0; i<array.count; i++) {
                                        [self.categories addObject:array[i][@"name"]];
                                        [self.categoriesID addObject:array[i][@"category_id"]];

                                    }

                                           });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //  [spinnerFooter removeFromSuperview];
                        //  [self.linksTableView reloadData];
                       
                    });
                    
                }
                
                
                
                
            });
            
            
        }

        
    }];
    
    
    
    
}

-(void)initialiseData
{
    self.groupNames = [[NSMutableArray alloc] init];
    self.activeGroupIds = [[NSMutableArray alloc] init];
    self.categories = [[NSMutableArray alloc] init];
    self.categoriesID = [[NSMutableArray alloc] init];
    self.dayReminderArray = [[NSMutableArray alloc] init];
    self.hourReminderArray = [[NSMutableArray alloc] init];
    self.minuteReminderArray = [[NSMutableArray alloc] init];
     self.repeatsSelectionArray = [[NSMutableArray alloc] init];
     self.repeatForArray = [[NSMutableArray alloc] init];
     self.weeklyVerticalSelectionArray = [[NSMutableArray alloc] init];
    
    for (int i =1; i<31; i++) {
         [self.dayReminderArray addObject:[NSString stringWithFormat:@"%d Days",i]];
    }
    
    for (int i =1; i<24; i++) {
        [self.hourReminderArray addObject:[NSString stringWithFormat:@"%d Hours",i]];
    }
    
    for (int i =1; i<60; i++) {
        [self.minuteReminderArray addObject:[NSString stringWithFormat:@"%d Minutes",i]];
    }
    
    for (int i =1; i<31; i++) {
        [self.repeatForArray addObject:[NSString stringWithFormat:@"%d Days",i]];
    }
    
     [self.repeatsSelectionArray addObject:@"Daily"];
     [self.repeatsSelectionArray addObject:@"Weekly"];
     [self.repeatsSelectionArray addObject:@"Monthly"];
     [self.repeatsSelectionArray addObject:@"Yearly"];
    
    
    _eventRecurring = @"N";
    _eventNotification = @"D";
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    [[GroupManager sharedManager] fetchGroupsWithCompletion:^(NSDictionary *result, NSError *error) {
//        if (error) {
//            
//        }else{
//            // id obj = result.data;
//            //            for (Group* group in obj) {
//            //                [self.groupNames addObject:group.groupName];
//            //            }
//            
//            NSLog(@"Data %@",[result objectForKey:@"data"]);
//            
//            [self.groupNames addObjectsFromArray:[result objectForKey:@"data"]];
//            
//            
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // code here
//                CGFloat height = self.settingsTableView.rowHeight;
//                height *= self.groupNames.count;
//                
//                self.tableViewHeight.constant = height+self.settingsTableView.rowHeight;
//                
//                [self.settingsTableView reloadData];
//              
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
//              
//            });
//            
//        }
//    }];
    
    [[GroupService sharedService] fetchGroupsListWithCompletion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    
                    [self.groupNames addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"groups"]];
                    
                    CGFloat height = self.settingsTableView.rowHeight;
                                    height *= self.groupNames.count;
                    
                                    self.tableViewHeight.constant = height+self.settingsTableView.rowHeight;
                    
                    for (int i =0; i<self.groupNames.count; i++) {
                        if ([[NSString stringWithFormat:@"%@",self.groupNames[i][@"select"]] isEqualToString:@"1"]) {
                            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //   [spinnerFooter removeFromSuperview];
                        //   [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        //  [spinnerFooter removeFromSuperview];
                        //  [self.linksTableView reloadData];
                        [self.settingsTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
    }];

    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.groupNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsHeaderTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    cell.groupNameLabel.text = self.groupNames[indexPath.row][@"name"];
    
    
    
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]]) {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    else
    {
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    
    [cell.radioButton setTag:indexPath.row];
    [cell.radioButton addTarget:self action:@selector(checkOrUncheckGroup:) forControlEvents: UIControlEventTouchUpInside];
    
    return cell;
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom header cell
    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
    }
    else
    {
        
        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    
    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
    
    // [headerCell setTag:section];
    return headerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 60;
}

-(IBAction)selectAllGroups:(id)sender
{
    
    if (self.activeGroupIds.count<self.groupNames.count) {
        [self.activeGroupIds removeAllObjects];
        for (int i=0; i<self.groupNames.count; i++) {
            [self.activeGroupIds addObject:self.groupNames[i][@"customer_group_id"]];
            [self.settingsTableView reloadData];
        }
    }
    else
    {
        [self.activeGroupIds removeAllObjects];
        [self.settingsTableView reloadData];
        
    }
}


-(IBAction)checkOrUncheckGroup:(id)sender
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    SettingsHeaderTableViewCell *cell = (SettingsHeaderTableViewCell*)[self.settingsTableView cellForRowAtIndexPath:indexPath];
    
    if ( [self.activeGroupIds containsObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]])
    {
        [self.activeGroupIds removeObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
        
    }
    else
    {
        [self.activeGroupIds addObject:[NSString stringWithFormat:@"%@",self.groupNames[indexPath.row][@"customer_group_id"]]];
        [cell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.activeGroupIds forKey:@"Groups"];
}

//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    // 1. Dequeue the custom header cell
//    SettingsHeaderTableViewCell *headerCell = (id)[tableView dequeueReusableCellWithIdentifier:@"SettingsHeaderTableViewCell"];
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonunselected"]];
//    }
//    else
//    {
//
//        [headerCell.radioButtonImage setImage:[UIImage imageNamed:@"RadioButtonCheck"]];
//    }
//
//    [headerCell.radioButton addTarget:self action:@selector(selectAllGroups:) forControlEvents: UIControlEventTouchUpInside];
//
//    // [headerCell setTag:section];
//    return headerCell;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 60;
//}


//-(IBAction)selectAllGroups:(id)sender
//{
//
//    if (self.activeGroupIds.count<self.groupNames.count) {
//        [self.activeGroupIds removeAllObjects];
//        for (int i=0; i<self.groupNames.count; i++) {
//            [self.activeGroupIds addObject:self.groupNames[i][@"group_id"]];
//            [self.settingsTableView reloadData];
//        }
//    }
//    else
//    {
//        [self.activeGroupIds removeAllObjects];
//        [self.settingsTableView reloadData];
//
//    }
//}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)startDatePressed:(id)sender {
    
    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(StartDateWasSelected:) origin:sender];
    
    
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)endDatePressed:(id)sender {
    NSDate *maxDate = [NSDate date];
    
    
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:maxDate
                                                          minimumDate:maxDate
                                                          maximumDate:nil
                                                               target:self action:@selector(EndDateWasSelected:) origin:sender];
    
    
    [self.actionSheetPicker showActionSheetPicker];
}

- (void)StartDateWasSelected:(NSDate *)selectedDate{
    // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    // self.dateTextField.text = [self.selectedDate description];
    
    
    
    [self.startDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (void)EndDateWasSelected:(NSDate *)selectedDate{
    // self.selectedDate = selectedDate;
    
    //may have originated from textField or barButtonItem, use an IBOutlet instead of element
    // self.dateTextField.text = [self.selectedDate description];
    
    
    
    [self.endDate setTitle:[[self dateFormatter] stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (NSDateFormatter *)dateFormatterOther
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy hh:mm:ss aa";
    }
    
    return dateFormatter;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)categoryButtonPressed:(id)sender {
    
      [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:self.categories initialSelection:0 target:self successAction:@selector(categoryWasSelected:) cancelAction:nil origin:sender];
}


- (void)categoryWasSelected:(NSNumber *)selectedIndex{

    
    [self.categoryButton setTitle:(self.categories)[(NSUInteger) [selectedIndex intValue]] forState:UIControlStateNormal];
    self.selectedCategory = (self.categoriesID)[(NSUInteger) [selectedIndex intValue]];
    
    NSLog(@"selected %@",self.selectedCategory);
}

- (IBAction)startTimeButtonPressed:(id)sender {
    
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(startTimeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}

- (IBAction)endTimeButtonClicked:(id)sender {
    
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(endTimeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}

-(void)startTimeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm:ss aa"];
    [self.startTimeButton setTitle:[dateFormatter stringFromDate:selectedTime] forState:UIControlStateNormal];
}

-(void)endTimeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm:ss aa"];
    [self.endTimeButton setTitle:[dateFormatter stringFromDate:selectedTime] forState:UIControlStateNormal];
}


- (IBAction)dayReminderButtonClicked:(id)sender {
    
    
      [ActionSheetStringPicker showPickerWithTitle:@"Select Days" rows:self.dayReminderArray initialSelection:0 target:self successAction:@selector(dayWasSelected:) cancelAction:nil origin:sender];
}

- (void)dayWasSelected:(NSNumber *)selectedIndex{
    _eventNotification = @"-";
    [self.dayReminderButton setTitle:(self.dayReminderArray)[(NSUInteger) [selectedIndex intValue]] forState:UIControlStateNormal];
}

- (IBAction)hourReminderButtonClicked:(id)sender {
    
     [ActionSheetStringPicker showPickerWithTitle:@"Select Hours" rows:self.hourReminderArray initialSelection:0 target:self successAction:@selector(hourWasSelected:) cancelAction:nil origin:sender];
    
}
- (IBAction)addEvent:(id)sender {

    
    
    NSString *joinedComponents = [self.activeGroupIds componentsJoinedByString:@","];
    
    if (self.titleTextField.text != nil && (self.activeGroupIds != nil || self.activeGroupIds.count > 0) && self.selectedCategory != nil) {

    
        
         [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
//        [[CalendarViewService sharedService] addEventWithRequest:dict Completion:^(AddCallResponse *result, NSError *error) {
//            if (error) {
//                
//            }else{
//                int status = [result.status intValue];
//                NSString *message = result.message;
//                if (status == 200){
//                    // success
//                    NSLog(@"success add event %@", message);
//                    
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        // code here
//                        //   [_calendarManager reload];
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
//                        [self.navigationController popViewControllerAnimated:YES];
//                        
//                        
//                    });
//                }
//            }
//        }];
        
        
        [[CalendarViewService sharedService] AddEventWithTitle:self.titleTextField.text
                                              EventDescription:self.detailsTextField.text
                                                 EventLocation:self.locationTextField.text
                                                     StartDate:[NSString stringWithFormat:@"%@ %@",self.startDate.titleLabel.text,self.startTimeButton.titleLabel.text]
                                                       EndDate: [NSString stringWithFormat:@"%@ %@",self.endDate.titleLabel.text,self.endTimeButton.titleLabel.text]
                                                        Groups:joinedComponents
                                                 EventCategory:self.selectedCategory
                                                EventRecurring:_eventRecurring
                                                    eventEvery:@"1"
                                                    eventWeekDay:@""
                                             eventRecurringEnd:@""
                                             eventNotification:_eventNotification
                                              NotificationHour:self.hourReminderButton.titleLabel.text
                                               NotificationMin:self.minuteReminderButton.titleLabel.text
                                               NotificationDay:self.dayReminderButton.titleLabel.text NotificationWeek:@"" Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
            
            if (error) {
            }else{
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
            }

        
        }];
        

    }
    
    
}

- (void)hourWasSelected:(NSNumber *)selectedIndex{
    
    _eventNotification = @"-";
    [self.hourReminderButton setTitle:(self.hourReminderArray)[(NSUInteger) [selectedIndex intValue]] forState:UIControlStateNormal];
}
- (IBAction)minuteReminderButtonClicked:(id)sender {
    
       [ActionSheetStringPicker showPickerWithTitle:@"Select Minutes" rows:self.minuteReminderArray initialSelection:0 target:self successAction:@selector(minuteWasSelected:) cancelAction:nil origin:sender];
}

- (void)minuteWasSelected:(NSNumber *)selectedIndex{
    
    _eventNotification = @"-";
    [self.minuteReminderButton setTitle:(self.minuteReminderArray)[(NSUInteger) [selectedIndex intValue]] forState:UIControlStateNormal];
}




#pragma mark - Text Field methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.titleTextField resignFirstResponder];
    [self.locationTextField resignFirstResponder];
    [self.detailsTextField resignFirstResponder];
    [self.view endEditing:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
    
    if (textField==self.titleTextField) {
        
        
        
        if([[textField text] length] > 72)
        {
            return NO;
        }
        
    }
    
    if (textField==self.detailsTextField) {
        
        
        
        if([[textField text] length] > 255)
        {
            return NO;
        }
        
    }
    
    
    NSLog(@"%lu",[[textField text] length]);
    
    
    
    return YES;
}

-(void)initializeUI
{
 
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _titleTextField.leftView = paddingView1;
    _titleTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _locationTextField.leftView = paddingView2;
    _locationTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _detailsTextField.leftView = paddingView3;
    _detailsTextField.leftViewMode = UITextFieldViewModeAlways;
    
}



@end
