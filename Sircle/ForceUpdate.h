//
//  ForceUpdate.h
//  Sircle
//
//  Created by MOSES AFONSO on 24/08/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForceUpdate : NSObject
+ (instancetype)sharedService;

-(void) getAppVersionWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))callback;
@end
