//
//  CalendarViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarViewController : UIPageViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addEventButton;
- (IBAction)addEvents:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButton;
@property (weak, nonatomic) NSString *eventID;
@end
