//
//  GroupSaveRequest.m
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "GroupSaveRequest.h"

@implementation GroupSaveRequest

@synthesize regId, groupValString;

- (NSDictionary*) dictionaryMapping{
    return @{@"regId": self.regId, @"groupValString": self.groupValString};
}
@end
