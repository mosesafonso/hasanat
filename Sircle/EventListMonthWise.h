//
//  EventListMonthWise.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventListMonthWise : NSObject
{
    NSMutableArray *data;
}
+(EventListMonthWise*)classMethodeOfEventListMonthWise;

-(void)setEventListMonthWise:(NSMutableArray*)data;

-(NSMutableArray*)returnData;


@end
