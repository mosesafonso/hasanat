//
//  Notification.m
//  Sircle
//
//  Created by Soniya Gadekar on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "Notification.h"

@implementation Notification

@synthesize subject, message, dateString, timeString;
////notifications -> (Array) subject, message, date_string, time_string

-(NSDictionary*) dictionaryMapping{
    return @{@"subject": self.subject, @"message": self.message, @"date_string": self.dateString, @"time_string" : timeString};
}

@end
