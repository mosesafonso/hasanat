//
//  VideoListService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"

@interface VideoListService : BaseService
+ (instancetype)sharedService;
-(void) fetchVideosListWithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;

-(void) fetchVideosListWithPageNumber:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
