//
//  VideosViewController.m
//  Sircle
//
//  Created by MOSES AFONSO on 12/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "VideosViewController.h"
#import "UIColor+HexaDecimalColors.h"
#import "SWRevealViewController.h"
#import "VideosTableViewCell.h"
#import "VideoListService.h"
#import "UIImageView+WebCache.h"
#import "YouTubeViewController.h"
#import "MBProgressHUD.h"


@interface VideosViewController ()
{
    NSMutableArray *data;
    NSInteger pageCount;
    UIActivityIndicatorView *spinnerFooter;
     UIRefreshControl *refreshControl;
}
@property (strong, nonatomic) YTVimeoExtractor *extractor;
//@property (strong, nonatomic) MPMoviePlayerViewController *playerView;
@property (nonatomic) YTVimeoVideoQuality quality;
@end

@implementation VideosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   //  self.quality = YTVimeoVideoQualityBestAvailable;
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackOpaque];
    
    
    
    self.navigationController.navigationBar.barTintColor =  [UIColor colorWithHexString:@"339966"];
    self.navigationController.navigationBar.translucent = NO;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:@"Roboto-Regular" size:20.0],NSFontAttributeName,
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.topItem.title = @"Videos";
    
    [self customSetup];
    
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([VideosTableViewCell class])
                                    bundle:nil];
    [self.videosTableView registerNib:cellNib
                   forCellReuseIdentifier:@"VideosTableViewCell"];
    self.videosTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.videosTableView.estimatedRowHeight = 72;
    
    pageCount = 0;
    
    self.videosTableView.tableFooterView = [UIView new];
    
    
    
    data = [[NSMutableArray alloc] init];
    
    [self callWebservice];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(startRefresh:)forControlEvents:UIControlEventValueChanged];
    
    [self.videosTableView addSubview:refreshControl];
    

}

-(IBAction)startRefresh:(id)sender
{
    // [refreshControl endRefreshing];
    
    pageCount = 0;
    [self callWebservice];
}

-(void)callWebservice
{
    pageCount = pageCount +1;
    
    
    if (pageCount==1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
//    [[VideoListService sharedService] fetchVideosListWithPage:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSDictionary *response, NSError *error) {
//        
//        NSLog(@"Response %@",response);
//        
//        
//        
//        [data addObjectsFromArray:[[response objectForKey:@"data"] objectForKey:@"videos"]];
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // code here
//            [spinnerFooter removeFromSuperview];
//            [self.videosTableView reloadData];
//            
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
//        
//        
//    }];
    [[VideoListService sharedService] fetchVideosListWithPageNumber:[NSString stringWithFormat:@"%ld",(long)pageCount] Completion:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            
            
            
        }else{
            //NSString *accessToken = ((LoginResponse*) result).data.oauth.accessToken;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                
                
                NSDictionary *jsonLoginDictionary = [NSJSONSerialization JSONObjectWithData:urlData
                                                                                    options:NSJSONReadingMutableContainers
                                                                                      error:nil];
                NSLog(@"Data %@",jsonLoginDictionary);
                
                
                
                
                
                if ([[NSString stringWithFormat:@"%@",[jsonLoginDictionary objectForKey:@"code"]] isEqualToString:@"200"]) {
                    
                    if (refreshControl.isRefreshing) {
                        [data removeAllObjects];
                        [self.videosTableView reloadData];
                        [refreshControl endRefreshing];
                    }
                    
                    [data addObjectsFromArray:[[jsonLoginDictionary objectForKey:@"data"] objectForKey:@"videos"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.videosTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // code here
                        [spinnerFooter removeFromSuperview];
                        [self.videosTableView reloadData];
                        
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                    });
                    
                }
                
                
                
                
            });
            
            
        }
        
        
        
        
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButton setTarget: self.revealViewController];
        [self.revealButton setAction: @selector( revealToggle: )];
        // [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideosTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:@"VideosTableViewCell"];
    cell.name.text = data[indexPath.row][@"caption_name"];
    NSString * str = data[indexPath.row][@"date"];
    NSArray * arr = [str componentsSeparatedByString:@" "];
    
     cell.createdOn.text = arr[0];
     cell.timeLabel.text = arr[1];
    [cell.videoThumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",data[indexPath.row][@"thumbnail"]]] placeholderImage:[UIImage imageNamed:@"imagePlaceholder"]];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:25];
    }
    else
    {
        cell.name.font = [UIFont fontWithName:@"Roboto-Light" size:20];
    }

    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    // UITableView only moves in one direction, y axis
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 0.0) {
        // [spinnerFooter startAnimating];
        spinnerFooter = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [spinnerFooter startAnimating];
        spinnerFooter.frame = CGRectMake(0, 0,self.videosTableView.frame.size.width, 44);
        self.videosTableView.tableFooterView = spinnerFooter;
        [self callWebservice];
        
        //[self methodThatAddsDataAndReloadsTableView];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([data[indexPath.row][@"video_type"] isEqualToString:@"youtube"]) {
        self.videoID = data[indexPath.row][@"video_id"];
        [self performSegueWithIdentifier:@"YouTubeVideoPlayerIdentifier" sender:self];
    }
    
   else if ([data[indexPath.row][@"video_type"] isEqualToString:@"vimeo"]) {
        self.videoID = data[indexPath.row][@"video_id"];
       // [self performSegueWithIdentifier:@"YouTubeVideoPlayerIdentifier" sender:self];
//       [YTVimeoExtractor fetchVideoURLFromURL:[NSString stringWithFormat:@"http://vimeo.com/%@",self.videoID] quality:self.quality completionHandler:^(NSURL *videoURL, NSError *error, YTVimeoVideoQuality quality) {
//           if (error) {
//               NSLog(@"Error : %@", [error localizedDescription]);
//           } else if (videoURL) {
//               NSLog(@"Extracted url : %@", [videoURL absoluteString]);
//               NSLog(@"Extracted with quality : %@", @[@"Low", @"Medium", @"HD 720", @"HD 1080"][quality]);
//               
//               dispatch_async (dispatch_get_main_queue(), ^{
//                   self.playerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
//                   [self.playerView.moviePlayer prepareToPlay];
//                   [self presentViewController:self.playerView animated:YES completion:^(void) {
//                       self.playerView = nil;
//                   }];
//               });
//           }
//       }];
       
       
       
       [[YTVimeoExtractor sharedExtractor]fetchVideoWithVimeoURL:[NSString stringWithFormat:@"%@",data[indexPath.row][@"video_url"]] withReferer:nil completionHandler:^(YTVimeoVideo * _Nullable video, NSError * _Nullable error) {
           
           if (video) {
               
              // self.titleLabel.text = [NSString stringWithFormat:@"Video Title: %@",video.title];
               //Will get the lowest available quality.
               //NSURL *lowQualityURL = [video lowestQualityStreamURL];
               
               //Will get the highest available quality.
               NSURL *highQualityURL = [video highestQualityStreamURL];
               
               MPMoviePlayerViewController *moviePlayerViewController = [[MPMoviePlayerViewController alloc]initWithContentURL:highQualityURL];
               
               [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];
           }else{
               
               UIAlertView *alertView = [[UIAlertView alloc]init];
               alertView.title = error.localizedDescription;
               alertView.message = error.localizedFailureReason;
               [alertView addButtonWithTitle:@"OK"];
               alertView.delegate = self;
               [alertView show];
               
           }
           
       }];
       
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"YouTubeVideoPlayerIdentifier"]) {
        
        // Get destination view
        YouTubeViewController *vc = [segue destinationViewController];
        vc.videoID = self.videoID;
    }
    
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
