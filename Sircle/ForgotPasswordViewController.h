//
//  ForgotPasswordViewController.h
//  Sircle
//
//  Created by MOSES AFONSO on 17/06/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailIdTextFeild;
- (IBAction)loginButtonClicked:(id)sender;
@end
