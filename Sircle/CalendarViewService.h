//
//  CalendarViewService.h
//  Sircle
//
//  Created by MOSES AFONSO on 13/12/15.
//  Copyright © 2015 MOSES AFONSO. All rights reserved.
//

#import "BaseService.h"
#import "AddCallResponse.h"

@interface CalendarViewService : BaseService
+ (instancetype)sharedService;
-(void) fetchEventDatesForMonth:(NSString*)month ForYear:(NSString*)year WithPage:(NSString*)pageNumber  Completion:(void (^)(NSDictionary *, NSError *))callback;

-(void) addEventWithRequest:(NSDictionary*)request Completion:(void (^)(AddCallResponse *, NSError *))callback;

-(void) deleteEventWithID:(NSString*)eventID completion:(void (^)(AddCallResponse *, NSError *))callback;

-(void) deleteEventWithID:(NSString*)eventID Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) AddPublicHolidayEventWithTitle:(NSString*)eventTitle StartDate:(NSString*)startDate EndDate:(NSString*)endDate  Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) AddSchoolHolidayEventWithTitle:(NSString*)eventTitle StartDate:(NSString*)startDate EndDate:(NSString*)endDate Groups:(NSString*)groups  Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) fetchEventDatesForMonth:(NSString*)month ForYear:(NSString*)year Page:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

-(void) AddEventWithTitle:(NSString*)eventTitle EventDescription:(NSString*)eventDescription EventLocation:(NSString*)eventLocation  StartDate:(NSString*)startDate EndDate:(NSString*)endDate Groups:(NSString*)groups  EventCategory:(NSString*)eventCategory EventRecurring:(NSString*)eventRecurring  eventEvery:(NSString*)eventEvery  eventWeekDay:(NSString*)eventWeekDay eventRecurringEnd:(NSString*)eventRecurringEnd eventNotification:(NSString*)eventNotification NotificationHour:(NSString*)notificationHour NotificationMin:(NSString*)notificationMin NotificationDay:(NSString*)notificationDay NotificationWeek:(NSString*)notificationWeek Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;


-(void)fetchEventDatesForDate:(NSString*)date Page:(NSString*)pageNumber Completion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))documentsListCallBack;

@end
