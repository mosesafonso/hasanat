//
//  NSURLImplementation.h
//  Demo-Restkit
//
//  Created by Soniya Gadekar on 25/11/15.
//  Copyright © 2015 creativecapsule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesProtocol.h"
#import "BaseModel.h"

@interface NSURLImplementation : NSObject<ServicesProtocol>


@end
