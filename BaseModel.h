//
//  BaseModel.h
//  Sircle
//
//  Copyright (c) 2014 CCI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

@property (nonatomic) int identifier;

/**
 *  typechecks object created dynamically.
 *  @return true-if model type false otherwise
 */
- (BOOL) isOfModelType;

/**
 *  gives the mapping
 *  @return mapping array
 */
+ (NSArray *)mappingArray;
// (RKObjectMapping*) mapping:(Class) objectClass;

+(NSDictionary*) dictionaryMapping;
@end
