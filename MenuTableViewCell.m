//
//  MenuTableViewCell.m
//  Sircle
//
//  Created by Prasad Lokhande on 14/11/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
