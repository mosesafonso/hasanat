//
//  UIColor+HexaDecimalColors.h
//  STYFI
//
//  Created by MOSES AFONSO on 08/08/15.
//  Copyright (c) 2015 Stylabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexaDecimalColors)
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
