//
//  BaseModel.m
//  CCIReferenceArchitecture
//
//  Created by Rahul Shettigar on 18/11/14.
//  Copyright (c) 2014 CCI. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

@synthesize identifier;

- (BOOL) isOfModelType{
    return true;
}

+ (NSArray *)mappingArray{
    return [NSArray array];
}


+ (NSDictionary*) dictionaryMapping{
    NSDictionary *dictionary = [NSDictionary new];
    return dictionary;
}


@end

