//
//  MenuTableViewCell.h
//  Sircle
//
//  Created by Prasad Lokhande on 14/11/16.
//  Copyright © 2016 MOSES AFONSO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMenu;
@property (weak, nonatomic) IBOutlet UILabel *labelMenu;

@end
